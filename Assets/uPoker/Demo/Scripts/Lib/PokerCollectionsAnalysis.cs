﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using uPoker;

/*
PERMITE ANALIZAR E GERAR PROBABILIDADES DE VITÓRIA, APARTIR DAS CARTAS DE MÃO E DE MESA
Esta é uma biblioteca simples de usar, porém extremamente robusta que lhe permitirá analiar mãos de poker afim
de criar probabilidades de vitória mesmo quando houver somente duas cartas conhecidas pelo jogador, suas cartas de bolço.
Aqui você encontrará dois algoritmos, sendo PPOT (POSITIVE POTENTIAL)  e PPOT_MONTECARLO (POSITIVE POTENTIAL MONTE CARLO). 
Ambos possuem a mesma finalidade de otimizar seus ganhos e minizar suas perdas apartir de dados estatisticos sobre possivel
vitória com suas cartas de bolço contra seus oponentes em mesa! 
*/
namespace PokerCollections.Analysis
{
    /// <summary>
    ///Positive Potential of a player's hand winner
    ///When you are playing poker on-line, you don't have access to other players' pocket cards. 
    ///One technique that can be used is to analyze the potential of a player's hand compared to an average opponent. 
    ///This is calculated by comparing all possible boards and all possible opponent pocket cards to the player’s hand.
    ///This gives a good feeling for the potential of a hand.However, rarely are you playing against an "average" player.
    ///So keep that in mind if you are using this technique in real poker play.
    ///The following code example shows how this calculation is made:  
    /// <para>
    /// Use the methods of the class, only when all the hole cards of the player and three or more cards on 
    /// the table. Otherwise the turnaround time for any results will be very large, making impossible the use of 
    /// this class 
    /// </para>    
    /// </summary>
    /// <example>
    /// Sample using uPoker mananger PPOT:
    /// <code>
    /// using UnityEngine;
    /// using System.Collections;
    /// using PokerCollections;
    /// using PokerCollections.Analysis;
    ///
    /// /*
    /// ====READ-ME=================================================
    /// These are the odds that a currently losing hand will improve to be a winner. 
    /// Positive Potential of a player's hand winner
    /// When you are playing poker on-line, you don't have access to other players' pocket cards. 
    /// One technique that can be used is to analyze the potential of a player's hand compared to an average opponent. 
    /// This is calculated by comparing all possible boards and all possible opponent pocket cards to the player’s hand.
    /// This gives a good feeling for the potential of a hand.However, rarely are you playing against an "average" player.
    /// So keep that in mind if you are using this technique in real poker play.
    /// The following code example shows how this calculation is made:  
    ///
    /// Use the methods of the class, only when all the hole cards of the player and three or more cards on 
    /// the table. Otherwise the turnaround time for any results will be very large, making impossible the use of 
    /// this class 
    /// ------------------------------------------------------------------
    /// > There are several ways to calculate the POT. Let's see a in this example: 
    /// Calculating POT from a manual cards  
    /// ------------------------------------------------------------------
    /// */
    ///
    /// public class PpotManual : MonoBehaviour
    /// {
    ///
    ///     PPOT pPot;
    ///
    ///     void Start()
    ///     {
    ///         //Create a new PPOT for probability calcule
    ///         string playerCards = "as ks";
    ///         string boardCards = "Ts Qs 2d";
    ///
    ///         pPot = new PPOT();
    ///         pPot.CalculePPOT(playerCards, boardCards);
    ///
    ///         Debug.Log("Hand: " + playerCards);
    ///         Debug.Log("Board " + boardCards);
    ///         Debug.Log("Hand Description: " + PokerEval.GetHandDescription(playerCards, boardCards));
    ///         Debug.Log("PPOT: " + pPot.ToString());
    ///     }
    /// }
    /// </code>
    /// Sample using MatchMananger
    /// <code>
    /// using UnityEngine;
    /// using System.Collections;
    /// using PokerCollections;
    /// using PokerCollections.Analysis;
    /// using PokerCollections.MatchGame;
    ///
    /// /*
    /// ====READ-ME=================================================
    /// These are the odds that a currently losing hand will improve to be a winner. 
    /// Positive Potential of a player's hand winner
    /// When you are playing poker on-line, you don't have access to other players' pocket cards. 
    /// One technique that can be used is to analyze the potential of a player's hand compared to an average opponent. 
    /// This is calculated by comparing all possible boards and all possible opponent pocket cards to the player’s hand.
    /// This gives a good feeling for the potential of a hand.However, rarely are you playing against an "average" player.
    /// So keep that in mind if you are using this technique in real poker play.
    /// The following code example shows how this calculation is made:  
    ///
    /// Use the methods of the class, only when all the hole cards of the player and three or more cards on 
    /// the table. Otherwise the turnaround time for any results will be very large, making impossible the use of 
    /// this class 
    /// ------------------------------------------------------------------
    /// > There are several ways to calculate the POT. Let's see a in this example: 
    /// Calculating POT from a gambling table.  
    /// ------------------------------------------------------------------
    /// */
    /// public class PpotTable : MonoBehaviour
    /// {
    ///
    ///     PPOT Ppot; //Positive POTential
    ///
    ///     void Start()
    ///     {
    ///         //Create a new Poker Table
    ///         PokerTable table = new PokerTable(true);
    ///         //Create a new Poker Match Mananger to the pokerTable
    ///         MatchGame match = new MatchGame(this, table);
    ///
    ///         match.StartNewMatch();
    ///
    ///         //Add New Player
    ///         BetPlayer player = table.CreatBetPlayer("test");
    ///         //Take cards to Player!
    ///         match.GetCardsToBetPlayers();
    ///         //Take first 3 cards from "FLOP" to the Board
    ///         match.turn = BetTurn.flop;
    ///
    ///         //Create a new Analizis to the Positive potential hand, 1 Oppent only
    ///         Ppot = new PPOT();
    ///         Ppot.CalculePPOT(player.holeCards, table.boardCards);
    ///
    ///         Debug.Log("Hand: " + player.PrintCards());
    ///         Debug.Log("Board " + table.PrintBoardCards());
    ///         Debug.Log("Hand Description: " + player.getHandDescription());
    ///         Debug.Log("PPOT: " + Ppot.ToString());
    ///     }
    /// }
    /// </code>
    /// </example>
    public class PPOT
    {
        /// <summary>
        /// Get HighCard winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        /// <see cref="Calcule"/>
        public double HighCard { get; private set; }
        /// <summary>
        /// Get Pair winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        /// <see cref="Calcule"/>
        public double Pair { get; private set; }
        /// <summary>
        /// Get Two Pair winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        public double TwoPair { get; private set; }
        /// <summary>
        /// Get ThreeOfKind winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        /// <see cref="Calcule"/>
        public double ThreeOfKind { get; private set; }
        /// <summary>
        /// Get Stratight winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        /// <see cref="Calcule"/>
        public double Stratight { get; private set; }
        /// <summary>
        /// Get Flush winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        /// <see cref="Calcule"/>
        public double Flush { get; private set; }
        /// <summary>
        /// Get FullHouse winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        /// <see cref="Calcule"/>
        public double FullHouse { get; private set; }
        /// <summary>
        /// Get FourOfKind winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        /// <see cref="Calcule"/> 
        public double FourOfKind { get; private set; }
        /// <summary>
        /// Get StraightFlush winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>
        /// <see cref="Calcule"/>
        public double StraightFlush { get; private set; }
        /// <summary>
        /// Get Total winner potential percent
        /// </summary>
        /// <note type="note">Use Calcule() Method to update values</note>  
        /// <see cref="Calcule"/>      
        public double totalPlayerWin { get; private set; }

        /// <summary>
        /// Player Pocket Cards
        /// </summary>
        private ulong playerMask;
        /// <summary>
        /// Board pokect cards
        /// </summary>
        private ulong boardMask;

        //Use to calculate the PPOT hand winner already stipulated in the boot in its class            
        private void Calcule()
        {
            HighCard = 0;
            Pair = 0;
            TwoPair = 0;
            ThreeOfKind = 0;
            Stratight = 0;
            Flush = 0;
            FullHouse = 0;
            FourOfKind = 0;
            StraightFlush = 0;
            totalPlayerWin = 0;

            // Calculate values for each hand type
            double[] playerWins = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            double[] opponentWins = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            // Count of total hands examined.
            long count = 0;

            // Iterate through all possible opponent hands
            foreach (ulong opponentMask in Hand.Hands(0UL, boardMask | playerMask, 2))
            {
                // Iterate through all possible boards
                foreach (ulong board in Hand.Hands(boardMask, opponentMask | playerMask, 5))
                {
                    // Create a hand value for each player
                    uint playerHandValue = Hand.Evaluate(board | playerMask, 7);
                    uint opponentHandValue = Hand.Evaluate(board | opponentMask, 7);

                    // Calculate Winners
                    if (playerHandValue > opponentHandValue)
                    {
                        // Player Win
                        playerWins[Hand.HandType(playerHandValue)] += 1.0;
                    }
                    else if (playerHandValue < opponentHandValue)
                    {
                        // Opponent Win
                        opponentWins[Hand.HandType(opponentHandValue)] += 1.0;
                    }
                    else if (playerHandValue == opponentHandValue)
                    {
                        // Give half credit for ties.
                        playerWins[Hand.HandType(playerHandValue)] += 0.5;
                        opponentWins[Hand.HandType(opponentHandValue)] += 0.5;
                    }
                    count++;
                }
            }



            HighCard = playerWins[(int)Hand.HandTypes.HighCard] / ((double)count) * 100.0;
            Pair = playerWins[(int)Hand.HandTypes.Pair] / ((double)count) * 100.0;
            TwoPair = playerWins[(int)Hand.HandTypes.TwoPair] / ((double)count) * 100.0;
            ThreeOfKind = playerWins[(int)Hand.HandTypes.Trips] / ((double)count) * 100.0;
            Stratight = playerWins[(int)Hand.HandTypes.Straight] / ((double)count) * 100.0;
            Flush = playerWins[(int)Hand.HandTypes.Flush] / ((double)count) * 100.0;
            FullHouse = playerWins[(int)Hand.HandTypes.FullHouse] / ((double)count) * 100.0;
            FourOfKind = playerWins[(int)Hand.HandTypes.FourOfAKind] / ((double)count) * 100.0;
            StraightFlush = playerWins[(int)Hand.HandTypes.StraightFlush] / ((double)count) * 100.0;
            totalPlayerWin = (HighCard + Pair + TwoPair + ThreeOfKind + Stratight + Flush + FullHouse + FourOfKind + StraightFlush + totalPlayerWin);
            /*
            Debug.Log("HighCard " + HighCard.ToString("N0") + "%");
            Debug.Log("Pair " + Pair.ToString("N0") + "%");
            Debug.Log("TwoPair " + TwoPair.ToString("N0") + "%");
            Debug.Log("ThreeOfKind " + ThreeOfKind.ToString("N0") + "%");
            Debug.Log("Stratight " + Stratight.ToString("N0") + "%");
            Debug.Log("Flush " + Flush.ToString("N0") + "%");
            Debug.Log("FullHouse " + FullHouse.ToString("N0") + "%");
            Debug.Log("FourOfKind " + FourOfKind.ToString("N0") + "%");
            Debug.Log("StraightFlush " + StraightFlush.ToString("N0") + "%");
            Debug.Log("TOTAL " + totalPlayerWin.ToString("N0") + "%");
            */
        }
        /// <summary>
        /// Use to calculate the PPOT winning percentage of a new hand
        /// </summary>
        /// <param name="_playerCards">Cards from player hand. (PoketCards format)</param>
        /// <param name="_boardCards">Cards from board. (PoketCards format)</param>
        public void CalculePPOT(string _playerCards, string _boardCards)
        {
            List<string> pc = _playerCards.Split(' ').ToList<string>();
            List<string> bc = _boardCards.Split(' ').ToList<string>();
            if (pc.Count != 2)
            {
                Debug.LogWarning("Probability calculation will not be executed why the player does not have a valid number of cards in hand !!");
                return;
            }
            else if (bc.Count < 3)
            {

                Debug.LogWarning("Probability calculation will not be executed why the board does not have a valid number of cards!!");
                return;
            }

            playerMask = Hand.ParseHand(_playerCards);
            boardMask = Hand.ParseHand(_boardCards);
            Calcule();
        }
        public void CalculePPOT(CardsList<Card> _playerCards, CardsList<Card> _boardCards)
        {
            if (_playerCards.Count != 2)
            {
                Debug.LogWarning("Probability calculation will not be executed why the player does not have a valid number of cards in hand !!");
                return;
            }
            else if (_boardCards.Count < 3)
            {
                Debug.LogWarning("Probability calculation will not be executed why the board does not have a valid number of cards!!");
                return;
            }
            //Convert Cards formmat on Eval cards formmat
            string evalPlayerCards = PokerEval.CardsToEval(_playerCards);
            string evalBoardCards = PokerEval.CardsToEval(_boardCards);
            //Convert eval cards to masck formmat
            playerMask = PokerEval.EvalToMask(evalPlayerCards);
            boardMask = PokerEval.EvalToMask(evalBoardCards);
            Calcule();
        }
        public override string ToString()
        {
            string std = "";
            std += ("HighCard " + HighCard.ToString("N1") + "%\n");
            std += ("Pair " + Pair.ToString("N1") + "%\n");
            std += ("TwoPair " + TwoPair.ToString("N1") + "%\n");
            std += ("ThreeOfKind " + ThreeOfKind.ToString("N1") + "%\n");
            std += ("Stratight " + Stratight.ToString("N1") + "%\n");
            std += ("Flush " + Flush.ToString("N1") + "%\n");
            std += ("FullHouse " + FullHouse.ToString("N1") + "%\n");
            std += ("FourOfKind " + FourOfKind.ToString("N1") + "%\n");
            std += ("StraightFlush " + StraightFlush.ToString("N1") + "%\n");
            std += ("TOTAL " + totalPlayerWin.ToString("N1") + "%\n");
            return std;
        }

    }
    /// <summary>
    /// Monte Carlo analysis can be used in analyzing the Positive Potential of a player's hand winner, compared to an average opponent.
    /// There are many reasons people choose to use Monte Carlo Analysis.
    /// In poker, the most common reason is to get a quick answer for something that otherwise would take a prohibitively long time to calculate.
    ///<para> Using this method of analysis, you will be able to check the potêncial of his letters, even when there is no card on the table, only the hand!</para>
    /// </summary>
    /// <example>
    /// Using PPOT_MONTERCALO uPoker Mananger
    /// <code>
    /// using UnityEngine;
    /// using System.Collections;
    /// using PokerCollections;
    /// using PokerCollections.Analysis;
    /// using PokerCollections.MatchGame;
    /// /*
    /// ====READ-ME=================================================
    /// Positive Potential Monte Carlo Algorithm
    /// Monte Carlo analysis can be used in analyzing the Positive Potential of a player's hand winner, compared to an average opponent.
    /// There are many reasons people choose to use Monte Carlo Analysis.
    /// In poker, the most common reason is to get a quick answer for something that otherwise would take a prohibitively long time to calculate.
    /// Using this method of analysis, you will be able to check the potêncial of his letters, even when there is no card on the table, only the hand!
    /// */
    /// public class PpotMonteCarloTable : MonoBehaviour
    ///     {
    ///         PPOT_MONTECARLO Ppot; //Positive POTential
    ///
    ///         void Start()
    ///         {
    ///             //Create a new Poker Table
    ///             PokerTable table = new PokerTable(true);
    ///             //Create a new match mananger for this pokerTable
    ///             MatchGame match = new MatchGame(this, table);
    ///
    ///             match.StartNewMatch();
    ///
    ///             //Add New Player
    ///             BetPlayer player = table.CreatBetPlayer("test");
    ///             //Take cards to Player!
    ///             match.GetCardsToBetPlayers();
    ///             //Take first 3 cards from "FLOP" to the Board
    ///             match.turn = BetTurn.flop;
    ///
    ///             //Create a new PPOT for probability calcule
    ///             Ppot = new PPOT_MONTECARLO();
    ///             Ppot.Calcule(1, player.holeCards, table.boardCards);
    ///
    ///             Debug.Log("Hand: " + player.PrintCards());
    ///             Debug.Log("Board " + table.PrintBoardCards());
    ///             Debug.Log("Hand Description: " + player.getHandDescription());
    ///             Debug.Log("PPOT Player: " + Ppot.player.ToString());
    ///             Debug.Log("PPOT Opponent: " + Ppot.opponent.ToString());
    ///         }
    ///    }
    /// </code>
    /// /// This Sample using MatchGame mananger
    /// <code>
    /// using UnityEngine;
    /// using System.Collections;
    /// using PokerCollections;
    /// using PokerCollections.Analysis;
    /// /*
    /// Positive Potential Monte Carlo Algorithm
    /// Monte Carlo analysis can be used in analyzing the Positive Potential of a player's hand winner, compared to an average opponent.
    /// There are many reasons people choose to use Monte Carlo Analysis.
    /// In poker, the most common reason is to get a quick answer for something that otherwise would take a prohibitively long time to calculate.
    /// Using this method of analysis, you will be able to check the potêncial of his letters, even when there is no card on the table, only the hand!
    /// */
    /// public class PpotMonteCarloManual : MonoBehaviour
    ///     {
    ///         PPOT_MONTECARLO Ppot; //Positive POTential
    /// 
    ///         void Start()
    ///         {
    ///             //Create a new PPOT for probability calcule
    ///             string playerCards = "as ks";
    ///             string boardCards = "Ts Qs 2d";
    /// 
    ///             //Create a new PPOT for probability calcule
    ///             Ppot = new PPOT_MONTECARLO();
    ///             Ppot.Calcule(playerCards, boardCards);
    /// 
    ///             Debug.Log("Hand: " + playerCards);
    ///             Debug.Log("Board " + boardCards);
    ///             Debug.Log("Hand Description: " + PokerEval.GetHandDescription(playerCards, boardCards));
    ///             Debug.Log("PPOT Player: " + Ppot.player.ToString());
    ///             Debug.Log("PPOT Opponent: " + Ppot.opponent.ToString());
    ///         }
    ///     }
    /// </code>   
    /// Using Simple metod
    /// <code>
    /// using UnityEngine;
    /// using System.Collections;
    /// using PokerCollections;
    /// using PokerCollections.Analysis;
    ///
    /// /*
    ///====READ-ME=================================================
    ///This method returns the approximate odd for the players mask winning against multiple opponents and no detalhed information. 
    ///This uses a default time duration of 0.25S (or 250mS) for the time allotment for Monte Carlo analysis.
    ///If you are interested in a full chance to report the percentage of each specific hand along with the total, 
    ///use the initialized class PPOT or PPOT_MonteCarlo
    /// */
    ///public class PpotMonteCarloSimple : MonoBehaviour
    ///    {
    ///        public string playerCards;
    ///        public string boardCards;
    ///        public int opponentCount;
    ///        [Header("ReadOnly")]
    ///        public string percentWinner = "";
    ///
    ///       void Start()
    ///        {
    ///            double percent = PPOT_MONTECARLO.GetWinnerPotential(playerCards, boardCards, opponentCount) * 100;
    ///           percentWinner = percent.ToString("N1") + "%";
    ///            Debug.Log(percentWinner);
    ///        }
    ///    }
    /// </code>
    /// </example>    
    public class PPOT_MONTECARLO
    {
        /// <summary>
        /// PPOT_Player é um container para guardar as informações espesificas sobre a probabilidade de vitoria
        /// apartir de todas as possiveis combinações de mãos do poker TexasHoldem
        /// </summary>
        public class PPOT_Player
        {
            /// <summary>
            /// Get HighCard winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            /// <see cref="Calcule"/>
            public double HighCard { get; private set; }
            /// <summary>
            /// Get Pair winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            /// <see cref="Calcule"/>
            public double Pair { get; private set; }
            /// <summary>
            /// Get Two Pair winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            public double TwoPair { get; private set; }
            /// <summary>
            /// Get ThreeOfKind winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            /// <see cref="Calcule"/>
            public double ThreeOfKind { get; private set; }
            /// <summary>
            /// Get Stratight winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            /// <see cref="Calcule"/>
            public double Stratight { get; private set; }
            /// <summary>
            /// Get Flush winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            /// <see cref="Calcule"/>
            public double Flush { get; private set; }
            /// <summary>
            /// Get FullHouse winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            /// <see cref="Calcule"/>
            public double FullHouse { get; private set; }
            /// <summary>
            /// Get FourOfKind winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            /// <see cref="Calcule"/> 
            public double FourOfKind { get; private set; }
            /// <summary>
            /// Get StraightFlush winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>
            /// <see cref="Calcule"/>
            public double StraightFlush { get; private set; }
            /// <summary>
            /// Get Total winner potential percent
            /// </summary>
            /// <note type="note">Use Calcule() Method to update values</note>  
            /// <see cref="Calcule"/>      
            public double totalWin { get; private set; }

            public void SetAsArray(double[] array)
            {
                HighCard = array[0] * 100;
                Pair = array[1] * 100;
                TwoPair = array[2] * 100;
                ThreeOfKind = array[3] * 100;
                StraightFlush = array[4] * 100;
                Flush = array[5] * 100;
                FullHouse = array[6] * 100;
                FourOfKind = array[7] * 100;
                StraightFlush = array[8] * 100;

                //Set Total
                foreach (double a in array)
                    totalWin += a;

                totalWin = totalWin * 100;

            }
            public override string ToString()
            {
                string logPlayer = "";

                logPlayer += ("HighCard " + HighCard.ToString("N1") + "%\n");
                logPlayer += ("Pair " + Pair.ToString("N1") + "%\n");
                logPlayer += ("TwoPair " + TwoPair.ToString("N1") + "%\n");
                logPlayer += ("ThreeOfKind " + ThreeOfKind.ToString("N1") + "%\n");
                logPlayer += ("Stratight " + Stratight.ToString("N1") + "%\n");
                logPlayer += ("Flush " + Flush.ToString("N1") + "%\n");
                logPlayer += ("FullHouse " + FullHouse.ToString("N1") + "%\n");
                logPlayer += ("FourOfKind " + FourOfKind.ToString("N1") + "%\n");
                logPlayer += ("StraightFlush " + StraightFlush.ToString("N1") + "%\n");
                logPlayer += ("TOTAL " + totalWin.ToString("N1") + "%\n");

                return logPlayer;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        public PPOT_Player player { get; private set; }        
        /// <summary>
        /// 
        /// </summary>
        public PPOT_Player opponent { get; private set; }
      
        //Player Pocket Cards       
        private string playerMask;        
        //Board pokect cards
        private string boardMask;        
        private int opponentsCount;
        private double timeDuration;
        /// <summary>
        /// Potential Positive odd tie, initializator 
        /// </summary>
        public PPOT_MONTECARLO()
        {
            // playerMask = _playerCards;
            // boardMask = _boardCards;
            player = new PPOT_Player();
            opponent = new PPOT_Player();
            opponentsCount = 1;
            timeDuration = 0.5;
        }
        private void _Calcule()
        {
            double[] playerodds;
            double[] oppodds;

            Hand.DetailedOddsWithMultiplePlayers(playerMask, boardMask, 0, opponentsCount, out playerodds, out oppodds, timeDuration);

            //Set player ppot values
            player.SetAsArray(playerodds);
            //Set opponent ppot values
            opponent.SetAsArray(oppodds);

        }
        public void Calcule(string _playerCards, string _boardCards)
        {

            List<Card> pc = PokerEval.EvalToCards(_playerCards);
            List<Card> bc = PokerEval.EvalToCards(_boardCards);

            //Checking for player hand cards error
            if (_playerCards.Length > 0 && pc.Count == 0)
            {
                Debug.LogError("player cards, eval formmat error");
                return;
            }
            else if (_playerCards.Length == 0)
            {
                Debug.LogError("poket cards from player can not empty or null");
                return;
            }

            //Checking for board cards error
            if (_boardCards.Length > 0 && bc.Count == 0)
            {
                Debug.LogError("board cards, eval formmat error");
                return;
            }


            playerMask = _playerCards;
            boardMask = _boardCards;

            _Calcule();

        }
        public void Calcule(int _opponentsCount, string _playerCards, string _boardCards)
        {
            List<Card> pc = PokerEval.EvalToCards(_playerCards);
            List<Card> bc = PokerEval.EvalToCards(_boardCards);

            //Checking for player hand cards error
            if (_playerCards.Length > 0 && pc.Count == 0)
            {
                Debug.LogError("player cards, eval formmat error");
                return;
            }
            else if (_playerCards.Length == 0)
            {
                Debug.LogError("poket cards from player can not empty or null");
                return;
            }

            //Checking for board cards error
            if (_boardCards.Length > 0 && bc.Count == 0)
            {
                Debug.LogError("board cards, eval formmat error");
                return;
            }

            if (_opponentsCount <= 0)
            {
                Debug.LogError("_playersCount can not zero");
                return;
            }

            playerMask = _playerCards;
            boardMask = _boardCards;
            opponentsCount = _opponentsCount;

            _Calcule();

        }
        public void Calcule(int _opponentsCount, string _playerCards, string _boardCards, double maxDuration)
        {
            List<Card> pc = PokerEval.EvalToCards(_playerCards);
            List<Card> bc = PokerEval.EvalToCards(_boardCards);

            //Checking for player hand cards error
            if (_playerCards.Length > 0 && pc.Count == 0)
            {
                Debug.LogError("player cards, eval formmat error");
                return;
            }
            else if (_playerCards.Length == 0)
            {
                Debug.LogError("poket cards from player can not empty or null");
                return;
            }

            //Checking for board cards error
            if (_boardCards.Length > 0 && bc.Count == 0)
            {
                Debug.LogError("board cards, eval formmat error");
                return;
            }

            if (_opponentsCount <= 0)
            {
                Debug.LogError("_playersCount can not zero");
                return;
            }

            playerMask = _playerCards;
            boardMask = _boardCards;
            opponentsCount = _opponentsCount;
            timeDuration = maxDuration;

            _Calcule();

        }
        public void Calcule(CardsList<Card> _playerCards, CardsList<Card> _boardCards)
        {

            //Checking for player hand cards error
            if (_playerCards.Count == 0)
            {
                Debug.LogError("player cards, eval formmat error");
                return;
            }

            playerMask = PokerEval.CardsToEval(_playerCards);

            if (_boardCards.Count == 0)
                boardMask = "";
            else
                boardMask = PokerEval.CardsToEval(_boardCards);

            _Calcule();
        }
        public void Calcule(int _opponentsCount, CardsList<Card> _playerCards, CardsList<Card> _boardCards)
        {

            //Checking for player hand cards error
            if (_playerCards.Count == 0)
            {
                Debug.LogError("player cards, eval formmat error");
                return;
            }

            playerMask = PokerEval.CardsToEval(_playerCards);

            if (_boardCards.Count == 0)
                boardMask = "";
            else
                boardMask = PokerEval.CardsToEval(_boardCards);

            opponentsCount = _opponentsCount;
            _Calcule();
        }
        public void Calcule(int _opponentsCount, CardsList<Card> _playerCards, CardsList<Card> _boardCards, double maxDuration)
        {

            if (_opponentsCount <= 0)
            {
                Debug.LogError("_playersCount can not zero");
                return;
            }

            //Checking for player hand cards error
            if (_playerCards.Count == 0)
            {
                Debug.LogError("player cards, eval formmat error");
                return;
            }

            playerMask = PokerEval.CardsToEval(_playerCards);

            if (_boardCards.Count == 0)
                boardMask = "";
            else
                boardMask = PokerEval.CardsToEval(_boardCards);

            opponentsCount = _opponentsCount;
            timeDuration = maxDuration;
            _Calcule();
        }
        public override string ToString()
        {
            string logPlayer = "Player: \n";
            string logOpponent = "Opponent: \n";


            logPlayer += ("HighCard " + player.HighCard.ToString("N0") + "%\n");
            logPlayer += ("Pair " + player.Pair.ToString("N0") + "%\n");
            logPlayer += ("TwoPair " + player.TwoPair.ToString("N0") + "%\n");
            logPlayer += ("ThreeOfKind " + player.ThreeOfKind.ToString("N0") + "%\n");
            logPlayer += ("Stratight " + player.Stratight.ToString("N0") + "%\n");
            logPlayer += ("Flush " + player.Flush.ToString("N0") + "%\n");
            logPlayer += ("FullHouse " + player.FullHouse.ToString("N0") + "%\n");
            logPlayer += ("FourOfKind " + player.FourOfKind.ToString("N0") + "%\n");
            logPlayer += ("StraightFlush " + player.StraightFlush.ToString("N0") + "%\n");
            logPlayer += ("TOTAL " + player.totalWin.ToString("N0") + "%\n");

            logOpponent += ("HighCard " + opponent.HighCard.ToString("N0") + "%\n");
            logOpponent += ("Pair " + opponent.Pair.ToString("N0") + "%\n");
            logOpponent += ("TwoPair " + opponent.TwoPair.ToString("N0") + "%\n");
            logOpponent += ("ThreeOfKind " + opponent.ThreeOfKind.ToString("N0") + "%\n");
            logOpponent += ("Stratight " + opponent.Stratight.ToString("N0") + "%\n");
            logOpponent += ("Flush " + opponent.Flush.ToString("N0") + "%\n");
            logOpponent += ("FullHouse " + opponent.FullHouse.ToString("N0") + "%\n");
            logOpponent += ("FourOfKind " + opponent.FourOfKind.ToString("N0") + "%\n");
            logOpponent += ("StraightFlush " + opponent.StraightFlush.ToString("N0") + "%\n");
            logOpponent += ("TOTAL " + opponent.totalWin.ToString("N0") + "%\n");

            return logPlayer + logOpponent;
        }
        /// <summary>
        /// This method returns the approximate odd for the players mask winning against
        /// multiple opponents. This uses a default time duration of 0.25S (or 250mS) for
        /// the time allotment for Monte Carlo analysis.
        /// </summary>
        /// <param name="pocket">The pocket mask of the player.</param>
        /// <param name="board">The current board cards</param>
        /// <param name="numOpponents">The number of oppoents 1-9 are legal values</param>
        /// <returns></returns>
        public static double GetWinnerPotential(string pocket, string board, int numOpponents)
        {
            return Hand.WinOdds(pocket, board, numOpponents);
        }
    }
}


