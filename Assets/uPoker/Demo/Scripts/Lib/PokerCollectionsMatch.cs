﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using uPoker;
using System.Security.Principal;

/*

Starting this library, you will be able to create and manage Poker match easily. 
This is an expansion of the library "PokerCollections", It has its own methods, and also methods of extensions to the PokerCollections library.
Expand functions of PokerCollections to create all the components necessary for the full development of your poker game.
With this expansion you will be able to relax and focus your attention on other areas of the development of your game, because here you will have everything you need to manage the rules of a game of poker, including the pot division, containing a fair award that each player have entitled at the end of each match!
Keep in mind that this library has all functions to ensure that you will be able to create and manage poker games in TexasOldem style, but if you have special needs that go beyond what we have here, you have complete freedom to continue to implement as your need

*/
namespace PokerCollections.MatchGame
{
    using PokerCollections;
    using Interfaces;
    using EventHandler;

    /// <summary>
    /// Additional extensions to Collections poker and the MatchGame Collections
    /// </summary>
    public static class MatchGameExtensions
    {
        /// <summary>
        /// Return the Current active <see cref="MatchGame"/> of the pokertable
        /// </summary>
        /// <param name="pokertable">PokerTable origin</param>
        /// <returns>
        /// Current MatchGame of the PokerTable or null if not exist
        /// <note type="note">
        /// Keep in mind that this method will only return the MatchGame with the isActive stats in true
        /// </note> 
        /// </returns>
        public static MatchGame Get_MatchGame(this PokerTable pokertable)
        {
            //if(pokertable==null)
            //   throw
            MatchGame match = MatchGameListner.Matches.Get_MatchFromPokerTable(pokertable);
            return match;
        }
        /// <summary>
        /// has current active game? This method return true if is current matchGame is started
        /// </summary>
        /// <param name="pokertable"></param>
        /// <returns>true of false</returns>
        public static bool HasCurrentActiveGame(this PokerTable pokertable)
        {
            bool result = MatchGameListner.Matches.Get_HasCurrentActiveMatch(pokertable);

            return result;

        }
        /// <summary>
        /// Return <see cref="MatchGame"/> List of the pokertable
        /// </summary>
        /// <param name="pokertable">
        /// PokerTable origin
        /// </param>
        /// <returns>
        /// MatchGame List of the pokertable or empty list
        /// </returns>
        public static List<MatchGame> Get_Matches(this PokerTable pokertable)
        {
            if (pokertable == null)
                return new List<MatchGame>();

            List<MatchGame> matches = MatchGameListner.Matches.Get_MatchesFromPokerTable(pokertable);

            return matches;
        }
        /// <summary>
        /// Find all <see cref="MatchGame"/> actived from own player <see cref="BetPlayer"/>
        /// </summary>
        /// <param name="betplayer">BetPlayer</param>
        /// <returns>Returns list of all the matches in which the player of origin is active</returns>
        public static List<MatchGame> Get_Matches(this BetPlayer betplayer)
        {
            List<MatchGame> matches = MatchGameListner.Matches.Get_MatchesFromPlayer(betplayer);
            if (matches == null)
                return new List<MatchGame>();

            return matches;
        }
        /// <summary>
        /// List of actions that the player has made so far in a match
        /// </summary>
        /// <param name="betplayer">Player Origin</param>
        /// <returns>MatchAction List of the Player</returns>
        public static List<MatchAction> Get_BetActions(this BetPlayer betplayer)
        {
            List<MatchAction> actions = null;
            try
            {
                MatchGame match = betplayer.pokerTable.Get_MatchGame();
                actions = MatchGameListner.Get_ActionsFromPlayer(betplayer, match);
                actions = actions.OrderBy(r => r.id).ToList();
            }
            catch (Exception ex)
            {
                actions = new List<MatchAction>();
                Debug.LogException(ex);
            }

            return actions;

        }
        /// <summary>
        /// List of actions that the player has made so far in a match
        /// </summary>
        /// <param name="betplayer">Player Origin</param>
        /// <param name="betType">Type of the bet</param>
        /// <returns>List of the player's actions with the specified type of bet</returns>
        public static List<MatchAction> Get_BetActions(this BetPlayer betplayer, BetType betType)
        {
            MatchGame match = betplayer.pokerTable.Get_MatchGame();
            List<MatchAction> actions = null;
            try
            {
                actions = MatchGameListner.Get_ActionsFromPlayer(betplayer, match, betType);
            }
            catch (Exception ex)
            {
                actions = new List<MatchAction>();
                Debug.LogException(ex);
            }

            return actions;

        }
        /// <summary>
        /// List of actions that the player made the turn specified
        /// </summary>
        /// <param name="betplayer">Player Origin</param>
        /// <param name="turn">Betturn to find</param>
        /// <returns>MatchAction of betPlayer from betTurn specified </returns>
        public static List<MatchAction> Get_BetActions(this BetPlayer betplayer, BetTurn turn)
        {

            List<MatchAction> actions = null;
            try
            {
                MatchGame match = betplayer.pokerTable.Get_MatchGame();
                actions = MatchGameListner.Get_ActionsFromPlayer(betplayer, match, turn);
            }
            catch (Exception ex)
            {
                actions = new List<MatchAction>();
                Debug.LogException(ex);
            }

            return actions;

        }
        /// <summary>
        /// Total actions taken by BetPlayer
        /// </summary>
        /// <param name="betplayer">BetPlayer Origin</param>
        /// <returns>Total shares of this player, until the point in the game</returns>
        public static int Get_ActionsCount(this BetPlayer betplayer)
        {
            return betplayer.Get_BetActions().Count();
        }
        /// <summary>
        /// Total actions taken by this BetPlayer on specified turn
        /// </summary>
        /// <param name="betplayer">Betplayer Origin</param>
        /// <param name="turn">BetTurn to find</param>
        /// <returns>Total actions from BetPlayer on specified turn</returns>
        public static int Get_ActionsCount(this BetPlayer betplayer, BetTurn turn)
        {
            return betplayer.Get_BetActions(turn).Count();
        }
        /// <summary>
        /// Total actions taken by this BetPlayer of the specified betType
        /// </summary>
        /// <param name="betplayer">Betplayer Origin</param>
        /// <param name="BetType">BetType to find</param>
        /// <returns>Total actions from BetPlayer on specified BetType</returns>
        public static int Get_ActionsCount(this BetPlayer betplayer, BetType betType)
        {
            return betplayer.Get_BetActions(betType).Count();
        }
        /// <summary>
        /// Informs a specified player is in a state of allin.. 
        /// </summary>
        /// <param name="betplayer">BetPlayer Origin</param>
        /// <returns>True if the player bets made equivalent to its total cash balance</returns>
        public static bool Is_Allin(this BetPlayer betplayer)
        {
            MatchGame match = betplayer.pokerTable.Get_MatchGame();
            List<MatchAction> actions = null;
            bool exist = false;
            try
            {
                actions = MatchGameListner.Get_ActionsFromPlayer(betplayer, match);
                exist = actions.Exists(r => r.isAllin);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }

            return exist;
        }
        /// <summary>
        /// Use this to receive the full amount of bets a BetPlayer made to the current point in the game
        /// </summary>
        /// <param name="betplayer">BetPlayer Origin</param>
        /// <returns>Bet amount of the BetPlayer</returns>
        public static double Get_BetsAmount(this BetPlayer betplayer)
        {
            List<MatchAction> actions = betplayer.Get_BetActions();
            double totalAmound = 0d;
            foreach (MatchAction a in actions)
            {

                totalAmound += a.bet_chips;
            }

            return totalAmound;
        }
        /// <summary>
        /// Use this to receive the full amount of bets a BetPlayer made to the turn of the game
        /// </summary>
        /// <param name="betplayer">BetPlayer Origin</param>
        /// <param name="turn">BetTurn to find</param>
        /// <returns>Bets amount from specified  turn</returns>
        public static double Get_BetsAmount(this BetPlayer betplayer, BetTurn turn)
        {
            List<MatchAction> actions = betplayer.Get_BetActions(turn);
            double totalAmound = 0d;
            foreach (MatchAction a in actions)
            {
                totalAmound += a.bet_chips;
            }

            return totalAmound;
        }
        /// <summary>
        /// Next action permitted on match.
        /// This action will evaluate the current turn and the previous actions to validate the next action. 
        /// If all shares are denied validation, null will be returned, meaning that the current turn is finalized
        /// </summary>
        /// <param name="allowAction">previous actions</param>
        /// <param name="match">current allowAction mananger</param>
        /// <returns>Allowed Actions or null if turn is on end</returns>
        public static MatchAllowedAction Next(this MatchAllowedAction allowAction)
        {

            MatchAction action = allowAction.matchGame.matchActions.Get_Last(allowAction.matchGame.turn);

            if (action == null)
                return allowAction;

            if (action.match_player.betPlayer != allowAction.matchGame.BetPlayer_Current)
                return allowAction;

            BetTurn turn = allowAction.matchGame.turn;

            //Jump to next player
            allowAction.matchGame.Overhide_CurrentBetPlayer = allowAction.matchGame.BetPlayer_Current.Next();

            allowAction = turn.Get_AllowAction(allowAction.matchGame);

            return allowAction;
        }
        /// <summary>
        ///Find the next valid player on current turn.
        /// FOLD and ALLIN BetPlayers are not considered active players and will not be returned.
        /// </summary>
        /// <param name="betPlayer">BetPlayer Origin</param>
        /// <returns>
        /// Returns the next valid player this turn or player himself if there is no longer any valid
        /// </returns>
        public static BetPlayer Next(this BetPlayer betPlayer)
        {
            MatchGame matchGame = betPlayer.pokerTable.Get_MatchGame();
            PokerTable pokerTable = matchGame.pokertable;
            BetPlayer next = pokerTable.betplayers.NextRel(betPlayer);

            //se o jogador escolhido estiver em fold vamos para o proximo
            for (int a = 0; a < pokerTable.betplayers.Count; a++)
            {
                if (next.Get_ActionsCount(BetType.fold) > 0 || next.Is_Allin() == true)
                {
                    next = pokerTable.betplayers.NextRel(next);
                }
                else
                {
                    break;
                }
            }

            if (next.Get_ActionsCount(BetType.fold) > 0 || next.Is_Allin() == true)
                next = betPlayer;

            return next;
        }
        /// <summary>
        ///Find the prev valid player on current turn.
        /// FOLD and ALLIN BetPlayers are not considered active players and will not be returned.
        /// </summary>
        /// <param name="betPlayer">BetPlayer Origin</param>
        /// <returns>
        /// Returns the prev valid player this turn or player himself if there is no longer any valid
        /// </returns>
        public static BetPlayer Prev(this BetPlayer betPlayer)
        {
            MatchGame matchGame = betPlayer.pokerTable.Get_MatchGame();
            PokerTable pokerTable = matchGame.pokertable;
            BetPlayer prev = pokerTable.betplayers.PrevRel(betPlayer);

            //se o jogador escolhido estiver em fold vamos para o proximo
            for (int a = 0; a < pokerTable.betplayers.Count; a++)
            {
                if (prev.Get_ActionsCount(BetType.fold) > 0 || prev.Is_Allin() == true)
                {
                    prev = pokerTable.betplayers.PrevRel(prev);
                }
                else
                {
                    break;
                }
            }

            if (prev.Get_ActionsCount(BetType.fold) > 0 || prev.Is_Allin() == true)
                prev = betPlayer;

            return prev;
        }
        /// <summary>
        /// BidderPlayer is the player whose actions make him the highest bidder until the moment.
        /// <note type="caution">If there is no valid next player the current bidder will be returned..</note>
        /// </summary>
        /// <param name="betPlayer">BetPlayer Origem</param>
        /// <returns>Returns another highest bidder except the player reference</returns>
        public static BetPlayer NextBidder(this BetPlayer betPlayer)
        {
            if (betPlayer == null)
                return null;

            BetPlayer result = betPlayer;

            PokerTable pokerTable = betPlayer.pokerTable;
            if (pokerTable == null)
                return betPlayer;

            MatchGame match = pokerTable.Get_MatchGame();

            if (match == null)
                return betPlayer;

            BetPlayer bidder = match.BetPlayer_Bidder;

            if (match.matchActions.Get_Count() == 0)
                return bidder;
            if (match.matchActions.Get_Count() == 1)
                return bidder;

            List<BetPlayer> players = pokerTable.betplayers.ToList();

            players.Remove(bidder);

            double maxValue = 0d;
            foreach (BetPlayer p in players)
            {
                double cv = p.Get_BetsAmount();
                if (cv > maxValue)
                    maxValue = cv;
            }


            List<BetPlayer> results = (from action in players
                                       where action.Get_BetsAmount() >= maxValue
                                       select action).ToList();

            //Removemos todas as entradas que não possuem ações. Assim previnimos erro
            //ao tentar reordenar a lista atraves do id de açãos dos jogadores
            results.RemoveAll(r => r.Get_BetActions() == null);

            results = results.OrderBy(r => r.Get_BetActions().LastOrDefault().id).ToList();

            result = results.LastOrDefault();

            if (result == null)
                result = bidder;

            return result;
        }
        /// <summary>
        /// Search and returns the next action in a source list starting from a reference action
        /// <note type="caution">If there is no next valid item in the list, Null will be returned</note>
        /// </summary>
        /// <param name="list">MatchAction List origin</param>
        /// <param name="source">MatchAction origin</param>
        /// <returns>Next valid MatchAction or null</returns>
        public static MatchAction Next(this List<MatchAction> list, MatchAction source)
        {
            int index = list.IndexOf(source);
            int next = index < list.Count - 1 ? index + 1 : -1;
            MatchAction item = list[next];
            return item;

        }
        /// <summary>
        /// Search and returns the next action in a generic source list <see cref="MatchGame.matchActions"/>starting from a reference action
        /// <note type="caution">If there is no next valid item in the list, Null will be returned</note>
        /// </summary>
        /// <param name="source">MatchAction origin</param>
        /// <returns>Next valid MatchAction or null</returns>
        public static MatchAction Next(this MatchAction source)
        {
            List<MatchAction> list = source.match_player.match.matchActions.Get_Actions();
            int index = list.IndexOf(source);
            int next = index < list.Count - 1 ? index + 1 : -1;
            MatchAction item = list[next];
            list.Clear();
            return item;

        }
        /// <summary>
        /// Search and returns the next action in a source MatcAction, starting from a reference list.
        /// <note type="caution">If there is no next valid item in the list, Null will be returned</note>
        /// </summary>       
        /// <param name="source">MatchAction origin</param>
        /// <param name="list">List reference</param>
        /// <returns>Next valid MatchAction or null</returns>
        public static MatchAction Next(this MatchAction source, List<MatchAction> list)
        {
            int index = list.IndexOf(source);
            int next = index < list.Count - 1 ? index + 1 : -1;
            MatchAction item = list[next];
            list.Clear();
            return item;

        }
        /// <summary>
        /// Search and returns the prev action in a source list starting from a reference action
        /// <note type="caution">If there is no prev valid item in the list, Null will be returned</note>
        /// </summary>
        /// <param name="list">MatchAction List origin</param>
        /// <param name="source">MatchAction source reference</param>
        /// <returns>Prev valid MatchAction or null</returns>
        public static MatchAction Prev(this List<MatchAction> list, MatchAction source)
        {
            int index = list.IndexOf(source);
            int prev = index > 0 ? index - 1 : -1;
            MatchAction item = list[prev];
            return item;

        }
        /// <summary>
        /// Search and returns the prev action in a generic source list <see cref="MatchGame.matchActions"/>starting from a reference action
        /// <note type="caution">If there is no prev valid item in the list, Null will be returned</note>
        /// </summary>
        /// <param name="source">MatchAction origin</param>
        /// <returns>Prev valid MatchAction or null</returns>
        public static MatchAction Prev(this MatchAction source)
        {
            List<MatchAction> list = source.match_player.match.matchActions.Get_Actions();
            int index = list.IndexOf(source);
            int prev = index > 0 ? index - 1 : -1;
            MatchAction item = list[prev];
            list.Clear();
            return item;

        }
        /// <summary>
        /// Search and returns the prev action in a source MatcAction, starting from a reference list.
        /// <note type="caution">If there is no prev valid item in the list, Null will be returned</note>
        /// </summary>       
        /// <param name="source">MatchAction origin</param>
        /// <param name="list">List reference</param>
        /// <returns>Prev valid MatchAction or null</returns>
        public static MatchAction Prev(this MatchAction source, List<MatchAction> list)
        {
            int index = list.IndexOf(source);
            int prev = index > 0 ? index - 1 : -1;
            MatchAction item = list[prev];
            list.Clear();
            return item;

        }
        /// <summary>
        /// Allowed Actions in the current turn
        /// <note type="note">
        /// Whenever this method is called, the next player will be upgraded to a next valid player, analyzed by the system. 
        /// You can intercept this event through the corresponding event
        /// </note>
        /// </summary>
        /// <returns>Classe de ações permitidas para o jogador da vez atualizado</returns>
        public static MatchAllowedAction Get_AllowAction(this BetTurn turn, MatchGame matchGame)
        {
            //se este metodo for chamado quando não houver nenhuma outra acao na partida, é necessário confirmar quem é o
            //jogador da vez vez antes.
            ActionBet bet = new ActionBet(matchGame);
            ActionCall call = new ActionCall(matchGame);
            ActionCheck check = new ActionCheck(matchGame);
            ActionFold fold = new ActionFold(matchGame);
            ActionRaise raise = new ActionRaise(matchGame);

            BetPlayer betplayer = matchGame.BetPlayer_Current;
            MatchAllowedAction result = new MatchAllowedAction(matchGame, bet, call, check, raise, fold, betplayer);

            return result;
        }
        /// <summary>
        /// Highest bidder of turn
        /// </summary>
        /// <param name="turn">Current Turn</param>
        /// <param name="matchGame">MatchGame origin</param>
        /// <returns> 
        /// Returns the player whose actions make him the highest bidder this round
        /// <note type="caution">
        /// If exist more than one player with the same betting value the system will return the player with "placId" higher
        /// </note>
        /// </returns>
        public static BetPlayer Get_BetPlayer_Bidder(this BetTurn turn, MatchGame matchGame)
        {
            return matchGame.BetPlayer_BidderOn(turn);
        }
        /// <summary>
        /// Returns the last action performed in this MatchGame and in specified turn
        /// </summary>
        /// <param name="turn">Current turn</param>
        /// <param name="matchGame">MatchGame origin</param>
        /// <returns></returns>
        public static MatchAction Get_LastAction(this BetTurn turn, MatchGame matchGame)
        {
            return matchGame.matchActions.Get_Last(turn);
        }
        /// <summary>
        /// List of active players from source list
        /// <note type="caution">FOLD and AllIn Player shall not be considered as assets</note>
        /// </summary>
        /// <param name="betplayers">BetPlayer List origen</param>
        /// <returns>List of active players from source list</returns>
        public static List<BetPlayer> Get_ActivePlayers(this List<BetPlayer> betplayers)
        {
            List<BetPlayer> results = betplayers.ToList();
            results.RemoveAll(r => r.Get_ActionsCount(BetType.fold) > 0);
            results.RemoveAll(r => r.Is_Allin());

            return results;

        }
        /// <summary>
        /// If the player made bets that have not been paid by the other players of the game you can find out if he has bet values to be returned, starting this method
        /// </summary>
        /// <param name="betPlayer">BetPlayer origin</param>
        /// <returns>Zero or any value corresponding to player bet that was not paid by the other players!</returns>
        public static double Get_PoteContribReturn(this BetPlayer betPlayer)
        {
            double result = 0;
            PokerTable pokertable = betPlayer.pokerTable;

            if (pokertable == null)
                return result;

            MatchGame match = pokertable.Get_MatchGame();

            if (match == null)
                return result;

            if (betPlayer == match.BetPlayer_Bidder)
                result = match.matchActions.Get_BidderChipsReturn();

            return result;
        }
        /// <summary>
        /// Total amount raised from all bets in the game
        /// </summary>
        /// <param name="game">Current Game</param>
        /// <returns>Total amount raised from all bets in the game</returns>
        public static double Get_PoteTotal(this MatchGame game)
        {
            double totalPote = 0;
            try
            {
                if (game != null)
                {
                    totalPote = game.matchActions.Get_ChipsAmount();
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }

            return totalPote;
        }
        /// <summary>
        /// Informa se o jogador já efetuou uma aposta voluntária.
        /// <note type="note">
        /// Preflop round, there are some actions that are automatic system for small and big player.
        /// These players are considered "non-voluntary" because they actions are carried out even without the will of the player.
        /// When the player making your first bet manually, this player will become a volunteer player!
        /// </note>
        /// </summary>
        /// <param name="betPlayer">BetPlayer Origin</param>
        /// <returns>True to voluntary and False to non-voluntary</returns>
        public static bool hasVoluntaryBet(this BetPlayer betPlayer)
        {
            bool result = betPlayer.Get_BetActions().Exists(r => r.isVoluntary == true);
            return result;
        }
        /// <summary>
        /// Returns list of players eligible to receive awards until the time of the game.
        /// Players in FOLD are considered non-legible and will not be returned in this list.
        /// </summary>
        /// <param name="actions">
        /// Current actions to evaluated
        /// </param>
        /// <returns>
        /// Returns list of players eligible to receive awards until the time of the game.
        /// </returns>
        public static List<BetPlayer> Get_ActiveBetPlayers(this MatchActions actions)
        {
            MatchGame match = actions.matchGame;
            List<BetPlayer> betplayers = match.pokertable.betplayers.ToList();
            betplayers.RemoveAll(r => r.Get_ActionsCount(BetType.fold) > 0);
            return betplayers;
        }

    }
    /// <summary>
    /// Represents a special type list of Match Game objects that can be accessed by index. 
    /// Provides methods to search, sort, and manipulate lists.
    /// Using this list method, you can attach functions to listen to specific events list manipulation!
    /// </summary>
    /// <typeparam name="MatchGame">The type of elements in the list.</typeparam>
    public class MatchGameList<MatchGame> : List<MatchGame> where MatchGame : IPokerMatch
    {
        private int maxCapacity = 0;
        private int autoID = 0;

        protected virtual void Event_OnAdd(MatchEventArgs e)
        {
            EventHandler_Matches<MatchEventArgs> handler = OnAdd;
            if (handler != null)
            {
                handler(e);
            }
        }
        protected virtual void Event_OnRemove(MatchEventArgs e)
        {
            EventHandler_Matches<MatchEventArgs> handler = OnRemove;
            if (handler != null)
            {
                handler(e);
            }
        }

        public event EventHandler_Matches<MatchEventArgs> OnAdd;
        public event EventHandler_Matches<MatchEventArgs> OnRemove;

        /// <summary>
        /// Parent component 
        /// </summary>
        public object component { get; private set; }
        /// <summary>
        /// Start a new list of instance from an existing list. You can use a different type of list to start if you want this!
        /// </summary>
        /// <param name="source"> the source list</param>
        public MatchGameList(IEnumerable<MatchGame> source)
        {
            maxCapacity = 0;
            if (this.GetType() != source.GetType())
            {
                foreach (MatchGame t in source)
                {
                    base.Add(t);
                }
            }

        }
        /// <summary>
        /// Start a new list instance
        /// </summary>
        /// <param name="_maxCapacity"> max capacity allowed for new elements</param>
        public MatchGameList(int _maxCapacity)
        {
            maxCapacity = _maxCapacity;
        }
        /// <summary>
        /// Start a new list instance
        /// </summary>
        public MatchGameList()
        {
            maxCapacity = 0;
        }
        /// <summary>
        /// Start a new list instance with parent component attched
        /// </summary>
        /// <param name="context">Component attached</param>
        public MatchGameList(object context)
        {
            component = context;
        }

        /// <summary>
        /// Item Next
        /// Search and return the next list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note>
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the next list item from the current item or returns the current item if there is no item in the list after him!</returns>
        public MatchGame Next(MatchGame current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<MatchGame> matches = this.ToList();
            matches = matches.OrderBy(r => r.id).ToList();

            MatchGame c = default(MatchGame);

            if (Count <= 0)
                return c;

            int index = matches.IndexOf(current);

            if (index < Count - 1)
                return matches.ElementAt(index + 1);

            return current;
        }
        /// <summary>
        /// Item Previous
        /// Search and return the previous list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the previous list item from the current item or returns the current item if there is no item in the list before him!</returns>
        public MatchGame Prev(MatchGame current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<MatchGame> match = this.ToList();
            match = match.OrderBy(r => r.id).ToList();
            MatchGame c = default(MatchGame);

            if (Count <= 0)
                return c;

            int index = match.IndexOf(current);

            if (index >= 1)
                return match.ElementAt(index - 1);
            return current;
        }
        /// <summary>
        /// Item Relative Next:
        /// Search and return the next list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the next list item from the current item or returns the first item if there is no item in the list after him!</returns>
        public MatchGame NextRel(MatchGame current)
        {
            MatchGame c = default(MatchGame);
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<MatchGame> matches = this.ToList();
            matches = matches.OrderBy(r => r.id).ToList();

            if (Count <= 0)
                return c;

            if (Count > 1)
            {
                int index = matches.IndexOf(current);

                if (index < Count - 1)
                {
                    return matches.ElementAt(index + 1);
                }
                else
                {
                    return matches.FirstOrDefault();
                }
            }
            else
            {
                return current;
            }
        }
        /// <summary>
        /// Item Relative Previous:
        /// Search and return the prev list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the prev list item from the current item or returns the last item if there is no item in the list after him!</returns>
        public MatchGame PrevRel(MatchGame current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<MatchGame> matches = this.ToList();
            matches = matches.OrderBy(r => r).ToList();

            MatchGame c = default(MatchGame);

            if (Count <= 0)
                return c;

            if (Count > 1)
            {
                int index = matches.IndexOf(current);

                if (index >= 1)
                {
                    return matches.ElementAt(index - 1);
                }
                else
                {
                    return matches.LastOrDefault();
                }
            }
            else
            {
                return current;
            }
        }
        /// <summary>
        /// Item Relative Next:
        /// Search and return the next list item from current item place id reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="placeID"> Current Item place id</param>
        /// <returns>returns the next list item from the current item or returns the first item if there is no item in the list after him!</returns>
        public MatchGame NextRel(int placeID)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<MatchGame> macthes = this.ToList();
            macthes = macthes.OrderBy(r => r.id).ToList();


            MatchGame cur = default(MatchGame);

            for (int a = placeID; a < 9; a++)
            {
                int p = a + 1;
                cur = macthes.Find(r => r.id == p);
                if (cur != null)
                    break;
            }

            if (cur == null)
            {
                for (int a = 0; a < 9; a++)
                {
                    int p = a + 1;
                    cur = macthes.Find(r => r.id == p);
                    if (cur != null)
                        break;
                }
            }

            if (cur == null)
                return macthes.FirstOrDefault();
            else
                return cur;

        }
        /// <summary>
        /// Item Relative Previous:
        /// Search and return the prev list item from current item place id reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="placeID"> Current Item place id</param>
        /// <returns>returns the prev list item from the current item or returns the last item if there is no item in the list after him!</returns>
        public MatchGame PrevRel(int placeID)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<MatchGame> matches = this.ToList();
            matches = matches.OrderBy(r => r.id).ToList();


            MatchGame cur = default(MatchGame);

            for (int a = placeID; a > 1; a--)
            {
                int p = a - 1;
                cur = matches.Find(r => r.id == p);
                if (cur != null)
                    break;
            }

            if (cur == null)
            {
                for (int a = 10; a > 1; a++)
                {
                    int p = a - 1;
                    cur = matches.Find(r => r.id == p);
                    if (cur != null)
                        break;
                }
            }

            if (cur == null)
                return matches.LastOrDefault();
            else
                return cur;

        }
        /// <summary>
        /// Get the first Item of the List
        /// </summary>
        /// <returns>First Item of the List or null if empty list</returns>
        public MatchGame First()
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<MatchGame> matches = this.ToList();
            matches = matches.OrderBy(r => r.id).ToList();

            MatchGame c = default(MatchGame);

            if (Count <= 0)
                return c;

            return matches.FirstOrDefault();
        }
        /// <summary>
        /// Get the last Item of the List
        /// </summary>
        /// <returns>Last item of the List or null if empty list</returns>
        public MatchGame Last()
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<MatchGame> matches = this.ToList();
            matches = matches.OrderBy(r => r.id).ToList();

            MatchGame c = default(MatchGame);

            if (Count <= 0)
                return c;

            return matches.LastOrDefault();
        }

        //Extend Methods to Search
        /// <summary>
        /// Returns a list of matches that the player is active
        /// </summary>
        /// <param name="betplayer">BetPlayer origin</param>
        /// <returns>MatchGame Generic List or empty llist</returns>
        public List<MatchGame> Get_MatchesFromPlayer(BetPlayer betplayer)
        {
            List<MatchGame> matches = this.ToList().Where(r => r.pokertable.betplayers.Exists(a => a == betplayer) == true).ToList();

            if (matches == null)
                matches = new List<MatchGame>();

            return matches;
        }
        /// <summary>
        /// MatchGame List from pokerTable 
        /// </summary>
        /// <param name="pokertable">PokerTable origin</param>
        /// <returns>MatchGame List from pokertable or e empty List if not exist</returns>
        public List<MatchGame> Get_MatchesFromPokerTable(PokerTable pokertable)
        {
            List<MatchGame> matches = this.ToList().Where(r => r.pokertable.id == pokertable.id).ToList();
            if (matches == null)
                matches = new List<MatchGame>();
            return matches;
        }
        /// <summary>
        /// Current active MatchGame from pokerTable
        /// </summary>
        /// <param name="pokertable">PokerTable origin</param>
        /// <returns>
        /// Active MatchGame from pokertable or Null if not exist
        /// <note type="note">
        /// Keep in mind that this method will only return the MatchGame with the isActive stats in true
        /// </note>
        /// </returns>
        public MatchGame Get_MatchFromPokerTable(PokerTable pokertable)
        {
            MatchGame result = default(MatchGame);

            List<MatchGame> matches = this.ToList();

            if (matches.Count > 0)
            {
                result = matches.Last(r => r.pokertable == pokertable);

                //if (matches.Count > 0)

                if (result != null)
                    if (result.isActive == false)
                        result = default(MatchGame);

            }

            return result;

        }
        /// <summary>
        ///  Current active MatchGame from pokerTable ID
        /// </summary>
        /// <param name="tableID">ID to find</param>
        /// <returns> 
        /// Current MatchGame from pokerTable ID or NULL if not exist
        /// <note type="note">
        /// Keep in mind that this method will only return the MatchGame with the isActive stats in true
        /// </note> 
        /// </returns>
        public MatchGame Get_MatchFromPokerTable(int tableID)
        {
            List<MatchGame> matches = this.ToList();
            MatchGame result = default(MatchGame);

            if (matches.Count > 0)
            {
                result = matches.Last(r => r.pokertable.id == tableID);

                if (result != null)
                    if (result.isActive == false)
                        result = default(MatchGame);
            }
            return result;
        }
        /// <summary>
        ///  Current active MatchGame from MonoBehavior component
        /// </summary>
        /// <param name="component">component attached to find</param>
        /// <returns>
        /// Current MatchGame from MonoBehavior component or NULL if not exist
        /// <note type="note">
        /// Keep in mind that this method will only return the MatchGame with the isActive stats in true
        /// </note> 
        /// </returns>
        public MatchGame Get_MatchFromComponent(MonoBehaviour component)
        {
            List<MatchGame> matches = this.ToList();
            MatchGame result = default(MatchGame);

            if (matches.Count > 0)
                result = matches.Last(r => r.component == component);

            return result;
        }
        /// <summary>
        /// Has current active game in pokerTable
        /// </summary>
        /// <returns>true if exist or false is not</returns>
        public bool Get_HasCurrentActiveMatch(PokerTable pokertable)
        {
            bool result = false;
            List<MatchGame> matches = Get_MatchesFromPokerTable(pokertable);
            if (matches.Count > 0)
            {
                MatchGame last = matches.Last();
                if (last != null)
                    result = last.isActive;
            }
            return result;
        }
        /// <summary>
        /// Add new BetPlayer Item to the List.
        /// <note type="note"> 
        /// if <see cref="component"/> is a PokerTable, the <see cref="BetPlayer.pokerTable"/> will be updated automatically
        /// <para>Triggers Event: <see cref="OnAdd"/></para>
        /// <para>Change all prev matches progress in list to false and set true to current match add</para>
        /// </note>
        /// </summary>
        /// <param name="_match">BetPlayer Item to add</param>
        public new void Add(MatchGame _match)
        {
            if (maxCapacity > 0 && Count >= maxCapacity)
                return;

            if (Exists(r => r.Equals(_match)) == false)
            {
                _match.overhide_id = ++autoID;

                //Change all prev matches progress stats to false
                foreach (MatchGame game in this)
                    game.SetActive(false);

                //Change current match progress stats to true
                _match.SetActive(true);

                base.Add(_match);


                //if (component != null)
                //{
                // if (component.GetType() == typeof(PokerTable))
                //     _match.overhidePokertable = (PokerTable)component;
                //}

                MatchEventArgs args = new MatchEventArgs(_match);
                Event_OnAdd(args);
            }

        }
        /// <summary>
        /// Clar all items of the betPlayers list
        /// <note type="note"> 
        /// Triggers Event: <see cref="OnRemove"/> to eatch item.
        /// </note>
        /// </summary>
        public new void Clear()
        {
            List<MatchGame> list = new List<MatchGame>(this);
            foreach (MatchGame p in list)
            {
                Remove(p);
            }
            list.Clear();
            base.Clear();
        }
        /// <summary>
        /// Remove the current item of the list
        /// <note type="note"> 
        /// If <see cref="component"/> is a PokerTable, the <see cref="BetPlayer.pokerTable"/> will be update automatically
        /// Triggers Event: <see cref="OnRemove"/>;
        /// </note>
        /// </summary>
        /// <param name="_match"></param>
        /// <returns>true if success and false if fail</returns>
        public new bool Remove(MatchGame _match)
        {
            _match.SetActive(false);
            bool result = base.Remove(_match);
            if (result == true)
            {
                if (component != null)
                {
                    //if (component.GetType() == typeof(PokerTable))
                    //    _match.overhidePokertable = null;
                }

                MatchEventArgs args = new MatchEventArgs(_match);
                Event_OnRemove(args);
            }
            return result;
        }
    }
    /// <summary>
    /// Classifies the pot division type used on game
    /// </summary>
    public enum MatchPoteType
    {
        limit = 1,
        no_limit = 2,
    }
    /// <summary>
    /// Classifies the type of entity owns a share, TABLE or PLAYER
    /// </summary>
    public enum MatchActionEntity
    {
        board = 1,
        player = 2,
    }
    /// <summary>
    ///Categorizes the evaluation type 
    /// </summary>
    public enum EvaluatedType
    {
        winner,
        tie,
        loose,
        recoins,
    }
    /// <summary>
    /// Evaluates the permission to use the BET command in a game managed by <see cref="MatchGame"/>.
    /// </summary>
    public class ActionBet
    {
        private bool _allow;
        public bool allow
        {
            get
            {
                if (matchGame != null)
                {
                    verify();
                    return _allow;
                }
                else
                {
                    return false;

                }
            }
        }
        public double bet_min { get; private set; }
        public double bet_max { get; private set; }

        private MatchGame matchGame;

        public ActionBet(MatchGame matchGame)
        {
            bet_min = 0;
            bet_max = 0;
            _allow = false;
            this.matchGame = matchGame;
            verify();
        }

        /// <summary>
        /// Valida e adiciona uma nova ação <see cref="MatchAction"/> à lista de ações do jogador, 
        /// descontando automaticamente o valor espesificado dos fundos do jogador.
        /// Utilize o evento <see cref=" MatchGameListner.Listner_OnBetAction()"/> para escutar o evento de ações do jogador.
        /// <note type="note">Se o valor real a ser descontado do jogador com esta ação for superior ao que o jogador possui, 
        /// esta ação sera setada como ALLIN
        /// </note>
        /// </summary>
        /// <param name="value">Valor desejado para a aposta. 
        /// Se a aposta for inferior ao <see cref="bet_min"/> mas o jogador possuir esta quantia, este valor sera setado automaticamente
        /// para a aposta. Caso contrario ALLIN sera setado para esta ação e o valor apostado permanecerá.
        /// Se a aposta for superior ao <see cref="bet_max"/> o valor máximo permitido sera setado para esta ação.
        /// Se em ambos os casos o jogador não possuir saldo suficiente em dinheiro, ALLIN sera setado a esta ação e valor total que
        /// o jogador possui sera setado à esta aposta!
        /// </param>
        /// <returns>
        /// Retorna a ação validade e adcionada à lista de ações do jogador
        /// </returns>
        public MatchAction Execute(double value)
        {
            verify();

            if (!_allow)
            {
                throw new Exception("Action Denny, you no have permision to execute ActionBet");
            }

            MatchAction result = null;
            BetPlayer currentBetPlayer = matchGame.BetPlayer_Current;

            double total = value;
            //Valor que podera sobrar do desconto total pretendido e o total que o jogador possui
            double sub = 0;
            //Valor real descontado do jogador
            double realsub = 0;
            bool allin = false;
            bool voluntary = true;
            if (total > bet_max)
                total = bet_max;
            else if (total < bet_min)
                total = bet_min;

            //Sub sera maior q 0 caso exista sobras na subtração do total, indicando que o jogador possuia a quantidade
            //necessário para o desconto total.
            sub = currentBetPlayer.SubChip(total);

            if (currentBetPlayer.chipsAmount <= 0)
                allin = true;

            realsub = total - sub;

            //Verificando ação voluntaria
            if (matchGame.turn == BetTurn.preflop)
                if (matchGame.matchActions.Get_Count(matchGame.turn) == 0 || matchGame.matchActions.Get_Count(matchGame.turn) == 1)
                    voluntary = false;


            result = matchGame.matchActions.AddBetAction(currentBetPlayer, BetType.bet, realsub, currentBetPlayer.Get_BetsAmount(matchGame.turn) + realsub, allin, voluntary);
            return result;


        }
        /// <summary>
        /// Esta ação sera permitida somente quando não houver nenhuma aposta no turno atual da mesa.
        /// Esta ação equivale à primeira ação do turno, dando inicio a rodada de apostas dos outros jogadores.
        /// </summary>
        /// <param name="matchGame"></param>
        private void verify()
        {
            _allow = false;



            //Não existem mais jogadores ativos na partida
            if (matchGame.pokertable.betplayers.Get_ActivePlayers().Count <= 1)
                return;


            //Ação duplicada. Jogador tentando executar a mesma acao duas vezes
            if (matchGame.turn.Get_LastAction(matchGame) != null)
                if (matchGame.turn.Get_LastAction(matchGame).match_player.betPlayer == matchGame.BetPlayer_Current)
                    return;

            //Se ja houver uma aposta check no preflop. as acoes de turno finalizaram, pois o big ja deu sua ação voluntaria
            //de fechamento de turno.
            if (matchGame.turn == BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn, BetType.check) > 0)
                return;

            //Jogador ja cubriu a aposta do maior apostador neste turno 
            if (matchGame.BetPlayer_BidderOn(matchGame.turn) != null)
                if (matchGame.BetPlayer_BidderOn(matchGame.turn).Get_BetsAmount(matchGame.turn) <= matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn))
                    return;


            MatchAction lastAction = matchGame.matchActions.Get_Last(matchGame.turn);

            if (lastAction != null) //Primeira ação deste turno.
            {
                if (lastAction.bet_action != BetType.check)
                    return;
            }



            bet_min = matchGame.config.smallBlind;

            if (matchGame.turn == BetTurn.preflop)
                bet_max = bet_min;
            else
                bet_max = matchGame.BetPlayer_Current.chipsAmount;


            _allow = true;

        }
    }
    /// <summary>
    /// Evaluates the permission to use the CALL command in a game managed by <see cref="MatchGame"/>.
    /// </summary>
    public class ActionCall
    {
        private bool _allow;
        public bool allow
        {
            get
            {
                if (matchGame != null)
                {
                    verify();
                    return _allow;
                }
                else
                {
                    return false;

                }
            }
        }
        public double bet_min { get; private set; }
        public double bet_max { get; private set; }
        public MatchGame matchGame { get; private set; }

        public ActionCall(MatchGame matchGame)
        {
            bet_min = 0;
            bet_max = 0;
            _allow = false;
            this.matchGame = matchGame;
            verify();
        }
        public MatchAction Execute()
        {
            verify();

            if (!_allow)
                throw new Exception("Action Denny, you no have permision to execute ActionCall");

            MatchAction result = null;
            BetTurn turn = matchGame.turn;

            BetPlayer currentBetPlayer = matchGame.BetPlayer_Current;
            BetPlayer bidderPlayer = turn.Get_BetPlayer_Bidder(matchGame);

            double total = bidderPlayer.Get_BetsAmount(matchGame.turn) - currentBetPlayer.Get_BetsAmount(matchGame.turn);
            //Valor que podera sobrar do desconto total pretendido e o total que o jogador possui
            double sub = 0;
            //Valor real descontado do jogador
            double realsub = 0;
            bool allin = false;
            bool voluntary = true;

            //Sub sera maior q 0 caso exista sobras na subtração do total, indicando que o jogador possuia a quantidade
            //necessário para o desconto total.
            sub = currentBetPlayer.SubChip(total);

            if (currentBetPlayer.chipsAmount <= 0)
                allin = true;

            realsub = total - sub;

            result = matchGame.matchActions.AddBetAction(currentBetPlayer, BetType.call, realsub, currentBetPlayer.Get_BetsAmount(matchGame.turn) + realsub, allin, voluntary);
            return result;
        }
        private void verify()
        {
            /* COMANDO CALL
                           
                 O Comando de pagar sera permitido sempre que houver uma aposta na mesa maior do que a minha atual
                 > Se o turno for PREFLOP, o numero de ações devera ser obrigatoriamente maior ou igual a 2, isto significa
                 que a acao call so sera liberada no preflop, quando o raize obrigatorio ja tiver sido liberado.  
                > O Jogador da vez devra ser diferente do ultimo jogador a ter apostado no turno.
                > O Valor de CALL é o total do grande apostador subtraido do toatl que o jogador ja apostou no turno
            */


            _allow = false;

            if (matchGame.matchActions.Get_Count(matchGame.turn) <= 0)//Não existe apostas na mesa para serem pagas
                return;


            //Não existem mais jogadores ativos na partida
            if (matchGame.pokertable.betplayers.Get_ActivePlayers().Count <= 1)
            {
                //Esta acao so sera ativa se eu ainda tiver que pagar algum bidder.
                //Em alguns casos o Bidder pode entrar em ALLIN deixando somente o jogador da vez ativo. 
                //Neste caso o jogador da vez ainda tera este comando valido para responder ao bidder
                BetPlayer bidder = matchGame.BetPlayer_BidderOn(matchGame.turn);
                if (bidder == null || bidder == matchGame.BetPlayer_Current)
                    return;

                if (bidder.Get_BetsAmount(matchGame.turn) <= matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn))
                    return;
            }

            //Ação duplicada. Jogador tentando executar a mesma acao duas vezes
            if (matchGame.turn.Get_LastAction(matchGame) != null)
                if (matchGame.turn.Get_LastAction(matchGame).match_player.betPlayer == matchGame.BetPlayer_Current)
                    return;

            //Se ja houver uma aposta check no preflop. as acoes de turno finalizaram, pois o big ja deu sua ação voluntaria
            //de fechamento de turno.
            if (matchGame.turn == BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn, BetType.check) > 0)
                return;

            if (matchGame.matchActions.Get_Last(matchGame.turn).bet_action == BetType.check) //Se a ultima acao do turno foi um check, nao existem apostas para pagar
                return;

            MatchAction lastAction = matchGame.matchActions.Get_Last(matchGame.turn);       //Ultima ação da mesa
            BetPlayer BidderPlayer = matchGame.BetPlayer_BidderOn(matchGame.turn);          //Maior apostador do turno

            if (matchGame.turn == BetTurn.preflop)
            {

                if (matchGame.matchActions.Get_Count(matchGame.turn) < 2)//No Preflop a acao call so sera liberada depois do raize obrigatorio da mesa
                    return;
            }
            else
            {

                if (matchGame.matchActions.Get_Count(matchGame.turn) <= 0)//Nos demais turno, so sera liberado havendo mais ações na mesa
                    return;
            }

            if (lastAction.match_player.betPlayer == matchGame.BetPlayer_Current) //Jogador da vez é o ultimo com ação valida
                return;

            if (BidderPlayer == matchGame.BetPlayer_Current)//O Maior apostador é o prorio jogador da vez
                return;

            //Jogador ja cubriu a aposta do maior apostador neste turno
            if (BidderPlayer.Get_BetsAmount(matchGame.turn) <= matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn))
                return;

            double requiredCall = BidderPlayer.Get_BetsAmount(matchGame.turn) - matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn);
            //double sub = matchGame.BetPlayer_Current.SubChip(requiredCall);

            bet_min = requiredCall;
            bet_max = requiredCall;
            _allow = true;


        }

    }
    /// <summary>
    /// Evaluates the permission to use the RAISE command in a game managed by <see cref="MatchGame"/>.
    /// </summary>
    public class ActionRaise
    {
        private bool _allow;
        public bool allow
        {
            get
            {
                if (matchGame != null)
                {
                    verify();
                    return _allow;
                }
                else
                {
                    return false;

                }
            }
        }
        public double bet_min { get; private set; }
        public double bet_max { get; private set; }
        private MatchGame matchGame;
        public ActionRaise(MatchGame matchGame)
        {
            bet_min = 0;
            bet_max = 0;
            this.matchGame = matchGame;
            _allow = false;

            verify();
        }

        /// <summary>
        /// Valida e adiciona uma nova ação <see cref="MatchAction"/> à lista de ações do jogador, 
        /// descontando automaticamente o valor espesificado dos fundos do jogador.
        /// Utilize o evento <see cref=" MatchGameListner.Listner_OnBetAction()"/> para escutar o evento de ações do jogador.
        /// <note type="note">Se o valor real a ser descontado do jogador com esta ação for superior ao que o jogador possui, 
        /// esta ação sera setada como ALLIN.   
        ///
        /// PS: Eu axo que o melhor é desativar esta acao quando so houver o jogador da vez ativo.
        /// Isso significa que o jogador da vez nao podera aumentar uma aposta quando não houver mais jogadores ativos.
        /// Por padrao estou permitindo mas seria legal desativar no futuro
        /// 
        /// </note>
        /// </summary>
        /// <param name="value">Valor desejado para a aposta. 
        /// Se a aposta for inferior ao <see cref="bet_min"/> mas o jogador possuir esta quantia, este valor sera setado automaticamente
        /// para a aposta. Caso contrario ALLIN sera setado para esta ação e o valor apostado permanecerá.
        /// Se a aposta for superior ao <see cref="bet_max"/> o valor máximo permitido sera setado para esta ação.
        /// Se em ambos os casos o jogador não possuir saldo suficiente em dinheiro, ALLIN sera setado a esta ação e valor total que
        /// o jogador possui sera setado à esta aposta!
        /// </param>
        /// <returns>
        /// Retorna a ação validade e adcionada à lista de ações do jogador
        /// </returns>
        public MatchAction Execute(double value)
        {
            verify();
            if (!_allow)
                throw new Exception("Action Denny, you no have permision to execute ActionRaise");


            MatchAction result = null;
            BetPlayer currentBetPlayer = matchGame.BetPlayer_Current;

            double total = value;
            //Valor que podera sobrar do desconto total pretendido e o total que o jogador possui
            double sub = 0;
            //Valor real descontado do jogador
            double realsub = 0;
            bool allin = false;
            bool voluntary = true;
            if (total > bet_max)
                total = bet_max;
            else if (total < bet_min)
                total = bet_min;

            //Sub sera maior q 0 caso exista sobras na subtração do total, indicando que o jogador possuia a quantidade
            //necessário para o desconto total.
            sub = currentBetPlayer.SubChip(total);

            if (currentBetPlayer.chipsAmount <= 0)
                allin = true;

            realsub = total - sub;

            //Verificando ação voluntaria
            if (matchGame.turn == BetTurn.preflop)
                if (matchGame.matchActions.Get_Count(matchGame.turn) == 0 || matchGame.matchActions.Get_Count(matchGame.turn) == 1)
                    voluntary = false;

            result = matchGame.matchActions.AddBetAction(currentBetPlayer, BetType.rise, realsub, currentBetPlayer.Get_BetsAmount(matchGame.turn) + realsub, allin, voluntary);
            return result;
        }
        private void verify()
        {
            /* COMANDO RAISE
            O Comando de Aumentar sera permitido ao jogador da vez, 
            quando ja houverem outras apostas no mesmo turno de outros jogadores.

            > O Jogador da vez devra ser diferente do ultimo jogador a ter apostado no turno.
            > O Minimo que jogador podera apostar é o call obrigatório somado ao mínimo para aumentar
            */

            _allow = false;

            if (matchGame.matchActions.Get_Count(matchGame.turn) <= 0)//Não existe apostas na mesa para serem aumentadas
                return;

            //Não existem mais jogadores ativos na partida
            //PS: Eu axo que o melhor é desativar esta acao quando so houver o jogador da vez ativo.
            //Isso significa que o jogador da vez nao podera aumentar uma aposta quando não houver mais jogadores ativos.
            //Por padrao estou permitindo mas seria legal desativar no futuro
            if (matchGame.pokertable.betplayers.Get_ActivePlayers().Count <= 1)
            {
                //Esta acao so sera ativa se eu ainda tiver que pagar algum bidder.
                //Em alguns casos o Bidder pode entrar em ALLIN deixando somente o jogador da vez ativo. 
                //Neste caso o jogador da vez ainda tera este comando valido para responder ao bidder
                BetPlayer bidder = matchGame.BetPlayer_BidderOn(matchGame.turn);
                if (bidder == null || bidder == matchGame.BetPlayer_Current)
                    return;

                if (bidder.Get_BetsAmount(matchGame.turn) <= matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn))
                    return;
            }

            //Ação duplicada. Jogador tentando executar a mesma acao duas vezes
            if (matchGame.turn.Get_LastAction(matchGame) != null)
                if (matchGame.turn.Get_LastAction(matchGame).match_player.betPlayer == (matchGame.BetPlayer_Current))
                    return;

            //Se ja houver uma aposta check no preflop. as acoes de turno finalizaram, pois o big ja deu sua ação voluntaria
            //de fechamento de turno.
            if (matchGame.turn == BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn, BetType.check) > 0)
                return;

            MatchAction lastAction = matchGame.matchActions.Get_Last(matchGame.turn);       //Ultima ação da mesa
            BetPlayer BidderPlayer = matchGame.BetPlayer_BidderOn(matchGame.turn);  //Maior apostador do turno

            int validActions = 0;
            validActions += matchGame.matchActions.Get_Count(matchGame.turn, BetType.bet);
            validActions += matchGame.matchActions.Get_Count(matchGame.turn, BetType.call);
            validActions += matchGame.matchActions.Get_Count(matchGame.turn, BetType.rise);

            if (matchGame.matchActions.Get_Count(matchGame.turn) <= 0 && matchGame.turn != BetTurn.preflop)//Não a apostas para aumetnar no turno
                return;

            if (validActions <= 0) //Nao existem apostas validas neste turno para permitir o raise
                return;

            if (lastAction.match_player.betPlayer == (matchGame.BetPlayer_Current)) //Jogador da vez é o ultimo com ação valida
                return;

            //O Maior apostador é o prorio jogador da vez .. Isto e permitido no PreFLOP, quando o BIDDER for o BIGBLIND dando sua primeira acao voluntaria. (SOMENTE)
            if (BidderPlayer == matchGame.BetPlayer_Current && matchGame.turn != BetTurn.preflop)
                return;
            if (BidderPlayer == matchGame.BetPlayer_Current && matchGame.turn == BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn) > 2)
                return;

            //Jogador ja cubriu a aposta do maior apostador neste turno
            if (BidderPlayer.Get_BetsAmount(matchGame.turn) <= matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn))
                return;

            double requiredCall = BidderPlayer.Get_BetsAmount(matchGame.turn) - matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn);
            //Valor obrigatório do call Somados ao small blind permitido da mesa
            double requiredMin = requiredCall + matchGame.config.smallBlind;

            if (matchGame.BetPlayer_Current.chipsAmount < requiredMin)
                return;
            bet_min = requiredMin;

            //Se esta ação nao for voluntaria, o raize maximo sera o bigblind da mesa, caso contrario
            //O Limite é o dinheiro que o jogador possui subtraido do mínimo, pois ao setar o maximo o minimo tambem sera discontado
            //bet_max = matchGame.BetPlayer_Current.chipsAmount - requiredCall;
            bet_max = matchGame.BetPlayer_Current.chipsAmount;
            if (matchGame.turn == BetTurn.preflop)
                if (matchGame.matchActions.Get_Count(matchGame.turn) == 1)
                    bet_max = matchGame.config.bigBlind;


            _allow = true;

        }
    }
    /// <summary>
    /// Evaluates the permission to use the CHECK command in a game managed by <see cref="MatchGame"/>.
    /// </summary>
    public class ActionCheck
    {
        private bool _allow;
        public bool allow
        {
            get
            {
                if (matchGame != null)
                {
                    verify();
                    return _allow;
                }
                else
                {
                    return false;

                }
            }
        }
        public double bet_min { get; private set; }
        public double bet_max { get; private set; }
        private MatchGame matchGame { get; set; }

        public ActionCheck(MatchGame matchGame)
        {
            bet_min = 0;
            bet_max = 0;
            _allow = false;
            this.matchGame = matchGame;

            verify();
        }
        public MatchAction Execute()
        {
            verify();

            if (!_allow)
                throw new InvalidOperationException("Action Denny, you no have permision to execute ActionCheck");

            MatchAction result = null;
            BetPlayer currentBetPlayer = matchGame.BetPlayer_Current;

            result = matchGame.matchActions.AddBetAction(currentBetPlayer, BetType.check, 0, currentBetPlayer.Get_BetsAmount(matchGame.turn), false, true);
            return result;
        }
        private void verify()
        {
            /*
          CHECK
          A Ação Check podera ser usada somente quando não houver nenhuma aposta no turno exeto no turno PREFLOP
          onde o check devera estar habilitado para o jogador BIG BLIND para a sua primeira ação Voluntaria não havendo nenhuma
          aposta maior do que a dele mesmo no turno e desabilitado para todas as demais ações e jogadores do mesmo turno
          */

            _allow = false;

            //Não existem mais jogadores ativos na partida
            if (matchGame.pokertable.betplayers.Get_ActivePlayers().Count <= 1)
                return;

            //Ação duplicada. Jogador tentando executar a mesma acao duas vezes
            if (matchGame.turn.Get_LastAction(matchGame) != null)
                if (matchGame.turn.Get_LastAction(matchGame).match_player.betPlayer == (matchGame.BetPlayer_Current))
                    return;

            BetPlayer CurrentPlayer = matchGame.BetPlayer_Current;
            BetPlayer BigPlayer = matchGame.BetPlayer_Big;
            //BetPlayer Bidder = matchGame.BetPlayer_BidderOn(matchGame.turn);

            //Se ja houver uma aposta check no preflop. as acoes de turno finalizaram, pois o big ja deu sua ação voluntaria
            //de fechamento de turno.
            if (matchGame.turn == BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn, BetType.check) > 0)
                return;

            //O Unico raise permitido no preflo é o do BIG Automatico.. Caso existe mais algum o check nao sera mais permitido
            if (matchGame.turn == BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn, BetType.rise) > 1)
                return;

            if (matchGame.turn == BetTurn.preflop && BigPlayer.Get_ActionsCount(matchGame.turn) != 1)
                return;

            if (matchGame.turn == BetTurn.preflop && BigPlayer != CurrentPlayer)
                return;

            if (matchGame.turn == BetTurn.preflop && BigPlayer != CurrentPlayer)
                return;

            if (matchGame.turn != BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn) > 0)
            {
                MatchAction action = matchGame.matchActions.Get_Last(matchGame.turn);

                if (action.bet_action != BetType.check)
                    return;
                else if (action.bet_action == BetType.check && matchGame.matchActions.Get_Count(matchGame.turn, BetType.check) == matchGame.pokertable.betplayers.Get_ActivePlayers().Count)
                    return;
            }



            bet_min = 0;
            bet_max = 0;
            _allow = true;
        }

    }
    /// <summary>
    /// Evaluates the permission to use the FOLD command in a game managed by <see cref="MatchGame"/>.
    /// </summary>
    public class ActionFold
    {
        private bool _allow;
        public bool allow
        {
            get
            {
                if (matchGame != null)
                {
                    verify();
                    return _allow;
                }
                else
                {
                    return false;

                }
            }
        }
        public double bet_min { get; private set; }
        public double bet_max { get; private set; }
        private MatchGame matchGame { get; set; }

        public ActionFold(MatchGame matchGame)
        {
            bet_min = 0;
            bet_max = 0;
            _allow = false;
            this.matchGame = matchGame;
            verify();
        }
        public MatchAction Execute()
        {
            verify();

            if (!_allow)
                throw new InvalidOperationException("Action Denny, you no have permision to execute ActionCheck");

            MatchAction result = null;
            BetPlayer currentBetPlayer = matchGame.BetPlayer_Current;

            result = matchGame.matchActions.AddBetAction(currentBetPlayer, BetType.fold, 0, currentBetPlayer.Get_BetsAmount(matchGame.turn), false, true);
            return result;
        }
        private void verify()
        {
            /* FOLD
            Sera permitido sempre, exeto para as 2 acoes obrigatorios do PREFLOP Small e Big blinds.
            */

            _allow = false;

            //Não existem mais jogadores ativos na partida
            //Não existem mais jogadores ativos na partida
            if (matchGame.pokertable.betplayers.Get_ActivePlayers().Count <= 1)
            {
                //Esta acao so sera ativa se eu ainda tiver que pagar algum bidder.
                //Em alguns casos o Bidder pode entrar em ALLIN deixando somente o jogador da vez ativo. 
                //Neste caso o jogador da vez ainda tera este comando valido para responder ao bidder
                BetPlayer bidder = matchGame.BetPlayer_BidderOn(matchGame.turn);
                if (bidder == null || bidder == matchGame.BetPlayer_Current)
                    return;

                if (bidder.Get_BetsAmount(matchGame.turn) <= matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn))
                    return;
            }

            //Ação duplicada. Jogador tentando executar a mesma acao duas vezes
            if (matchGame.turn.Get_LastAction(matchGame) != null)
                if (matchGame.turn.Get_LastAction(matchGame).match_player.betPlayer == (matchGame.BetPlayer_Current))
                    return;

            //Se ja houver uma aposta check no preflop. as acoes de turno finalizaram, pois o big ja deu sua ação voluntaria
            //de fechamento de turno.
            if (matchGame.turn == BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn, BetType.check) > 0)
                return;

            //Se o turno for preflop o fold so sera liberado apos a acao automatica do big, para ser usado pelo
            //proximo jogador da vez como acao voluntaria
            if (matchGame.turn == BetTurn.preflop && matchGame.matchActions.Get_Count(matchGame.turn) < 2)
                return;

            if (matchGame.BetPlayer_BidderOn(matchGame.turn) != null)
                if (matchGame.BetPlayer_BidderOn(matchGame.turn) == matchGame.BetPlayer_Current)//O Maior apostador é o prorio jogador da vez
                    return;

            //Jogador ja cubriu a aposta do maior apostador neste turno
            if (matchGame.BetPlayer_BidderOn(matchGame.turn) != null)
                if (matchGame.BetPlayer_BidderOn(matchGame.turn).Get_BetsAmount(matchGame.turn) <= matchGame.BetPlayer_Current.Get_BetsAmount(matchGame.turn))
                    return;

            bet_min = 0;
            bet_max = 0;
            _allow = true;
        }

    }
    /// <summary>
    /// Use to activate a component inspector way to set custom values for your game
    /// If you use the games manager <see cref="MatchGame"/>, this component will be required, since it will use the existing settings here to manage the game.
    /// </summary>
    [System.Serializable]
    public class MatchConfiguration
    {
        /// <summary>
        /// Pot division type
        /// </summary>
        public MatchPoteType poteType;
        /// <summary>
        /// minimum bet to automatic bet on preflop and manual bet on all turn
        /// </summary>
        public double smallBlind;
        /// <summary>
        /// Maximum bet to LimitedPot and automatic bet on preFlop
        /// </summary>
        public double bigBlind;
        /// <summary>
        /// Rake Amount: Minimum of money to sit at the table of bets
        /// </summary>
        public double smallStake;
        /// <summary>
        /// Rake Amount: Maximum money to sit at the table of bets
        /// </summary>
        public double bigStake;
    }
    /// <summary>
    /// 
    /// </summary>
    public class MatchPlayer
    {
        /// <summary>
        /// Betplayer PAI 
        /// </summary>
        public BetPlayer betPlayer { get; private set; }
        /// <summary>
        /// Partida Pai
        /// </summary>
        public MatchGame match
        {
            get
            {
                MatchGame game = null;
                if (betPlayer != null)
                {
                    if (betPlayer.pokerTable != null)
                    {
                        game = betPlayer.pokerTable.Get_MatchGame();
                    }
                }

                return game;
            }
        }
        /// <summary>
        /// Valor de dinheiro que o jogador possuia no momento desta ação.
        /// <note type="note">Este valor não é referente ao total que o jogador possui n apartida, mas sim o total
        /// que ojogador possuia no momento desta ação.
        /// Para saber o real valor que jogador possui no momento atual da partida, utilize <see cref="BetPlayer().chipAmount"/> sobre esta classe 
        /// </note>
        /// </summary>
        public double currentChips { get; set; }
        /// <summary>
        /// Cartas que o jogador possuia na mao nesta ação
        /// </summary>
        public CardsList<Card> handCards { get; set; }

        public MatchPlayer(BetPlayer _betPlayer)
        {
            betPlayer = _betPlayer;
            currentChips = _betPlayer.chipsAmount;
            handCards = _betPlayer.holeCards.Clone().ToCardsList();
        }

    }
    /// <summary>
    /// 
    /// </summary>
    public class MatchTable
    {
        public CardsList<Card> boardCards { get; private set; }

        public MatchTable(PokerTable pokerTable)
        {
            boardCards = pokerTable.boardCards.Clone().ToCardsList();
        }

    }
    /// <summary>
    /// 
    /// </summary>
    public class MatchAction : IPokerMatchAction, ICloneable
    {
        public MatchActionEntity action_type { get; set; }
        //Identificador único da acao
        public int id { get; private set; }
        //Inicio da acao
        public DateTime datetime { get; private set; }
        //Turno da acao
        public BetTurn bet_turn { get; private set; }
        //Tipo de acao
        public BetType bet_action { get; private set; }
        //Valor da aposta
        public double bet_chips { get; private set; }
        //Valor real descontado do jogador
        //public double bet_taked { get; private set; }
        //Valor total apostado até esta ação
        public double bet_total { get; private set; }
        //Jogador da acao
        public MatchPlayer match_player { get; private set; }
        //Mesa da acao
        public MatchTable bet_table { get; private set; }
        //False quando for aposta obrigatória da besa, true quando for voluntaria
        public bool isVoluntary { get; private set; }
        //Retorna true quando jogador tiver apostado tudo nesta acao
        public bool isAllin { get; private set; }

        public int overhide_id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public MatchAction(int _id, BetPlayer betPlayer, MatchGame match, BetType betAction, double takeChips, double totalChips, bool allIn, bool voluntaryBet)
        {
            this.id = _id;
            datetime = DateTime.Now;
            bet_turn = match.turn;
            bet_action = betAction;
            bet_chips = takeChips;
            bet_total = totalChips;
            isAllin = allIn;
            isVoluntary = voluntaryBet;
            match_player = new MatchPlayer(betPlayer);
            bet_table = new MatchTable(match.pokertable);
        }
        public MatchAction(int _id, BetPlayer betPlayer, PokerTable table, BetTurn turn, BetType betAction, double takeChips, double totalChips, bool allIn, bool voluntaryBet)
        {
            this.id = _id;
            datetime = DateTime.Now;
            bet_turn = turn;
            bet_action = betAction;
            bet_chips = takeChips;
            bet_total = totalChips;
            isAllin = allIn;
            isVoluntary = voluntaryBet;
            match_player = new MatchPlayer(betPlayer);
            bet_table = new MatchTable(table);
        }
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MatchActions
    {
        protected virtual void Event_OnAdd(MatchActionEventArgs e)
        {
            EventHandler_Matches<MatchActionEventArgs> handler = OnAdd;
            if (handler != null)
            {
                handler(e);
            }
        }
        protected virtual void Event_OnRemove(MatchActionEventArgs e)
        {
            EventHandler_Matches<MatchActionEventArgs> handler = OnRemove;
            if (handler != null)
            {
                handler(e);
            }
        }

        public event EventHandler_Matches<MatchActionEventArgs> OnAdd;
        public event EventHandler_Matches<MatchActionEventArgs> OnRemove;

        public int AID { get; private set; }

        /// <summary>
        /// Lista de ações da partida, executadas até o momento
        /// </summary>
        private List<MatchAction> actionsList;


        /// <summary>
        /// Partida dona das ações
        /// </summary>
        public MatchGame matchGame { get; private set; }
        public DateTime time_start { get; private set; }
        public DateTime time_finish { get; private set; }
        //Inicializando
        public MatchActions(MatchGame _matchGame)
        {
            matchGame = _matchGame;
            time_start = DateTime.Now;
            actionsList = new List<MatchAction>();

        }

        /// <summary>
        /// Adicionando uma nova acao à lista, sem verificação.
        /// <note type="note">
        /// Caso haja necessidade de validar as ações antes de adicionar à lista, utilize MatchAllowedAction.
        /// </note>
        /// </summary>
        /// <param name="player">Jogador origem</param>
        /// <param name="betType">Tipo da ação</param>
        /// <param name="betChips">Valor de aposta desta ação</param>
        /// <param name="takeChips">Valor real descontado do jogador nesta ação</param>
        /// <param name="totalChips">Valor total apostado pelo jogador até o momento desta ação</param>
        /// <param name="isAllin">Verdadeiro se o total descontado do jogador nesta ação, zerou suas fichas</param>
        /// <param name="isVoluntaryBet">Verdadeiro se esta ação não for a small e big orbrigatórios do preflop</param>
        /// <returns></returns>
        public MatchAction AddBetAction(BetPlayer player, BetType betType, double takeChips, double totalChips, bool isAllin, bool isVoluntaryBet)
        {
            MatchAction action = new MatchAction(++AID, player, matchGame, betType, takeChips, totalChips, isAllin, isVoluntaryBet);
            actionsList.Add(action);

            MatchActionEventArgs args = new MatchActionEventArgs(matchGame, action);
            Event_OnAdd(args);

            return action;
        }
        /// <summary>
        /// Adicionando uma nova acao à lista, sem verificação.
        /// <note type="note">
        /// Caso haja necessidade de validar as ações antes de adicionar à lista, utilize MatchAllowedAction.
        /// > Toda a acao utilizando este metodo sera considerada VOLUNTARIA pelo sistema
        /// > Esta ação ira descontar automaticamente o valor espesificado do saldo total do jogador, e o evento
        /// de desconto sera acionado normalmente. Em caso de falto de saldo pelo jogador, allIn sera setado para esta ação!
        /// </note>
        /// </summary>
        /// <param name="player">Jogador origem</param>
        /// <param name="betType">Tipo da ação</param>
        /// <param name="betChips">Valor de aposta desta ação</param>        
        /// <returns></returns>
        public MatchAction AddBetAction(BetPlayer player, BetType betType, double betChips, BetTurn turn)
        {
            double takeChips, totalChips;
            bool isAllin = false, isVoluntaryBet = true;

            double miss = player.SubChip(betChips);

            takeChips = betChips - miss;
            totalChips = player.Get_BetsAmount();

            if (player.chipsAmount == 0)
                isAllin = true;

            MatchAction action = new MatchAction(++AID, player, matchGame.pokertable, turn, betType, takeChips, totalChips, isAllin, isVoluntaryBet);
            actionsList.Add(action);

            MatchActionEventArgs args = new MatchActionEventArgs(matchGame, action);
            Event_OnAdd(args);

            return action;
        }
        /// <summary>
        /// Copia da lista de ações existente na partida até o momento
        /// </summary>
        /// <returns>List<MatchAction> actions</returns>
        public List<MatchAction> Get_Actions()
        {
            List<MatchAction> actions = new List<MatchAction>(actionsList);
            actions = actions.OrderBy(r => r.id).ToList();
            return actions;
        }
        /// <summary>
        ///  Copia da lista de ações existente na partida para um dado jogador até o momento
        /// </summary>
        /// <param name="betplayer">Jogador origem das ações</param>
        /// <returns>List<MatchAction> actions, do jogador</returns>
        public List<MatchAction> Get_Actions(BetPlayer betplayer)
        {
            List<MatchAction> results = actionsList.FindAll(r => r.match_player.betPlayer == betplayer);


            return results;
        }
        public List<MatchAction> Get_Actions(BetPlayer betplayer, BetType betType)
        {
            List<MatchAction> results = actionsList.FindAll(r => r.match_player.betPlayer == betplayer);
            results.RemoveAll(r => r.bet_action != betType);

            return results;
        }
        public List<MatchAction> Get_Actions(BetPlayer betplayer, BetTurn turn)
        {
            List<MatchAction> results;

            IEnumerable<MatchAction> query =
                from action in actionsList
                where action.match_player.betPlayer == betplayer && action.bet_turn == turn
                select action;

            results = query.ToList();


            return results;
        }
        public List<MatchAction> Get_Actions(BetTurn turn)
        {
            List<MatchAction> results;

            IEnumerable<MatchAction> query =
                from action in actionsList
                where action.bet_turn == turn
                select action;

            results = query.ToList();


            return results;
        }
        public MatchAction Get_Last()
        {
            MatchAction last = actionsList.Last();
            return last;
        }
        public MatchAction Get_Last(BetTurn turn)
        {
            MatchAction last = actionsList.LastOrDefault(r => r.bet_turn == turn);
            if (last != null)
                if (last.bet_turn != turn)
                    last = null;
            return last;
        }

        public int Get_Count()
        {
            int result = actionsList.Count;
            return result;
        }
        public int Get_Count(BetTurn turn)
        {
            int result = actionsList.Count(r => r.bet_turn == turn);
            return result;
        }

        public int Get_Count(BetType type)
        {

            IEnumerable<MatchAction> results =
                from action in actionsList
                where action.bet_action == type
                select action;

            return results.ToList().Count();

        }
        public int Get_Count(BetTurn turn, BetType type)
        {
            IEnumerable<MatchAction> results =
                from action in actionsList
                where action.bet_action.Equals(type) == true && action.bet_turn == turn
                select action;

            int result = results.ToList().Count();
            return result;
        }

        public double Get_ChipsAmount(BetTurn turn)
        {
            double total = 0d;
            foreach (BetPlayer p in matchGame.pokertable.betplayers)
            {
                total += p.Get_BetsAmount(turn);
            }

            return total;
        }

        /// <summary>
        /// Total de apostas efetuadas na partida. Este valor poderá ser utilizado como Valor de POTE Comunitário.
        /// Valores de Extorno não serão contabilizados aqui.
        /// Veja <see cref="Get_BidderChipsReturn"/> para saber se o BidderPlayer possui valores de extorno
        /// </summary>
        /// <returns>Valor do Pote comunitário da partida, subtraido de valroes de extorno</returns>
        public double Get_ChipsAmount()
        {
            double total = 0d;

            foreach (BetPlayer p in matchGame.pokertable.betplayers)
                total += p.Get_BetsAmount();

            return total - Get_BidderChipsReturn();
        }

        /// <summary>
        ///  Valor de extorno ao BidderPlayer, quando este não tiver sua aposta paga pelos demais jogadores da partida.
        /// </summary>
        /// <returns>Retorna valor total que devera ser retornado ao BidderPlayer</returns>
        public double Get_BidderChipsReturn()
        {
            double returnPay = 0d;
            try
            {
                //Procurando valores a serem extornados a jogadores cuja apostas nao foram pagas pelos demais jogadores
                BetPlayer bidder = matchGame.BetPlayer_Bidder;
                if (bidder != null)
                {
                    BetPlayer bidder2 = bidder.NextBidder();
                    if (bidder2 != null)
                        returnPay = bidder.Get_BetsAmount() - bidder2.Get_BetsAmount();
                }
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            return returnPay;
        }
    }
    /// <summary>
    /// It enables optimized management of all stages of a game of poker, assessing all the poker rules and standards applying them to every action taken by the players.
    /// It also allows a simple and effective way of the assessment and classification of final hands of the players as well as evaluation and award classification in cash based on the classification of final hands.
    /// </summary>
    /// <example>
    /// This will show in detail example of the hands of validation processes using the games manager
    /// <see cref="PokerCollections.MatchGame.MatchGame"/>
    /// <code>
    /// using UnityEngine;
    /// using System.Collections;
    /// using System.Collections.Generic;
    /// using PokerCollections;
    /// using PokerCollections.MatchGame;
    /// /*
    /// ====READ-ME=================================================
    /// Here we will use a simple but very functional library to work with the validation engine uPoker hands.
    /// This will allow you to manage the starting information, facilitating the creation and management of all
    /// components necessary for the development of a poker game.
    /// Note that the PokerCollections library is open and has the intention to be an initial step you implement
    /// new methods that meet all your needs in creating your game.
    /// > Feel free to modify and implement your needs.
    ///
    /// Using the Poker Collections library is very easy to create and validate poker texas hold hand. 
    /// - Support for multiple matches games simultaneous.
    /// - Easy Player create and mananger
    /// - Easy PokerTable create and Mananger
    /// - Easy Deck Card mananger
    /// - Fast and Very Very Very easy evalue hands
    /// ============================================================
    /// */
    /// public class EasyEvalue : MonoBehaviour
    /// {
    ///     void Start()
    ///     {
    ///         //We create a PokerTable for the game 
    ///         //and choose to automatically create a deck and shuffle to.  
    ///         // MatchListner.Initialize(this);
    ///         PokerTable table = new PokerTable(true);
    ///         MatchGame match = new MatchGame(this, table);
    /// 
    ///         //Creating Player for gamming
    ///         table.CreatBetPlayer("player1", 1);
    ///         table.CreatBetPlayer("player2", 2);
    /// 
    ///         //Iniciando uma nova partida
    ///         match.StartNewMatch();
    /// 
    ///         /*============================================================
    ///          -With my already shuffled deck, you can simply receive letters directly from the order, 
    ///          because the cards are already in random order.
    ///          ===============================================================*/
    ///
    ///         //TAKE PLAYER HAND CARDS FROM DECK
    ///         //Giving the two player cards automatically and at cyclo
    ///         match.GetCardsToBetPlayers();
    ///         //TAKE BOARD CARDS FROM DECK
    ///         //Giving the tree Flop cards board automatically.
    ///         match.turn = BetTurn.flop;
    ///         //Giving the one Turn card board automatically.
    ///         match.turn = BetTurn.turn;
    ///         //Giving the one River card board automatically.
    ///         match.turn = BetTurn.river;
    /// 
    ///         Debug.Log(table.PrintBoardCards());
    ///         //Print all players hand description
    ///         Debug.Log(table.PrintPlayersHandCards());
    /// 
    ///         //Validate and get list with winners, ties and lossers players.
    ///         EvaluatedHoldemHand handOdds = match.GetEvaluatedHandOdds();
    /// 
    ///         if (handOdds != null)
    ///         {
    ///             //Print odds result
    ///             Debug.Log(handOdds.ToString());
    /// 
    ///             //Print best five 
    ///             foreach (BetPlayer player in handOdds.winners)
    ///             {
    ///                 //Print from eval cards
    ///                 Debug.Log(player.holdemhand.PocketCards + " " + player.holdemhand.Board);
    ///                 Debug.Log("Best five cards of the hand: (" + player.holdemhand.BestFiveCards() + ")");
    /// 
    ///                 //List of the best five from evaluated cards and if this on hand or on board table
    ///                 List<![CDATA[<Card>]]> cards = player.GetBestFiveCards();
    ///                 foreach (Card c in cards)
    ///                 {
    ///                     if (player.holeCards.Exists(r => r.id == c.id))
    ///                         Debug.Log("At Hand > " + c.ToString());
    ///                     else
    ///                         Debug.Log("At Board > " + c.ToString());
    ///                 }
    ///             }
    ///         }
    ///
    ///         //Clear player cards and table cards
    ///         //set deckcards to default and shuffled
    ///         match.ResetTable();
    ///     }
    /// }
    /// </code>
    /// </example>
    public class MatchGame : IPokerMatch
    {
        public class IsDone
        {
            public bool done;
        }

        /// <summary>
        /// Unico identificado da partida. Este id e adicionado automaticamente quando sua matchGame possui um componente proprio de inicializacao
        /// Veja: <see cref="MatchGameListner.Matches"/> Para listar todas as partidas em existentes
        /// </summary>
        public int id { get; private set; }
        /// <summary>
        /// Permite modificar o Identificador único da partida
        /// </summary>
        public int overhide_id { get { return id; } set { id = value; } }
        /// <summary>
        /// progress status of the current match. See also: <see cref="SetIsActive(bool)"/>
        /// </summary>
        public bool isActive { get; private set; }
        /// <summary>
        /// Number of players of this match. This value is set when the match is started and counts all the players who will play this game
        /// </summary>
        public int playersCount { get; private set; }
        /// <summary>
        /// Number of viewers that game that are guarding place for the next match.This value is set when the game starts
        /// </summary>
        public int hoplayersCount { get; private set; }

        //Variaveis de acesso privado
        private Extend IExtend;
        //Componente monoBehavior
        public MonoBehaviour component { get; private set; }
        //Lugar referente ao dealler atual
        private int currentDeallerPlace;

        public MatchConfiguration config { get; private set; }
        /// <summary>
        /// PokerTable gerenciada pela MathGame
        /// </summary>
        public PokerTable pokertable { get; private set; }
        /// <summary>
        /// Histórico de todas as acoes da partida
        /// </summary>
        public MatchActions matchActions { get; private set; }
        /// <summary>
        /// Atualiza turno da partida
        /// </summary>
        public BetTurn turn
        {
            get
            {
                return IExtend.GetTurn();
            }
            set
            {
                if (IExtend.GetTurn() == value)
                    return;

                //Anulamos a referencia do jogador da vez, sempre que um turno for modificado
                //para que ele seja atualizado automaticamente pelo metodo de retorno BetPlayer_Current
                BetPlayer_Current = null;
                IExtend.SetTurn(value);



                //Evento
                // MatchListner.Execute_OnChangeTurn(this, value);
            }
        }
        /// <summary>
        /// Jogador da vez. Nullo quando a partida não estiver em andamento ou se não houver jogadores ativos na partida.
        /// Jogadores em FOLD não ~são considerados ativos na partida e nao serão retornados.
        /// </summary>
        /// <remarks> 
        /// <see cref="BetPlayer_Dealer"/>
        /// <see cref="BetPlayer_Small"/>
        /// <see cref="BetPlayer_Big"/>
        /// </remarks>
        public BetPlayer BetPlayer_Current
        {
            get
            {

                if (isStarted == false)
                {
                    IExtend.SetCurrentBetPlayer(null);
                    return null;
                }

                if (IExtend.GetCurrentBetPlayer() == null)
                {
                    if (turn == BetTurn.preflop)
                    {
                        if (matchActions.Get_Count() == 0)
                        {
                            IExtend.SetCurrentBetPlayer(BetPlayer_Small);
                        }
                    }
                    else
                    {
                        BetPlayer cur = pokertable.betplayers.NextRel(BetPlayer_Dealer);
                        IExtend.SetCurrentBetPlayer(cur);
                    }
                }

                /*
                //se o jogador escolhido estiver em fold vamos para o proximo
                for (int a = 0; a < pokerTable.betplayers.Count; a++)
                {
                    if (IExtend.GetCurrentBetPlayer().Get_ActionsCount(BetType.fold) > 0)
                    {
                        BetPlayer cur = pokerTable.betplayers.NextRel(BetPlayer_Dealer);
                        IExtend.SetCurrentBetPlayer(cur);
                    }
                    else
                    {
                        break;
                    }
                }

                if (IExtend.GetCurrentBetPlayer().Get_ActionsCount(BetType.fold) > 0)
                    IExtend.SetCurrentBetPlayer(null);
                    */
                return IExtend.GetCurrentBetPlayer();
            }
            private set
            {
                if (IExtend.GetCurrentBetPlayer() == value)
                    return;

                IExtend.SetCurrentBetPlayer(value);

            }
        }
        public BetPlayer Overhide_CurrentBetPlayer
        {
            set { BetPlayer_Current = value; }
        }
        /// <summary>
        /// Retorna o jogador referente ao Dealler
        /// Retorna Nullo se não houver partida iniciada
        /// </summary>
        /// <remarks> 
        /// <see cref="BetPlayer_Current"/>
        /// <see cref="BetPlayer_Small"/>
        /// <see cref="BetPlayer_Big"/>
        /// </remarks>
        public BetPlayer BetPlayer_Dealer
        {
            get
            {
                if (isStarted == false)
                    return null;

                BetPlayer result;
                result = pokertable.betplayers.NextRel(currentDeallerPlace);
                currentDeallerPlace = result.placeId > 1 ? result.placeId - 1 : 9;
                return result;
            }
        }
        /// <summary>
        /// Retorna o jogador referente ao "Pequeno Apostador", normalmente o jogador após o Dealler.
        /// Retorna o proprio Dealler em PreFlop HeadsUP, pois neste caso o Small é justamente o Dealler.
        /// Retorna Nullo se não houver partida iniciada
        /// </summary>
        /// <remarks> 
        /// <see cref="BetPlayer_Dealer"/>
        /// <see cref="BetPlayer_Current"/>
        /// <see cref="BetPlayer_Big"/>
        /// </remarks>
        public BetPlayer BetPlayer_Small
        {
            get
            {
                if (isStarted == false)
                    return null;

                BetPlayer result;

                if (pokertable.betplayers.Count == 2 && turn == BetTurn.preflop)
                    result = BetPlayer_Dealer;
                else
                    result = pokertable.betplayers.NextRel(BetPlayer_Dealer);

                return result;
            }
        }
        /// <summary>
        /// Retorna o jogador referente ao "Grande Apostador", sendo o jogador após o Small
        /// Retorna Nullo se não houver partida iniciada
        /// </summary>
        /// <remarks> 
        /// <see cref="BetPlayer_Dealer"/>
        /// <see cref="BetPlayer_Small"/>
        /// <see cref="BetPlayer_Current"/>
        /// </remarks>
        public BetPlayer BetPlayer_Big
        {
            get
            {
                if (isStarted == false)
                    return null;

                BetPlayer result;
                result = pokertable.betplayers.NextRel(BetPlayer_Small);
                return result;
            }
        }
        /// <summary>
        /// Maior apostador
        /// </summary>
        /// <returns> 
        /// Retorna o jogador cuja as ações fazem dele o maior apostador ate o momento
        /// <note type="note">
        /// Se houver mais de um jogador com o mesmo valor de apostas o sistema vai retornar o jogador 
        /// com a açao mais rescente
        /// </note>
        /// </returns>
        public BetPlayer BetPlayer_Bidder
        {
            get
            {
                BetPlayer result = null;
                try
                {
                    List<BetPlayer> actions = pokertable.betplayers;

                    double maxValue = actions.Max(r => r.Get_BetsAmount());

                    List<BetPlayer> results = (from action in actions
                                               where action.Get_BetsAmount() >= maxValue
                                               select action).OrderBy(r => r.Get_BetActions().LastOrDefault().id).ToList();
                    result = results.LastOrDefault();
                }
                catch
                {
                    result = null;
                }
                return result;
            }
        }
        /// <summary>
        /// Maior apostador do turno
        /// </summary>
        /// <param name="turn">Turno pesquisado</param>
        /// <returns> 
        /// Retorna o jogador cuja as ações fazem dele o maior apostador deste turno
        /// <note type="note">Se houver mais de um jogador com o mesmo valor de apostas o sistema vai retornar o jogador 
        /// com PlaceID Mais alto
        /// </note>
        /// </returns>
        public BetPlayer BetPlayer_BidderOn(BetTurn turn)
        {

            List<BetPlayer> players = pokertable.betplayers;

            double maxValue = players.Max(r => r.Get_BetsAmount());

            List<BetPlayer> results = (from action in players
                                       where action.Get_BetsAmount() >= maxValue && action.Get_BetActions(turn).Count > 0
                                       select action).OrderBy(r => r.Get_BetActions(turn).LastOrDefault().id).ToList();


            return results.FirstOrDefault();
        }
        /// <summary>
        /// Valor em grecesso equitavlente ao tempo de espera antes de iniciar efetivamente uma nova partida
        /// </summary>
        public float regreciveCount { get; private set; }
        /// <summary>
        /// Tempo em segundos equivalente ao total de tempo que uma partida aguarda antes d iniciar efetivamente
        /// </summary>
        public float regreciveTime { get; private set; }
        /// <summary>
        /// Valor em regcesso em porcentagem 1/100 para ser usado em componentes fillAmount ou loading bars
        /// </summary>
        public float regreciveAmount { get; private set; }
        /// <summary>
        /// True quando a partida estiver efetivamente iniciada. 
        /// False quando estiver finalizada ou em preparo
        /// </summary>
        public bool isStarted { get; private set; }
        /// <summary>
        /// Inicializa um novo gerenciador de partidas para uma pokertable.
        /// Ao usar este metodo de inicialização, sua MatchGame sera anexada ao GameObject origem e adicionado
        /// automáticamente à lista de MatchGames ativas no momento.
        /// <br>
        /// Veja: <see cref="MatchGameListner.Matches"/>!
        /// </br>
        /// <note type="important">
        /// Utilize <see cref="StartNewMatch(int)"/>, para iniciar uma nova partida apartir deste gerenciador.
        /// </note>
        /// </summary>
        /// <param name="_component">Componente Pai de origem</param>
        /// <param name="_pokertable">Pokertable Pai de origem</param>
        public MatchGame(MonoBehaviour _component, PokerTable _pokertable)
        {
            IExtend = new Extend(this);
            component = _component;
            pokertable = _pokertable;
            isStarted = false;
            regreciveAmount = 1.0f;
            // turn = BetTurn.preflop;
            config = new MatchConfiguration();
            turn = BetTurn.preflop;
            playersCount = _pokertable.betplayers.Count;
            hoplayersCount = _pokertable.hoplayers.Count;
            //Iniciando um novo gerenciador de acoes. 
            matchActions = new MatchActions(this);

            MatchGameListner.AddNewMatch(this);
        }
        /// <summary>
        /// Inicializa um novo gerenciador de partidas para uma pokertable
        /// Ao usar este metodo de inicialização, sua MatchGame sera anexada ao GameObject origem e adicionado
        /// automáticamente à lista de MatchGames ativas no momento.
        /// <br>
        /// Veja: <see cref="MatchGameListner.Matches"/>!
        /// </br>
        /// <note type="important">
        /// Utilize <see cref="StartNewMatch(int)"/>, para iniciar uma nova partida apartir deste gerenciador.
        /// </note>
        /// </summary>
        /// <param name="_component">Componente Pai de origem</param>
        /// <param name="_pokertable">Pokertable Pai de origem</param>
        /// <param name="_config">Classe de configuração para a partida</param>
        /// <param name="margeHoplayes">Se for true, ira fazer marge dos jogadores expectadores na lista de jogadores ativos</param>
        public MatchGame(MonoBehaviour _component, PokerTable _pokertable, MatchConfiguration _config, bool margeHoplayes)
        {
            IExtend = new Extend(this);
            component = _component;
            pokertable = _pokertable;
            isStarted = false;
            regreciveAmount = 1.0f;
            config = _config;

            if (margeHoplayes)
                _pokertable.MergeHoplayer();

            playersCount = _pokertable.betplayers.Count;
            hoplayersCount = _pokertable.hoplayers.Count;

            //Iniciando um novo gerenciador de acoes. 
            matchActions = new MatchActions(this);
            MatchGameListner.AddNewMatch(this);

            ResetTable();
        }
        /// <summary>
        /// Inicializa um novo gerenciador de partidas para uma pokertable
        /// Ao usar este metodo de inicialização, sua MatchGame sera anexada ao GameObject origem e adicionado
        /// automáticamente à lista de MatchGames ativas no momento.
        /// <br>
        /// Veja: <see cref="MatchGameListner.Matches"/>!
        /// </br>
        /// <note type="important">
        /// Utilize <see cref="StartNewMatch(int)"/>, para iniciar uma nova partida apartir deste gerenciador.
        /// </note>
        /// </summary>
        /// <param name="_component">Componente Pai de origem</param>
        /// <param name="_pokertable">Pokertable Pai de origem</param>
        /// <param name="_config">Classe de configuração para a partida</param>
        public MatchGame(MonoBehaviour _component, PokerTable _pokertable, MatchConfiguration _config)
        {
            IExtend = new Extend(this);
            component = _component;
            pokertable = _pokertable;
            isStarted = false;
            regreciveAmount = 1.0f;
            config = _config;
            playersCount = _pokertable.betplayers.Count;
            hoplayersCount = _pokertable.hoplayers.Count;
            //Iniciando um novo gerenciador de acoes. 
            matchActions = new MatchActions(this);
            MatchGameListner.AddNewMatch(this);
        }
        /// <summary>
        /// Inicializa uma nova partida imediatamente, não havendo necessidade de aguardar o processamento!
        /// <note type="important">
        /// O turno da partida sera modificação automaticamente para <see cref="BetTurn.preflop"/>
        /// e seu deck, <see cref="PokerTable.deck"/> sera embaralhado automáticamente e um proximo dealler baseado no ultimo dealler, 
        /// <see cref="MatchGame.BetPlayer_Dealer"/> sera selecionado.
        /// </note>
        /// </summary>
        public void StartNewMatch()
        {
            //Escolhendo um dealler
            if (currentDeallerPlace == 0)
                currentDeallerPlace = 0;
            else
                currentDeallerPlace = currentDeallerPlace < 9 ? currentDeallerPlace + 1 : 1;

            isStarted = true;

            turn = BetTurn.preflop;

        }
        /// <summary>
        /// Inicializa uma nova partida automaticamente depois dos segundos informado.
        /// <note type="important">
        /// Apos os segundos informados o turno da partida sera modificação automaticamente para <see cref="BetTurn.preflop"/>
        /// e seu deck, <see cref="PokerTable.deck"/> sera embaralhado automáticamente e um proximo dealler baseado no ultimo dealler, 
        /// <see cref="MatchGame.BetPlayer_Dealer"/> sera selecionado.
        /// </note>
        /// <note type="caution">
        /// Este metodo necessita de tempo para a finalização de seu processamento.
        /// Para não correr riscos utilize <see cref="IsDone.done"/> para verificar sua finalização efetiva. 
        /// </note>
        /// </summary>
        /// <param name="waitSeconds">Segundos aguardados para inicio efetivo da partida</param>
        /// <returns>MatchIsDone</returns>
        public IsDone StartNewMatch(int waitSeconds)
        {
            if (pokertable == null)
                throw new ArgumentNullException("PokerTable null");

            if (pokertable.betplayers.Count < 2)
                throw new ArgumentOutOfRangeException("BetPlayers is empty");

            isStarted = false;
            regreciveCount = waitSeconds;
            regreciveTime = waitSeconds;

            IsDone isDone = new IsDone();
            component.StartCoroutine(_PrepareMatch(isDone));

            //MatchListner.Execute_OnPreMatch(this);

            return isDone;
        }
        private System.Collections.IEnumerator _PrepareMatch(IsDone _isDone)
        {
            while (regreciveAmount > 0)
            {
                regreciveAmount -= 1.0f / regreciveTime * Time.deltaTime;
                yield return null;
            }

            //Verifica se a partida realmente tem condições de ser iniciadas
            //nao? resetTable
            //Sim? continue

            //Embaralhamos o novo deck
            pokertable.deck.Shuffle();

            //Escolhendo um dealler
            if (currentDeallerPlace == 0)
                currentDeallerPlace = 0;
            else
                currentDeallerPlace = currentDeallerPlace < 9 ? currentDeallerPlace + 1 : 1;

            isStarted = true;

            turn = BetTurn.preflop;
            //Iniciamos nova partida

            //MatchListner.Execute_OnStartMatch(this);

            // yield return new WaitForSeconds(0.2f);

            //Confirmamos dealer
            //MatchListner.Execute_OnChangeDealerPlayer(this, BetPlayer_Dealer);

            // yield return new WaitForSeconds(0.3f);

            //Confirmamos small
            //MatchListner.Execute_OnChangeSmallPlayer(this, BetPlayer_Small);

            // yield return new WaitForSeconds(0.3f);

            //Confirmamos big
            //MatchListner.Execute_OnChangeBigPlayer(this, BetPlayer_Big);

            // yield return new WaitForSeconds(0.3f);

            //Confirmamos currentPlayer
            //MatchListner.Execute_OnChangeCurrentPlayer(this, BetPlayer_Current);

            _isDone.done = true;

        }
        /// <summary>
        /// Take two cards for each player in this pokerTable and remove automatically 
        /// this card from <see cref="PokerTable.deck"/>
        /// <note type="important">
        /// For this method work successful, it is necessary that the deck has a number of cards
        /// equivalent to the cards that will be given to all players and more 5 cards referring 
        /// to the board of the table shifts(flop, turn, river).
        /// </note>
        /// <note type="caution">
        /// Cada carta dada aos jogadores utiliza um tempo de espera e isto pode gerar diverços erros caso você efetue
        /// a execução de metodos no decorrer deste tempo.
        /// A Finalização do processamento deste metodo só sera efetivo após o envio de todas as cartas a todos os 
        /// jogadores ativos na mesa!
        /// Para evitar problemas utilize <see cref="IsDone.done"/> retornado por este metodo, para saber quando
        /// seu processamento foi finalizado completamente!
        /// </note>
        /// <note type="note">
        /// Esta metodo não ira modificar o turno automaticamente. Lembre-se de fazer manualmente se 
        /// necessário. Veja: <see cref="BetTurn"/>
        /// </note>
        /// </summary>
        public IsDone GetCardsToBetPlayersAsync()
        {
            IsDone isDone = new IsDone();
            component.StartCoroutine(_GetCardsToPlayres(isDone));
            return isDone;
        }
        private System.Collections.IEnumerator _GetCardsToPlayres(IsDone _isDone)
        {
            Deck deck = pokertable.deck;
            List<BetPlayer> betplayers = pokertable.betplayers.ToList();

            if (deck.Cards.Count < ((betplayers.Count * 2) + 5))
            {
                Debug.LogWarning("TakeCardsToBetPlayers: Quantidade de cartas em seu Deck é isuficiente para todos os jogadores desta mesa bem como para todas as cartas obrigatórias dos turnos da mesa");
                yield break;
            }

            //Cartas aos jogadores devem ser obrigatoriamente no turno PREFLOP.
            //turn = BetTurn.preflop;
            //yield return new WaitForSeconds(1.2f);

            //Dando cartas em sentido horario
            BetPlayer p = BetPlayer_Dealer;

            //Carta 1
            for (int a = 0; a < betplayers.Count; a++)
            {
                p.holeCards.Clear();
                Card card = deck.Cards.TakeItem();
                p.holeCards.Add(card);
                p = pokertable.betplayers.NextRel(p);

                //evento
                //MatchListner.Execute_OnCardToPlayer(this, p, card);

                yield return new WaitForSeconds(0.2f);
            }

            //Carta 2
            p = BetPlayer_Dealer;

            for (int a = 0; a < betplayers.Count; a++)
            {
                Card card = deck.Cards.TakeItem();
                p.holeCards.Add(card);
                p = pokertable.betplayers.NextRel(p);

                //evento
                //MatchListner.Execute_OnCardToPlayer(this, p, card);

                yield return new WaitForSeconds(0.2f);
            }

            //Update EVAL HAND
            _isDone.done = true;

            //MatchListner.Execute_OnFinishCardToPlayer(this);

        }
        /// <summary>
        /// Envia carta a todos os jogadores da mesa automaticamente removendo as cartas do <see cref="PokerTable.deck"/>;
        /// <note type="important">
        /// For this method work successful, it is necessary that the deck has a number of cards
        /// equivalent to the cards that will be given to all players and more 5 cards referring 
        /// to the board of the table shifts(flop, turn, river).
        /// </note>
        /// <note type="note">
        /// Esta metodo não ira modificar o turno automaticamente. Lembre-se de fazer manualmente se 
        /// necessário. Veja: <see cref="BetTurn"/>
        /// </note>
        /// </summary>
        public void GetCardsToBetPlayers()
        {

            Deck deck = pokertable.deck;
            List<BetPlayer> betplayers = pokertable.betplayers.ToList();
            if (deck.Cards.Count < ((betplayers.Count * 2) + 5))
            {
                Debug.LogWarning("TakeCardsToBetPlayers: Quantidade de cartas em seu Deck é isuficiente para todos os jogadores desta mesa bem como para todas as cartas obrigatórias dos turnos da mesa");

                return;
            }

            //Dando cartas em sentido horario
            BetPlayer p = BetPlayer_Dealer;

            //Carta 1
            for (int a = 0; a < betplayers.Count; a++)
            {
                p.holeCards.Clear();
                Card card = deck.Cards.TakeItem();
                p.holeCards.Add(card);
                p = pokertable.betplayers.NextRel(p);
            }

            //Carta 2
            p = BetPlayer_Dealer;
            for (int a = 0; a < betplayers.Count; a++)
            {
                Card card = deck.Cards.TakeItem();
                p.holeCards.Add(card);
                p = pokertable.betplayers.NextRel(p);
            }

        }
        /// <summary>
        /// Take automatically, tree cards for board in this pokerTable and remove automatically
        /// this card from <see cref="PokerTable.deck"/> and updates <see cref="MatchGame.turn"/> of for <see cref="BetTurn.flop"/>
        /// </summary>
        /// <note>
        /// Se houver cartas na lista de cartas da mesa da pokertable, ela sera limpa quando este metodo for chamado!
        /// For this method work successful, it is necessary that the deck has a number of cards
        /// equivalent to the 5 cards referring to the board of the table shifts(flop, turn, river).
        ///<para>It is also necessary that all the players at the table have two cards in your hands!</para>
        /// </note>
        /// <returns>Taked cards to board</returns>
        protected List<Card> GetFlopCards()
        {
            Deck deck = pokertable.deck;

            if (deck.Cards.Count < 5)
            {
                Debug.LogError("The number of  cards in your deck is not enough to get the necessary 5 cards from (flop, turn , river)! Reset your deck for default 52 cards value");
                return null;
            }

            pokertable.boardCards.Clear();
            List<Card> flopcards = new List<Card>();
            while (flopcards.Count < 3)
            {
                Card card = deck.Cards.TakeItem();
                flopcards.Add(card);
                pokertable.boardCards.Add(card);
            }

            return flopcards.ToList();
        }
        /// <summary>
        /// Take automatically one card for board in this pokerTable and remove automatically this card from <see cref="PokerTable.deck"/>
        /// and updates the <see cref="MatchGame.turn"/> for <see cref="BetTurn.turn"/>
        /// </summary>
        /// <note>
        /// For this method work successful, it is necessary that the deck has a number of cards
        /// equivalent to the 5 cards referring to the board of the table shifts(flop, turn, river).
        ///<para>It is also necessary that all the players at the table have two cards in your hands!</para>
        /// </note> 
        /// <returns>Taked cards to board</returns>
        protected Card GetTurnCard()
        {
            Deck deck = pokertable.deck;

            if (deck.Cards.Count < 5)
            {
                Debug.LogError("The number of  cards in your deck is not enough to get the necessary 5 cards from (Flop, Turn, River)! Reset your deck for default 52 cards value");
                return null;
            }
            if (pokertable.boardCards.Count != 3)
            {
                Debug.LogError("O Numero de cartas de na mesa não esta correto!. Chame o turno \"Flop\" Antes");
                return null;
            }

            Card turncard = deck.Cards.TakeItem();
            pokertable.boardCards.Add(turncard);


            return turncard;
        }
        /// <summary>
        /// Take automatically one card for board in this pokerTable and remove automatically this card from <see cref="PokerTable.deck"/>
        /// and updates the <see cref="MatchGame.turn"/> for <see cref="BetTurn.river"/>
        /// </summary>
        /// <note>
        /// For this method work successful, it is necessary that the deck has a number of cards
        /// equivalent to the 5 cards referring to the board of the table shifts(flop, turn, river).
        ///<para>It is also necessary that all the players at the table have two cards in your hands!</para>
        /// </note> 
        /// <returns>Taked cards to board</returns>
        protected Card GetRiverCard()
        {

            Deck deck = pokertable.deck;
            if (deck.Cards.Count < 5)
            {
                Debug.LogError("The number of  cards in your deck is not enough to get the necessary 5 cards from (Flop,Turn,River)! Reset your deck for default 52 cards value");
                return null;
            }

            if (pokertable.boardCards.Count != 4)
            {
                Debug.LogError("O Numero de cartas de na mesa não esta correto!. Chame o turno \"Flop\" ou Turn, Antes");
                return null;
            }


            Card rivercard = deck.Cards.TakeItem();
            pokertable.boardCards.Add(rivercard);


            return rivercard;
        }
        /// <summary>
        /// Clar and update deck cards more player cards and boardcards and to default all pokertable values
        /// <note type="important">
        /// O <see cref="MatchGame.turn"/> sera atualizado para <see cref="BetTurn.none"/>
        /// </note>
        /// </summary>
        /// <param name="shuffledeck">True for shuffle deck</param>
        public void ResetTable()
        {
            Deck deck = pokertable.deck;
            //Clear all cards from player Hand
            resetPlayersCards();
            //Clear all cards from board
            pokertable.boardCards.Clear();
            //reset deck to default
            deck.Reset();
            //reset table bet turn
            turn = BetTurn.none;
            //Partida finalizada
            isStarted = false;

        }
        /// <summary>
        ///Enables or disables the starting status in progress for this game. Keep in mind that this status changes automatically whenever a new game is started.        
        ///<para>Understand how:</para>
        ///<para>
        ///When a new game is started, it is automatically added to a list of matchGames, that belong to the same pokertable, and in the process it 
        ///automatically gain the true status for "isActive" variable, showing that this is starting currently underway for this pokertable.
        ///When a new game is started, all previous matches in the list will win the false status to "isActive". 
        ///In this way, only the last game created for this table will remain with the status "isActive" true.
        /// </para>
        /// <note type="note">You can set this status manually, for example, when a game reaches the end and no new game will be started. So it's a good conduct you establish the status of this end game to false.</note>
        ///<note type="caution">
        ///Keep in mind that methods that return the current departure of a pokertable will not return a match with false status.Unless you use a specific method for this case. Always know the legends of each method, not to be deceived!
        /// </note>
        /// </summary>
        /// <param name="stats">manually modify the starting status in progress</param>
        public void SetActive(bool stats)
        {
            isActive = stats;
        }
        /// <summary>
        /// Avalia a mão de todos os jgoadores juntamente com as cartas da mesa para gerar uma lista contendo
        /// todos os jogadores vencedores, empates e perdedores da partida.
        /// <note type="note"> 
        /// A lista de jogadores avaliada sera por padrão a <see cref="PokerTable.betplayers"/> que estiverem eletivos
        /// para a premiação final, <b>isto significa que jogadores em FOLD não serão avaliados e não estarao nem na lista de
        /// derrotados.</b>
        /// </note>
        /// </summary>
        /// <returns>EvaluatedHoldemHand </returns>
        public EvaluatedHoldemHand GetEvaluatedHandOdds()
        {
            List<BetPlayer> betplayers = matchActions.Get_ActiveBetPlayers();

            List<Card> boardCards = pokertable.boardCards.ToList();

            try
            {
                if (betplayers.Count < 0)
                    return null;
                if (boardCards.Count < 5)
                    return null;

                string board = pokertable.getTableCardsEval();
                int playersCount = betplayers.Count;

                long[] winss = new long[playersCount];
                long[] ties = new long[playersCount];
                long[] losses = new long[playersCount];
                List<string> pokets = new List<string>();
                List<BetPlayer> __winners = new List<BetPlayer>();
                List<BetPlayer> __ties = new List<BetPlayer>();
                List<BetPlayer> __losses = new List<BetPlayer>();

                long totalhands = 0;

                foreach (BetPlayer p in betplayers)
                    pokets.Add(p.holdemhand.PocketCards);

                Hand.HandOdds(pokets.ToArray(), board, "", winss, ties, losses, ref totalhands);

                //Winners
                for (int a = 0; a < playersCount; a++)
                {
                    if (winss[a] > 0)
                        __winners.Add(betplayers[a]);
                    else if (ties[a] > 0)
                        __ties.Add(betplayers[a]);
                    else if (losses[a] > 0)
                        __losses.Add(betplayers[a]);
                }

                EvaluatedHoldemHand odds = new EvaluatedHoldemHand(__winners, __ties, __losses, totalhands);
                betplayers.Clear();
                boardCards.Clear();

                return odds;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return null;

            }
        }
        /// <summary>
        /// Avalia a mão de todos os jgoadores juntamente com as cartas da mesa para gerar uma lista contendo
        /// todos os jogadores vencedores, empates e perdedores da partida.
        /// <note type="note"> 
        /// A lista de jogadores avaliada sera a uma lista customziado enviada por você, porém os jogadores que não estiverem 
        /// para a premiação final nesta lista, não serao avaliados, <b>isto significa que jogadores em FOLD não serão avaliados e não estarao nem na lista de
        /// derrotados.</b>
        /// </note>
        /// </summary>
        /// <param name="players">Lista customizado por você, contendo os jogadores que você deseja avaliar a mão!</param>
        /// <returns></returns>
        public EvaluatedHoldemHand GetEvaluatedHandOdds(List<BetPlayer> players)
        {
            List<BetPlayer> betplayers = new List<BetPlayer>(players);
            betplayers.RemoveAll(r => r.Get_ActionsCount(BetType.fold) > 0);

            List<Card> boardCards = pokertable.boardCards.ToList();

            try
            {
                if (betplayers.Count < 0)
                    return null;
                if (boardCards.Count < 5)
                    return null;

                string board = pokertable.getTableCardsEval();
                int playersCount = betplayers.Count;

                long[] winss = new long[playersCount];
                long[] ties = new long[playersCount];
                long[] losses = new long[playersCount];
                List<string> pokets = new List<string>();
                List<BetPlayer> __winners = new List<BetPlayer>();
                List<BetPlayer> __ties = new List<BetPlayer>();
                List<BetPlayer> __losses = new List<BetPlayer>();

                long totalhands = 0;

                foreach (BetPlayer p in betplayers)
                    pokets.Add(p.holdemhand.PocketCards);

                Hand.HandOdds(pokets.ToArray(), board, "", winss, ties, losses, ref totalhands);

                //Winners
                for (int a = 0; a < playersCount; a++)
                {
                    if (winss[a] > 0)
                        __winners.Add(betplayers[a]);
                    else if (ties[a] > 0)
                        __ties.Add(betplayers[a]);
                    else if (losses[a] > 0)
                        __losses.Add(betplayers[a]);
                }

                EvaluatedHoldemHand odds = new EvaluatedHoldemHand(__winners, __ties, __losses, totalhands);
                betplayers.Clear();
                boardCards.Clear();

                return odds;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return null;

            }
        }
        //Clear cards from all player list
        private void resetPlayersCards()
        {
            List<BetPlayer> betplayers = pokertable.betplayers.ToList();
            //Update EVAL HAND
            foreach (BetPlayer b in betplayers)
            {
                b.holeCards.Clear();
            }
        }
        #region Extend Classes
        private class Extend
        {
            //Usado para indicar o jogador da vez na partida em andamento
            private MatchGame match;
            private BetPlayer __ext__BetPlayer_Current;
            public BetPlayer GetCurrentBetPlayer()
            {
                return __ext__BetPlayer_Current;
            }
            public void SetCurrentBetPlayer(BetPlayer _cur)
            {
                __ext__BetPlayer_Current = _cur;
                MatchEventArgs args = new MatchEventArgs(match);
                match.Event_ChangeCurrentPlayer(args);
            }

            //Usado para indicar o Turno atual da partida
            private BetTurn __ext__turn;
            public BetTurn GetTurn()
            {
                return __ext__turn;
            }
            public void SetTurn(BetTurn _turn)
            {


                if (_turn == match.turn)
                {
                    return;
                }
                __ext__turn = _turn;

                MatchEventArgs args = new MatchEventArgs(match);
                match.Event_ChangeTurn(args);

                if (match.turn == BetTurn.preflop || match.turn == BetTurn.none)
                {
                    match.pokertable.boardCards.Clear();
                }
                if (match.turn == BetTurn.flop)
                {
                    match.GetFlopCards();
                }
                else if (match.turn == BetTurn.turn)
                {
                    match.GetTurnCard();
                }
                else if (match.turn == BetTurn.river)
                {
                    match.GetRiverCard();
                }
            }

            public Extend(MatchGame matchGame)
            {
                match = matchGame;

            }


        }

        #endregion
        #region EVENTS
        protected virtual void Event_ChangeCurrentPlayer(MatchEventArgs e)
        {
            EventHandler_Matches<MatchEventArgs> handler = OnBetPlayer_Current;
            if (handler != null)
            {
                handler(e);
            }
        }
        protected virtual void Event_ChangeTurn(MatchEventArgs e)
        {
            EventHandler_Matches<MatchEventArgs> handler = OnTurn;
            if (handler != null)
            {
                handler(e);
            }
        }

        /// <summary>
        /// Disparado sempre que o <see cref="MatchGame.BetPlayer_Current"/> for atualizado
        /// </summary>
        public event EventHandler_Matches<MatchEventArgs> OnBetPlayer_Current;
        /// <summary>
        /// Disparado sempre que o turno da partida, <see cref="MatchGame.turn"/> for atualziado
        /// </summary>
        public event EventHandler_Matches<MatchEventArgs> OnTurn;

        #endregion
    }
    /// <summary>
    /// Evaluates final hands of all players in a match and automatically create list of POTS, containing the actual value of each player
    /// classified shall be entitled to the total raised table.
    /// <note type="note"> 
    /// Use this method to classify all players in the match, with the exact value that each player will receive on the final pot!
    /// </note>
    /// </summary>
    /// <example>
    /// This example shows the use of the classification system of final hands with award evaluation to the pot!
    /// Is not included here, the creation of players, starting start creating actions etc.
    /// Copy and Paste this example in your project that already has a smart management of a match!
    /// <code>
    /// using UnityEngine;
    /// using System.Collections;
    /// using System.Collections.Generic;
    /// using PokerCollections;
    /// using PokerCollections.MatchGame;
    ///
    /// public class PoteTeste : MonoBehaviour {
    ///
    ///        void Start()
    ///        {
    ///            StartCoroutine("start");
    ///        }
    ///        IEnumerator start()
    ///        {
    ///
    ///            PokerTable table = new PokerTable(true);
    ///            MatchGame match = new MatchGame(this, table, true);
    ///
    ///            //Example using POT to verify participation of profits from each player
    ///            EvaluatedHoldemPot potmananger = new EvaluatedHoldemPot(this, match);
    ///
    ///            while (!potmananger.isDone)
    ///                yield return null;
    ///
    ///            <![CDATA[List<EvaluatedHoldemPot.POT> pots = potmananger.Get_pots();]]> 
    ///
    ///            //List and show POTS with their players and awards of the pot
    ///            foreach (EvaluatedHoldemPot.POT pot in pots)
    ///            {
    ///                Debug.Log(pot.betplayer.name + ": " + pot.evalueType.ToString() + " " + pot.chips);
    ///            }
    ///
    ///            //Rest pot that can not be awarded to players due to the division of correction in case of ties!
    ///            Debug.Log("LeftOver " + potmananger.leftOver);
    ///            yield return null;
    ///        }
    ///    }
    /// </code>
    /// </example>
    public class EvaluatedHoldemPot
    {

        //Classifica a contribuição total que cada jogador possui em relação ao pote total da mesa.
        //Veja também <see cref="POT"/>, usado para a avaliação final de premios aos jogadores da partida!       
        private class CONTRIBUITIONS : ICloneable
        {
            /// <summary>
            /// Define a ordem de avaliação de cada pote. 
            /// Ex: 1 = Vencedor primario, 2 = secundario etc.
            /// <note type="note">
            /// Observe: Pode haver identificadores iguais, caso existe mais de um vencedor
            /// por identificador de pote.
            /// ex: Betplayer1 = tie 1, Betplayer2 = tie 1. Significando que o jogador 1 e 2 venceram empatados
            /// no pote primário!
            /// </note>
            /// </summary>
            public int EvaluatedID;
            /// <summary>
            /// Informa o tipo de validacao do pote. (Vencedor, Empate)
            /// </summary>
            public EvaluatedType evalueType;
            /// <summary>
            /// Jogador premiado
            /// </summary>
            public List<BetPlayer> betplayers { get; private set; }
            /// <summary>
            /// dinheiro ganho no premio
            /// </summary>
            public double chips { get; private set; }

            public CONTRIBUITIONS(int id, List<BetPlayer> _betplayers, double _chips)
            {
                betplayers = _betplayers;
                chips = _chips;
                EvaluatedID = id;
                evalueType = EvaluatedType.loose;
            }


            //Valia novos potes em ganhadores x contribuição.
            //Retorna lista de pots gerados
            public List<POT> EvaluatedPots(List<BetPlayer> winners)
            {
                //Somente jogadores vencedores que tem direito a este POTE vao ficar na nova lista
                List<BetPlayer> players = new List<BetPlayer>(betplayers);
                foreach (BetPlayer p in betplayers)
                    if (winners.Exists(r => r == p) == false)
                        players.Remove(p);


                EvaluatedType evType = EvaluatedType.winner;
                if (winners.Count > 1)
                    evType = EvaluatedType.tie;

                List<POT> pots = new List<POT>();


                foreach (BetPlayer p in players)
                {
                    double total = (chips / players.Count).Floor(2);
                    POT pot = new POT(evType, p, total, false);
                    pots.Add(pot);
                }

                players.Clear();

                return pots;
            }

            public object Clone()
            {
                return MemberwiseClone();
            }
        }
        /// <summary>
        /// Classifica Potes finais depois da avaliação de maos de cada jogadore da partida.
        /// Esta classe guarda o valor exato que determinado jogador terá direito de receber ao final da avaliação.
        /// </summary>
        public class POT : ICloneable
        {
            /// <summary>
            /// Informa o tipo de validacao do pote. (Vencedor, Empate)
            /// </summary>
            public EvaluatedType evalueType;
            /// <summary>
            /// Jogador premiado
            /// </summary>
            public BetPlayer betplayer { get; private set; }
            /// <summary>
            /// dinheiro ganho no premio
            /// </summary>
            public double chips { get; private set; }
            /// <summary>
            /// Identifica se este premia é um valor de devolução da mesa para o jogador.
            /// Ação tipica quando um jogador aposta mais do que todos os demais jogadores da mes. Por padrão devolvemos este
            /// valor ao jogador, ja que ele não fara ação com os demais!
            /// </summary>
            public bool recoins { get; private set; }

            public POT(EvaluatedType evType, BetPlayer _betplayer, double _chips, bool _recoins)
            {
                evalueType = evType;
                betplayer = _betplayer;
                chips = _chips;
                recoins = _recoins;
            }

            public object Clone()
            {
                return MemberwiseClone();
            }
        }

        //Lista de contribuições
        private List<CONTRIBUITIONS> _contribuitions;
        //Lista de potes gerados apartir de contribuições
        protected List<POT> _pots;

        /// <summary>
        /// Componente origem
        /// </summary>
        public MonoBehaviour component { get; private set; }
        /// <summary>
        /// Gerenciador de partidas origem
        /// </summary>
        public MatchGame match { get; private set; }
        /// <summary>
        /// falso enquanto o sistema estiver processando a avalição final dos jogadores e seus premios.
        /// true quando finalizar o processamento.
        /// <note type="note">
        /// Utilize IENUMERATOR para aguardar este valor ficar true e dar continuidade em seu script. 
        /// </note>
        /// </summary>
        public bool isDone { get; private set; }
        /// <summary>
        /// Valor "Resto", "sobra", do total arrecado subtraido do total premiado aos jogadors!
        /// Este valor sera maior do que zero em caso de divisão de potes para jogadores empatados, cujo valor
        /// resultante da divisão contenha restos que não possam ser dados ao jogador vencedor, exemplo: Milhagens.
        /// O Total destes restos sera leftOver, e você poderá reutilizar este valor da maneira que melhor convir!
        /// </summary>
        public double leftOver
        {
            get
            {
                double total = get_totalChips_avaliable();
                if (total < 0)
                    total = 0;

                return total;
            }
        }

        /// <summary>
        /// Classifica todos os jogadores vencedores e empatados da partida e cria automaticamente Potes cotendo
        /// o valor exato que cada jogador avaliado devera receber sobre o total arrecado na partida!
        /// </summary>
        /// <param name="monocomponent">componente origem</param>
        /// <param name="matchGame">gerenciador de partida</param>
        public EvaluatedHoldemPot(MonoBehaviour monocomponent, MatchGame matchGame)
        {
            _contribuitions = new List<CONTRIBUITIONS>();
            _pots = new List<POT>();
            component = monocomponent;
            match = matchGame;
            isDone = false;

            component.StartCoroutine(evalue_pots());

        }

        //Avalia a mão final dos jogadores classificando vencedores, empates e derrotas e criando apartir
        //de suas contribuições uma lista de potes com o premio que cada jogador tera direito em fichas sobre o
        //total arrecadado na partida!
        private IEnumerator evalue_pots()
        {
            //Cria lista de contribuições
            create_contribs();
            //Lista temporaria de jogadores ativos eletivos ao premio
            List<BetPlayer> players = match.matchActions.Get_ActiveBetPlayers();
            //Validação de maos
            EvaluatedHoldemHand eval;
            //Total disponível para premiação dos jogadores avaliados
            double total = get_totalChips_avaliable();

            while (total > 0.0d)
            {
                if (players.Count() <= 0) { break; }

                eval = match.GetEvaluatedHandOdds(players);

                if (eval.winners.Count == 0 && eval.ties.Count == 0) { break; }

                foreach (CONTRIBUITIONS contrib in _contribuitions)
                {
                    List<POT> pots_generate = default(List<POT>);

                    if (eval.winners.Count > 0)
                    {
                        pots_generate = contrib.EvaluatedPots(eval.winners);
                    }
                    else if (eval.ties.Count > 0)
                    {
                        pots_generate = contrib.EvaluatedPots(eval.ties);
                    }

                    foreach (POT pot in pots_generate)
                    {
                        _pots.Add(pot);
                        players.Remove(pot.betplayer);
                    }
                }

                total = get_totalChips_avaliable();
                yield return null;

            }

            //Criando pote de devolução de premio ao maior apostador em caso de exedente de aposta em relação
            //aos demais jogadores da partida
            if (match.matchActions.Get_BidderChipsReturn() > 0)
            {
                POT pot = new POT(EvaluatedType.recoins, match.BetPlayer_Bidder, match.matchActions.Get_BidderChipsReturn(), true);
                _pots.Add(pot);
            }

            isDone = true;
        }

        //Cria lista de contribuições para apartir dela criar lista de potes.
        //A lista de contribuicao e importante para saber quais jogadores tem direito a determinado
        //montante do valor total da mesa!
        private void create_contribs()
        {
            _contribuitions.Clear();

            //Copia da lista de ações ordenada por ID
            List<MatchAction> tmp_Actions = match.matchActions.Get_Actions();
            //Copia lista de jogadores
            List<BetPlayer> tmp_Players = match.pokertable.betplayers.ToList();
            List<BetPlayer> tmp_AllinPlayers = match.pokertable.betplayers.ToList();
            //Listando somente jogadores em allin
            tmp_AllinPlayers.RemoveAll(r => r.Is_Allin() == false);
            //Criando potes Apartir de jogadores ALLINS 
            double bet_min_allin = 0;
            int potID = 0;
            int allinsCount = tmp_AllinPlayers.Count();
            #region ALLINPOTs

            if (allinsCount > 0)
                bet_min_allin = tmp_AllinPlayers.Min(r => r.Get_BetsAmount());

            for (int a = 0; a < allinsCount; a++)
            {
                potID = a;
                BetPlayer minplayer = tmp_AllinPlayers.Find(r => r.Get_BetsAmount() == bet_min_allin);
                if (minplayer != null)
                {
                    MatchAction lastAction = minplayer.Get_BetActions().LastOrDefault();
                    List<MatchAction> acts = tmp_Actions.FindAll(r => r.bet_turn == lastAction.bet_turn);
                    if (lastAction != null && acts.Count > 0)
                    {
                        List<BetPlayer> actPlayers = new List<BetPlayer>();
                        double chips = 0;
                        foreach (MatchAction action in acts)
                        {
                            chips += correctBetChip(action);
                            if (actPlayers.Exists(r => r == action.match_player.betPlayer) == false)
                                actPlayers.Add(action.match_player.betPlayer);

                            tmp_Actions.Remove(action);
                        }

                        CONTRIBUITIONS pot = new CONTRIBUITIONS(potID, actPlayers, chips);
                        _contribuitions.Add(pot);

                        acts.Clear();

                    }
                    tmp_AllinPlayers.Remove(minplayer);
                }

                if (tmp_AllinPlayers.Count > 0)
                    bet_min_allin = tmp_AllinPlayers.Min(r => r.Get_BetsAmount());
                else
                    break;
            }
            #endregion

            //Criando pote final com as ações derradeiras
            #region FINAL POT
            if (tmp_Actions.Count > 0)
            {
                List<BetPlayer> restPlayers = new List<BetPlayer>();
                double restChips = 0;
                foreach (MatchAction action in tmp_Actions)
                {
                    restChips += correctBetChip(action);
                    if (restPlayers.Exists(r => r == action.match_player.betPlayer) == false)
                        restPlayers.Add(action.match_player.betPlayer);
                }

                CONTRIBUITIONS pot = new CONTRIBUITIONS(potID, restPlayers, restChips);
                _contribuitions.Add(pot);
            }
            #endregion

            //Limpando listas temporarias
            tmp_Actions.Clear();
            tmp_Players.Clear();
            tmp_AllinPlayers.Clear();
        }


        /// <summary>
        /// Valor total de arrecadacao com as contribuições no momento.    
        /// </summary>
        /// <returns>Total de CONTRIBUITIONS.chips</returns>
        private double get_totalChips_contrib()
        {
            double chips = 0;
            foreach (CONTRIBUITIONS contrib in _contribuitions)
            {
                chips += contrib.chips;
            }

            return chips;
        }

        /// <summary>
        /// Valor total de premios distribuido nos potes existentes
        /// </summary>
        /// <returns>Total de POT.chips</returns>
        private double get_totalChips_pots()
        {
            double chips = 0;
            foreach (POT pot in _pots)
            {
                chips += pot.chips;
            }

            return chips;
        }

        /// <summary>
        /// Retorna o total de dinheiro disponivel para premiação.
        /// </summary>
        /// <returns>
        /// (Total Contribuição) - (TotalArrecadado em Potes) = Total avaliado
        /// </returns>
        private double get_totalChips_avaliable()
        {
            double contrib = get_totalChips_contrib();
            double pots = get_totalChips_pots();
            double total = contrib - pots;

            return total;
        }

        /// <summary>
        /// BUGFIX: 
        /// Esta ação é usada para verificar uma ação afim de remover o valor de extorno dela, caso exista!
        /// Isto evita a criação de potes com o valor superior ao total da mesa, ja que na mesa temos o desconto do extorno automaticamente
        /// </summary>
        /// <param name="action">Ação cuja aposta sera verifica e corrigida</param>
        /// <returns>Valor de aposta subtratido do bidderContribuition se existir</returns>
        private double correctBetChip(MatchAction action)
        {
            double result = action.bet_chips;
            BetPlayer bidder = match.BetPlayer_Bidder;
            if (bidder.Get_PoteContribReturn() > 0)
            {
                List<MatchAction> acts = match.matchActions.Get_Actions().FindAll(r => r.match_player.betPlayer == bidder);
                MatchAction act = acts.LastOrDefault();
                if (act == action)
                {
                    result = act.bet_chips - bidder.Get_PoteContribReturn();
                }
            }
            return result;
        }

        /// <summary>
        /// Lista de Potes contendo o valor final que cada jogador avaliado tem direito.
        /// </summary>
        /// <returns>Lista de EvaluatedHoldemPot.POT</returns>
        internal List<POT> Get_pots()
        {
            return _pots.Clone();
        }
    }

    /// <summary>
    /// Tipos de ações permitidas para um jogador, em uma partida em execução.
    /// Utilize esta classe para setar e gerenciar permições de ação a um jogador.
    /// </summary>
    public class MatchAllowedAction
    {
        public MatchGame matchGame { get; private set; }
        public ActionBet actionBet { get; private set; }
        public ActionCall actionCall { get; private set; }
        public ActionCheck actionCheck { get; private set; }
        public ActionRaise actionRaise { get; private set; }
        public ActionFold actionFold { get; private set; }
        public BetPlayer betPlayer { get; private set; }

        public bool isOver
        {
            get
            {
                if (actionBet.allow == false && actionCall.allow == false && actionRaise.allow == false && actionCheck.allow == false && actionFold.allow == false)
                    return true;
                else
                    return false;
            }
        }
        public bool isAutoBet
        {
            get
            {
                BetTurn turn = matchGame.turn;
                if (turn == BetTurn.preflop && (matchGame.matchActions.Get_Count(turn) == 0 || matchGame.matchActions.Get_Count(turn) == 1) == true)
                    return true;
                else
                    return false;
            }
        }
        public bool isAutoSmallBet
        {
            get
            {
                BetTurn turn = matchGame.turn;
                if (turn == BetTurn.preflop && matchGame.matchActions.Get_Count(turn) == 0)
                    return true;
                else
                    return false;
            }
        }
        public bool isAutoBigBet
        {
            get
            {
                BetTurn turn = matchGame.turn;
                if (turn == BetTurn.preflop && matchGame.matchActions.Get_Count(turn) == 1)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Inicializa uma nova requisição de ações permitidas para um determinado jogador.
        /// <para>
        /// Este método não é indicado para inicializações manuais. 
        /// Se você deseja receber as ações permitidas para o jogador atual da vez em uma partida e turno em andamento,
        /// utilize <see cref="MatchGameExtensions.Get_AllowAction(BetTurn, MatchGame)"/>
        /// </para>
        /// <example>
        /// <code>
        ///  BetTurn turn = BetTurn.flop;
        ///  MatchAllowedAction allowActions = turn.Get_AllowAction(currentMatch);        
        ///  if(allowActions.actionBet.allow)
        ///  {
        ///     allowActions.actionBet.Execute(10);
        ///  }
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="match">Partida em andamento</param>
        /// <param name="actionBet">new <see cref="ActionBet"/> </param>
        /// <param name="actionCall">new <see cref="ActionCall"/></param>
        /// <param name="actionCheck">new <see cref="ActionCheck"/></param>
        /// <param name="actionRaise">new <see cref="ActionRaise"/></param>
        /// <param name="actionFold">new <see cref="ActionFold"/></param>
        /// <param name="betPlayer">Jogador dono das ações. Normalmente este é o Jogador da vez.</param>
        public MatchAllowedAction(MatchGame match, ActionBet actionBet, ActionCall actionCall, ActionCheck actionCheck, ActionRaise actionRaise, ActionFold actionFold, BetPlayer betPlayer)
        {
            this.matchGame = match;
            this.actionBet = actionBet;
            this.actionCall = actionCall;
            this.actionCheck = actionCheck;
            this.actionFold = actionFold;
            this.actionRaise = actionRaise;
            this.betPlayer = betPlayer;
        }
        public MatchAction Execute(BetType betType)
        {
            MatchAction result = null;

            switch (betType)
            {
                case BetType.call:
                    result = actionCall.Execute();
                    break;
                case BetType.check:
                    result = actionCheck.Execute();
                    break;
                case BetType.fold:
                    result = actionFold.Execute();
                    break;
            }

            return result;
        }
        public MatchAction Execute(BetType betType, double value)
        {
            MatchAction result = null;

            switch (betType)
            {
                case BetType.bet:
                    result = actionBet.Execute(value);
                    break;
                case BetType.rise:
                    result = actionRaise.Execute(value);
                    break;
            }

            return result;
        }
        public MatchAction AutoExecute()
        {

            //Prioridaes
            //> Apostas automaticas, smaller e bigger
            if (isOver)
            {
                Debug.Log("Nenhuma acao permitda");
                return null;
            }

            if (isAutoBet)
            {
                if (isAutoSmallBet)
                {
                    return Execute(BetType.bet, actionBet.bet_min);

                }
                else if (isAutoBigBet)
                {
                    return Execute(BetType.rise, actionBet.bet_min);

                }
                else
                {
                    Debug.LogError("Erro: allowAction não contem true para smaller e nem bigger");
                }
            }
            else
            {

                //Usando o comando aposta
                //Qual a quantidade a apostar?

                if (actionCheck.allow)
                {
                    //Debug.LogWarning(betPlayer.name + " Check Permitido");
                    if (!betPlayer.hasVoluntaryBet())
                    {
                        return Execute(BetType.check);
                    }
                    else
                    {
                        int rand = 4;
                        int res = UnityEngine.Random.Range((int)1, rand);
                        if (res % 2 > 0)
                        {
                            return Execute(BetType.check);
                        }
                    }
                }


                if (actionBet.allow)
                {
                    return Execute(BetType.bet, actionBet.bet_min);
                }
                /*
                if (actionRaise.allow)
                {                   
                    int rand = 4;
                    int res = UnityEngine.Random.Range((int)1, rand);
                    if (res % 2 > 0)
                    {

                        int raiseRand = UnityEngine.Random.Range((int)1, 10);
                        if (raiseRand == 10)
                            return Execute(BetType.rise, actionRaise.bet_max);
                        else if (raiseRand == 5)
                            return Execute(BetType.rise, actionRaise.bet_max / 2);
                        else
                            return Execute(BetType.rise, actionRaise.bet_min);
                    }
                }
                */

                if (actionCall.allow)
                {
                    return Execute(BetType.call);
                }
            }

            return null;
        }
        public override string ToString()
        {
            string result = "BetPlaye: " + betPlayer.name + "\n";

            if (actionBet.allow)
            {
                if (isAutoSmallBet)
                    result += "Bet: " + actionBet.bet_min.ToString("C2") + "/" + actionBet.bet_max.ToString("C2") + " Auto Small\n";
                else
                    result += "Bet: " + actionBet.bet_min.ToString("C2") + "/" + actionBet.bet_max.ToString("C2") + "\n";
            }
            if (actionCall.allow)
                result += "Call: " + actionCall.bet_min.ToString("C2") + "\n";
            if (actionRaise.allow)
            {
                if (isAutoBigBet)
                    result += "Raise: " + actionRaise.bet_min.ToString("C2") + "/" + actionRaise.bet_max.ToString("C2") + " Auto Big\n";
                else
                    result += "Raise: " + actionRaise.bet_min.ToString("C2") + "/" + actionRaise.bet_max.ToString("C2") + "\n";
            }
            if (actionCheck.allow)
                result += "Check \n";
            if (actionFold.allow)
                result += "Fold \n";

            if (isOver)
                result += " <color=#FF0000>Nothing Action Allowed</color>";

            return result;
        }

    }

    public class MatchGame_MonoBehavior : MonoBehaviour
    {
        public virtual void OnDestroy()
        {
            MatchGame game = MatchGameListner.Matches.Get_MatchFromComponent(this);
            if (game != null)
                MatchGameListner.Matches.Remove(game);
        }


    }
    //Lista todas as MatchGame em andamento
    public static class MatchGameListner
    {

        public static MatchGameList<MatchGame> Matches = new MatchGameList<MatchGame>();

        /// <summary>
        /// Adiciona uma nova matchGame à lista. 
        /// <note type="note">Sempre que uma new MatchGame é inicializada, éla é adicionada automáticamente a esta lista</note>
        /// </summary>
        /// <param name="match"></param>
        public static void AddNewMatch(MatchGame match)
        {
            Matches.Add(match);
        }
        public static List<MatchAction> Get_ActionsFromPlayer(BetPlayer betplayer, MatchGame match)
        {
            List<MatchAction> actions = match.matchActions.Get_Actions(betplayer);
            return actions;

        }
        public static List<MatchAction> Get_ActionsFromPlayer(BetPlayer betplayer, MatchGame match, BetType betType)
        {
            List<MatchAction> actions = match.matchActions.Get_Actions(betplayer, betType);
            return actions;
        }
        public static List<MatchAction> Get_ActionsFromPlayer(BetPlayer betplayer, MatchGame match, BetTurn turn)
        {
            List<MatchAction> actions = match.matchActions.Get_Actions(betplayer, turn);
            return actions;

        }

    }
}