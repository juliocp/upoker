﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using uPoker;

/*
Here we will use a simple but very functional library to work with the validation engine uPoker hands. 
This will allow you to manage the starting information, facilitating the creation and management of all 
components necessary for the development of a poker game.
Note that the PokerCollections library is open and has the intention to be an initial step you implement
new methods that meet all your needs in creating your game.
> Feel free to modify and implement your needs.
(Julio C.Pereira) juliocp.next@gmail.com
 */
namespace PokerCollections.Behavior
{
    using Interfaces;

    public class PokerBehaviour: MonoBehaviour
    {
        [Serializable]
        public class PlayerUI
        {

            public string player_name;
            public int player_id;
            public int place_id;
            public double chips_amount;

            public BetPlayer betplayer { get; private set; }
                      
            public void SetComponent(object component)
            {
                betplayer = new BetPlayer(player_name, component);
                betplayer.overhideID = player_id;
                betplayer.overhidePlaceID = place_id;
                betplayer.AddChip(chips_amount);
            }

               

        }
    }
}

