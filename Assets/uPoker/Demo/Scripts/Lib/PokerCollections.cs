﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using uPoker;

/*
Here we will use a simple but very strong and functional library to work with the Evaluation engine uPoker. 
This will allow you to manage the starting information, facilitating the creation and management of all components necessary for the development of a poker game.
This is High-Level layer that containing abstracted components of the uPoker library.
With PokerCollections you will have all these components in Unity3D, necessary to work with your poker game and therefore, you will not need to spend time to re-create all the crucial components of your game.
Here you will be able to also evaluate your hands to know winning players, losers and ties.
This package is highly suitable as it is the starting point to develop your game.
You will be able to extend the functions of this library to develop all the other mechanics of your game!
    
Note that the PokerCollections library is open and has the intention to be an initial step to you implement new methods that meet all your needs in creating your game.
*/
namespace PokerCollections
{
    using Interfaces;
    using EventHandler;

    using System.Reflection;
    /// <summary>
    /// Addicional methods extend for inumerators
    /// </summary>
    public static class PokerCollectionsExtsions
    {
        public static List<T> Clone<T>(this List<T> collection) where T : ICloneable
        {
            return collection.Select(item => (T)item.Clone()).ToList();
        }
        public static IEnumerable<TResult> SelectConsecutive<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TSource, TResult> selector)
        {
            using (IEnumerator<TSource> iterator = source.GetEnumerator())
            {
                if (!iterator.MoveNext())
                {
                    yield break;
                }
                TSource prev = iterator.Current;
                while (iterator.MoveNext())
                {
                    TSource current = iterator.Current;
                    yield return selector(prev, current);
                    prev = current;
                }
            }
        }
        public static CardsList<Card> ToCardsList<Card>(this IEnumerable<Card> source) where Card : IPokerCard
        {
            if (source == null)
            {
                throw new ArgumentNullException(source.ToString());
            }

            return new CardsList<Card>(source);

        }
        public static PlayerList<BetPlayer> ToPlayerList<BetPlayer>(this IEnumerable<BetPlayer> source) where BetPlayer : IPokerPlayer
        {
            if (source == null)
            {
                throw new ArgumentNullException(source.ToString());
            }

            return new PlayerList<BetPlayer>(source);

        }
        public static void UpdateBoardCards(this PlayerList<BetPlayer> betPlayers, List<Card> boardCards)
        {
            foreach (BetPlayer p in betPlayers)
                p.setBoard(boardCards);

        }
        public static void UpdateBoardCards(this PlayerList<BetPlayer> betPlayers, string boardCards)
        {
            foreach (BetPlayer p in betPlayers)
                p.setBoard(boardCards);

        }
        public static void UpdateBoardCards(this PlayerList<BetPlayer> betPlayers)
        {
            foreach (BetPlayer p in betPlayers)
                p.setBoard(p.pokerTable.boardCards.ToList());

        }
        public static CardsList<Card> ShuffleCardsList<Card>(this IEnumerable<Card> source) where Card : IPokerCard
        {
            System.Random rnd = new System.Random();
            source = source.OrderBy<Card, int>((item) => rnd.Next());
            return source.ToCardsList();

        }

        public static BetTurn Next(this BetTurn turn)
        {
            int c = (int)turn;
            int r = c + 1;
            if (r > (int)BetTurn.NumberOfTypes - 1)
                r = (int)BetTurn.NumberOfTypes - 1;

            return (BetTurn)r;
        }
        public static BetTurn Prev(this BetTurn turn)
        {
            int c = (int)turn;
            int r = c - 1;
            if (r < 0)
                r = 0;

            return (BetTurn)r;
        }
        public static BetTurn Last(this BetTurn turn)
        {
            int last = (int)BetTurn.NumberOfTypes - 1;
            return (BetTurn)last;
        }
        public static BetTurn First(this BetTurn turn)
        {
            int first = 0;
            return (BetTurn)first;
        }
        /// <summary>
        /// returns true if the player is in the hoplayers list. Otherwise it will return false.
        /// Notice something important here:
        /// Even if this method returns false, this does not mean that the player is in the list of active betplayers.This guarantee will depend on you.
        /// </summary>
        /// <param name="betplayer">betplayer origin to search</param>
        /// <returns>true or false</returns>
        public static bool isHoplayer(this BetPlayer betplayer)
        {
            bool result = false;

            if (betplayer.pokerTable != null)
                if (betplayer.pokerTable.hoplayers.Exists(r => r == betplayer))
                    result = true;
                return result;
        }

        public static void OnAddBetPlayer_Subscribe(this PokerTable pokerTable, EventHandler_Player<BetPlayerEventArgs> OnAdd)
        {
            PlayerList<BetPlayer> betplayers = pokerTable.betplayers;
            betplayers.OnAdd += OnAdd;
        }
        public static void OnRemoveBetPlayer_Subscribe(this PokerTable pokerTable, EventHandler_Player<BetPlayerEventArgs> OnRemove)
        {
            PlayerList<BetPlayer> betplayers = pokerTable.betplayers;
            betplayers.OnRemove += OnRemove;
        }
        public static void OnAddBetPlayer_Unsbscribe(this PokerTable pokerTable, EventHandler_Player<BetPlayerEventArgs> OnAdd)
        {
            PlayerList<BetPlayer> betplayers = pokerTable.betplayers;
            betplayers.OnAdd -= OnAdd;
        }
        public static void OnRemoveBetPlayer_Unsbscribe(this PokerTable pokerTable, EventHandler_Player<BetPlayerEventArgs> OnRemove)
        {
            PlayerList<BetPlayer> betplayers = pokerTable.betplayers;
            betplayers.OnRemove -= OnRemove;
        }

        public static void OnAddHoplayer_Subscribe(this PokerTable pokerTable, EventHandler_Player<BetPlayerEventArgs> OnAdd)
        {
            PlayerList<BetPlayer> betplayers = pokerTable.hoplayers;
            betplayers.OnAdd += OnAdd;
        }
        public static void OnRemoveHoplayer_Subscribe(this PokerTable pokerTable, EventHandler_Player<BetPlayerEventArgs> OnRemove)
        {
            PlayerList<BetPlayer> betplayers = pokerTable.hoplayers;
            betplayers.OnRemove += OnRemove;
        }
        public static void OnAddHoplayer_Unsbscribe(this PokerTable pokerTable, EventHandler_Player<BetPlayerEventArgs> OnAdd)
        {
            PlayerList<BetPlayer> betplayers = pokerTable.hoplayers;
            betplayers.OnAdd -= OnAdd;
        }
        public static void OnRemoveHoplayer_Unsbscribe(this PokerTable pokerTable, EventHandler_Player<BetPlayerEventArgs> OnRemove)
        {
            PlayerList<BetPlayer> betplayers = pokerTable.hoplayers;
            betplayers.OnRemove -= OnRemove;
        }


        public static void Events_Subscribe(this PokerTable pokerTable, EventHandler_Table<PokerTableEventArgs> OnAdd, EventHandler_Table<PokerTableEventArgs> OnRemove)
        {
            try
            {
                if (pokerTable.component != null)
                {
                    PokerTables.Tables.OnAdd += OnAdd;
                    PokerTables.Tables.OnRemove += OnRemove;
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        public static void Events_Unscribe(this PokerTable pokerTable, EventHandler_Table<PokerTableEventArgs> OnAdd, EventHandler_Table<PokerTableEventArgs> OnRemove)
        {
            try
            {
                if (pokerTable.component != null)
                {
                    PokerTables.Tables.OnAdd -= OnAdd;
                    PokerTables.Tables.OnRemove -= OnRemove;
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        public static double Floor(this double value, int decimals)
        {
            double adjustment = Math.Pow(10, decimals);
            return Math.Floor(value * adjustment) / adjustment;
        }
        public static double Ceil(this double value, int decimals)
        {
            double adjustment = Math.Pow(10, decimals);
            return Math.Ceiling(value * adjustment) / adjustment;
        }


    }
    /// <summary>
    /// Represents a special type list of poker cards objects that can be accessed by index. 
    /// Provides methods to search, sort, and manipulate lists.
    /// Using this list method, you can attach functions to listen to specific events list manipulation!
    /// </summary>
    /// <typeparam name="Card">The type of elements in the list.</typeparam>
    public class CardsList<Card> : List<Card> where Card : IPokerCard
    {
        private int maxCapacity;
        protected virtual void Event_OnAdd(CardEventArgs e)
        {
            EventHandler<CardEventArgs> handler = OnAdd;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected virtual void Event_OnRemove(CardEventArgs e)
        {
            EventHandler<CardEventArgs> handler = OnRemove;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<CardEventArgs> OnAdd;
        public event EventHandler<CardEventArgs> OnRemove;

        /// <summary>
        /// Component dono desta lista
        /// </summary>
        public object component
        {
            get; private set;
        }
        /// <summary>
        /// Start a new list of instance from an existing list. You can use a different type of list to start if you want this!
        /// </summary>
        /// <param name="source"> the source list</param>
        public CardsList(IEnumerable<Card> source)
        {
            maxCapacity = 0;
            if (this.GetType() != source.GetType())
            {
                foreach (Card t in source)
                    base.Add(t);
            }


        }
        /// <summary>
        /// Start a new list instance
        /// </summary>
        public CardsList()
        {
            maxCapacity = 0;
        }
        public CardsList(int capacity)
        {
            maxCapacity = capacity;
        }
        public CardsList(object context)
        {
            component = context;
        }
        public Card TakeItem()
        {

            Card card = default(Card);

            if (Count > 0)
            {
                card = base[0];
                Remove(card);
            }

            return card;
        }
        public Card TakeItem(int _id)
        {
            Card result = Find(r => r.id == _id);
            if (result != null)
            {
                Remove(result);
            }

            return result;

        }
        public bool TakeItem(Card _item)
        {
            if (_item == null)
                return false;

            Card result = Find(r => r.Equals(_item));

            return Remove(result);
        }
        /// <summary>
        /// Item Next
        /// Search and return the next list item from current item reference
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the next list item from the current item or returns the current item if there is no item in the list after him!</returns>
        public Card Next(Card current)
        {
            Card c = default(Card);

            if (Count <= 0)
                return c;

            int index = IndexOf(current);

            if (index < Count - 1)
                return this.ElementAt(index + 1);

            return current;
        }
        /// <summary>
        /// Item Previous
        /// Search and return the previous list item from current item reference
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the previous list item from the current item or returns the current item if there is no item in the list before him!</returns>
        public Card Prev(Card current)
        {
            Card c = default(Card);

            if (Count <= 0)
                return c;

            int index = IndexOf(current);

            if (index >= 1)
                return this.ElementAt(index - 1);
            return current;
        }
        /// <summary>
        /// Item Relative Next:
        /// Search and return the next list item from current item reference
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the next list item from the current item or returns the first item if there is no item in the list after him!</returns>
        public Card NextRel(Card current)
        {
            Card c = default(Card);

            if (Count <= 0)
                return c;

            if (Count > 1)
            {
                int index = IndexOf(current);

                if (index < Count - 1)
                {
                    return this.ElementAt(index + 1);
                }
                else
                {
                    return this[0];
                }
            }
            else
            {
                return current;
            }
        }
        /// <summary>
        /// Item Relative Previous:
        /// Search and return the prev list item from current item reference
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the prev list item from the current item or returns the last item if there is no item in the list after him!</returns>
        public Card PrevRel(Card current)
        {
            Card c = default(Card);

            if (Count <= 0)
                return c;

            if (Count > 1)
            {
                int index = IndexOf(current);

                if (index >= 1)
                {
                    return this.ElementAt(index - 1);
                }
                else
                {
                    return this[Count - 1];
                }
            }
            else
            {
                return current;
            }
        }
        /// <summary>
        /// Get the first Item of the List
        /// </summary>
        /// <returns>First Item of the List or null if empty list</returns>
        public Card First()
        {
            Card c = default(Card);

            if (Count <= 0)
                return c;

            return this[0];
        }
        /// <summary>
        /// Get the last Item of the List
        /// </summary>
        /// <returns>Last item of the List or null if empty list</returns>
        public Card Last()
        {
            Card c = default(Card);

            if (Count <= 0)
                return c;

            return this[Count - 1];
        }
        /// <summary>
        /// Adiciona uma nova carta à lista.
        /// <note type="note">
        /// Se <see cref="component"/> for igual a um betPlayer ou uma PokerTable, 
        /// A <see cref="Hand"/> do jogador sera atualizada automáticamente.
        /// O Evento <see cref="OnAdd"/>, sera disparado ao ser adicionada!
        /// </note>
        /// </summary>
        /// <param name="item">Carta a ser adicionada</param>
        public new void Add(Card item)
        {
            if (maxCapacity > 0 && Count >= maxCapacity)
                return;

            if (Exists(r => r.Equals(item)) == false)
            {
                base.Add(item);

                if (component != null)
                {
                    if (component.GetType() == typeof(BetPlayer))
                        ((BetPlayer)component).updateHand();
                    if (component.GetType() == typeof(PokerTable))
                        ((PokerTable)component).betplayers.UpdateBoardCards();
                }
                CardEventArgs args = new CardEventArgs(item);
                Event_OnAdd(args);
            }
        }
        /// <summary>
        /// Esvazia lista de cartas.
        /// <note type="note">
        /// Se <see cref="component"/> for igual a um betPlayer ou uma PokerTable, 
        /// a <see cref="Hand"/> do jogador sera atualizada automáticamente.
        /// O Evento <see cref="OnRemove"/>, sera disparado para cada carta removida da lista!
        /// Veja também: <see cref="Remove(Card)"/>!
        /// </note>
        /// </summary>
        public new void Clear()
        {
            foreach (Card card in new List<Card>(this))
            {
                Remove(card);
            }

        }
        /// <summary>
        /// Remove uma carta da lista.
        /// <note type="note">
        /// Se <see cref="component"/> for igual a um betPlayer ou uma PokerTable, 
        /// a <see cref="Hand"/> do jogador sera atualizada automáticamente.
        /// O Evento <see cref="OnRemove"/>, sera disparado ao ser removida!
        /// </note>
        /// </summary>
        /// <param name="item">Carta a ser removida</param>
        public new bool Remove(Card item)
        {
            bool result = base.Remove(item);
            if (result == true)
            {
                if (component != null)
                {
                    if (component.GetType() == typeof(BetPlayer))
                        ((BetPlayer)component).updateHand();
                    if (component.GetType() == typeof(PokerTable))
                        ((PokerTable)component).betplayers.UpdateBoardCards();
                }
                CardEventArgs args = new CardEventArgs(item);
                Event_OnRemove(args);
            }
            return result;
        }

    }
    /// <summary>
    /// Represents a special type list of poker players objects that can be accessed by index. 
    /// Provides methods to search, sort, and manipulate lists.
    /// Using this list method, you can attach functions to listen to specific events list manipulation!
    /// </summary>
    /// <typeparam name="BetPlayer">The type of elements in the list.</typeparam>
    public class PlayerList<BetPlayer> : List<BetPlayer> where BetPlayer : IPokerPlayer
    {
        private int maxCapacity;
        protected virtual void Event_OnAdd(BetPlayerEventArgs e)
        {
            EventHandler_Player<BetPlayerEventArgs> handler = OnAdd;
            if (handler != null)
            {
                handler(e);
            }
        }
        protected virtual void Event_OnRemove(BetPlayerEventArgs e)
        {
            EventHandler_Player<BetPlayerEventArgs> handler = OnRemove;
            if (handler != null)
            {
                handler(e);
            }
        }

        public event EventHandler_Player<BetPlayerEventArgs> OnAdd;
        public event EventHandler_Player<BetPlayerEventArgs> OnRemove;


        /// <summary>
        /// Objeto dono desta lista
        /// </summary>
        public object component { get; private set; }
        /// <summary>
        /// Start a new list of instance from an existing list. You can use a different type of list to start if you want this!
        /// </summary>
        /// <param name="source"> the source list</param>
        public PlayerList(IEnumerable<BetPlayer> source)
        {
            maxCapacity = 0;
            if (this.GetType() != source.GetType())
            {
                foreach (BetPlayer t in source)
                {
                    base.Add(t);
                }
            }

        }
        /// <summary>
        /// Start a new list instance
        /// </summary>
        /// <param name="_maxCapacity"> max capacity allowed for new elements</param>
        public PlayerList(int _maxCapacity)
        {
            maxCapacity = _maxCapacity;
        }
        /// <summary>
        /// Start a new list instance
        /// </summary>
        public PlayerList()
        {
            maxCapacity = 0;
        }
        public PlayerList(object context)
        {
            component = context;
        }
        public BetPlayer TakePlayer()
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();
            BetPlayer player = default(BetPlayer);

            if (Count > 0)
            {
                player = betplayers.FirstOrDefault();
                if (player != null)
                    Remove(player);
            }

            return player;
        }
        public BetPlayer TakePlayer(int _id)
        {

            BetPlayer result = Find(r => r.id == _id);
            if (result != null)
            {
                Remove(result);

            }

            return result;

        }
        public bool TakePlayer(BetPlayer _player)
        {
            if (_player == null)
                return false;

            BetPlayer result = Find(r => r.Equals(_player));

            bool isRemoved = Remove(result);


            return isRemoved;
        }
        /// <summary>
        /// Item Next
        /// Search and return the next list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note>
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the next list item from the current item or returns the current item if there is no item in the list after him!</returns>
        public BetPlayer Next(BetPlayer current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();
            BetPlayer c = default(BetPlayer);

            if (Count <= 0)
                return c;

            int index = betplayers.IndexOf(current);

            if (index < Count - 1)
                return betplayers.ElementAt(index + 1);

            return current;
        }
        /// <summary>
        /// Item Previous
        /// Search and return the previous list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the previous list item from the current item or returns the current item if there is no item in the list before him!</returns>
        public BetPlayer Prev(BetPlayer current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();
            BetPlayer c = default(BetPlayer);

            if (Count <= 0)
                return c;

            int index = betplayers.IndexOf(current);

            if (index >= 1)
                return betplayers.ElementAt(index - 1);
            return current;
        }
        /// <summary>
        /// Item Relative Next:
        /// Search and return the next list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the next list item from the current item or returns the first item if there is no item in the list after him!</returns>
        public BetPlayer NextRel(BetPlayer current)
        {
            BetPlayer c = default(BetPlayer);
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();

            if (Count <= 0)
                return c;

            if (Count > 1)
            {
                int index = betplayers.IndexOf(current);

                if (index < Count - 1)
                {
                    return betplayers.ElementAt(index + 1);
                }
                else
                {
                    return betplayers.FirstOrDefault();
                }
            }
            else
            {
                return current;
            }
        }
        /// <summary>
        /// Item Relative Previous:
        /// Search and return the prev list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the prev list item from the current item or returns the last item if there is no item in the list after him!</returns>
        public BetPlayer PrevRel(BetPlayer current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();

            BetPlayer c = default(BetPlayer);

            if (Count <= 0)
                return c;

            if (Count > 1)
            {
                int index = betplayers.IndexOf(current);

                if (index >= 1)
                {
                    return betplayers.ElementAt(index - 1);
                }
                else
                {
                    return betplayers.LastOrDefault();
                }
            }
            else
            {
                return current;
            }
        }
        /// <summary>
        /// Item Relative Next:
        /// Search and return the next list item from current item place id reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="placeID"> Current Item place id</param>
        /// <returns>returns the next list item from the current item or returns the first item if there is no item in the list after him!</returns>
        public BetPlayer NextRel(int placeID)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();


            BetPlayer cur = default(BetPlayer);

            for (int a = placeID; a < 9; a++)
            {
                int p = a + 1;
                cur = betplayers.Find(r => r.placeId == p);
                if (cur != null)
                    break;
            }

            if (cur == null)
            {
                for (int a = 0; a < 9; a++)
                {
                    int p = a + 1;
                    cur = betplayers.Find(r => r.placeId == p);
                    if (cur != null)
                        break;
                }
            }

            if (cur == null)
                return betplayers.FirstOrDefault();
            else
                return cur;

        }
        /// <summary>
        /// Item Relative Previous:
        /// Search and return the prev list item from current item place id reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="placeID"> Current Item place id</param>
        /// <returns>returns the prev list item from the current item or returns the last item if there is no item in the list after him!</returns>
        public BetPlayer PrevRel(int placeID)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();


            BetPlayer cur = default(BetPlayer);

            for (int a = placeID; a > 1; a--)
            {
                int p = a - 1;
                cur = betplayers.Find(r => r.placeId == p);
                if (cur != null)
                    break;
            }

            if (cur == null)
            {
                for (int a = 10; a > 1; a++)
                {
                    int p = a - 1;
                    cur = betplayers.Find(r => r.placeId == p);
                    if (cur != null)
                        break;
                }
            }

            if (cur == null)
                return betplayers.LastOrDefault();
            else
                return cur;

        }
        /// <summary>
        /// Get the first Item of the List
        /// </summary>
        /// <returns>First Item of the List or null if empty list</returns>
        public BetPlayer First()
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();

            BetPlayer c = default(BetPlayer);

            if (Count <= 0)
                return c;

            return betplayers.FirstOrDefault();
        }
        /// <summary>
        /// Get the last Item of the List
        /// </summary>
        /// <returns>Last item of the List or null if empty list</returns>
        public BetPlayer Last()
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<BetPlayer> betplayers = this.ToList();
            betplayers = betplayers.OrderBy(r => r.placeId).ToList();

            BetPlayer c = default(BetPlayer);

            if (Count <= 0)
                return c;

            return betplayers.LastOrDefault();
        }
        /// <summary>
        /// Add new BetPlayer Item to the List. 
        /// <note type="note"> 
        /// If <see cref="component"/> is a PokerTable, the pokerTable reference on betplayer when automaticaly update;
        /// Triggers Event: <see cref="OnAdd"/>
        /// </note>
        /// </summary>
        /// <param name="_player">BetPlayer Item to add</param>
        public new void Add(BetPlayer _player)
        {

            if (maxCapacity > 0 && Count >= maxCapacity)
                return;

            if (Exists(r => r.Equals(_player)) == false)
            {
                base.Add(_player);

                if (component != null)
                {
                    if (component.GetType() == typeof(PokerTable))
                        _player.overhidePokertable = (PokerTable)component;
                }

                BetPlayerEventArgs args = new BetPlayerEventArgs(_player);
                Event_OnAdd(args);
            }

        }
        /// <summary>
        /// Clar all items of the betPlayers list
        /// <note type="note"> 
        /// Triggers Event: <see cref="OnRemove"/> to eatch item.
        /// </note>
        /// </summary>
        public new void Clear()
        {
            List<BetPlayer> list = new List<BetPlayer>(this);
            foreach (BetPlayer p in list)
            {
                Remove(p);
            }
            list.Clear();
            base.Clear();
        }
        /// <summary>
        /// Remove the current item of the list
        /// <note type="note"> 
        /// If <see cref="component"/> is a PokerTable, the pokerTable reference on betplayer when automaticaly update;
        /// Triggers Event: <see cref="OnRemove"/>;
        /// </note>
        /// </summary>
        /// <param name="_player"></param>
        /// <returns>true if success and false if fail</returns>
        public new bool Remove(BetPlayer _player)
        {
            bool result = base.Remove(_player);
            if (result == true)
            {
                if (component != null)
                {
                    if (component.GetType() == typeof(PokerTable))
                        _player.overhidePokertable = null;
                }
                BetPlayerEventArgs args = new BetPlayerEventArgs(_player);
                Event_OnRemove(args);
            }
            return result;
        }

    }
    public class TableList<PokerTable> : List<PokerTable> where PokerTable : IPokerTable
    {
        private int autoID = 0;
        private int maxCapacity = 0;
        protected virtual void Event_OnAdd(PokerTableEventArgs e)
        {
            EventHandler_Table<PokerTableEventArgs> handler = OnAdd;
            if (handler != null)
            {
                handler(e);
            }
        }
        protected virtual void Event_OnRemove(PokerTableEventArgs e)
        {
            EventHandler_Table<PokerTableEventArgs> handler = OnRemove;
            if (handler != null)
            {
                handler(e);
            }
        }

        public event EventHandler_Table<PokerTableEventArgs> OnAdd;
        public event EventHandler_Table<PokerTableEventArgs> OnRemove;

        /// <summary>
        /// Objeto dono desta lista
        /// </summary>
        public object component { get; private set; }
        /// <summary>
        /// Start a new list of instance from an existing list. You can use a different type of list to start if you want this!
        /// </summary>
        /// <param name="source"> the source list</param>
        public TableList(IEnumerable<PokerTable> source)
        {
            maxCapacity = 0;
            if (this.GetType() != source.GetType())
            {
                foreach (PokerTable t in source)
                {
                    base.Add(t);
                }
            }

        }
        /// <summary>
        /// Start a new list instance
        /// </summary>
        /// <param name="_maxCapacity"> max capacity allowed for new elements</param>
        public TableList(int _maxCapacity)
        {
            maxCapacity = _maxCapacity;
        }
        /// <summary>
        /// Start a new list instance
        /// </summary>
        public TableList()
        {
            maxCapacity = 0;
        }
        public TableList(object context)
        {
            component = context;
        }
        /// <summary>
        /// Item Next
        /// Search and return the next list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note>
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the next list item from the current item or returns the current item if there is no item in the list after him!</returns>
        public PokerTable Next(PokerTable current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<PokerTable> tables = this.ToList();
            tables = tables.OrderBy(r => r.id).ToList();
            PokerTable c = default(PokerTable);

            if (Count <= 0)
                return c;

            int index = tables.IndexOf(current);

            if (index < Count - 1)
                return tables.ElementAt(index + 1);

            return current;
        }
        /// <summary>
        /// Item Previous
        /// Search and return the previous list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the previous list item from the current item or returns the current item if there is no item in the list before him!</returns>
        public PokerTable Prev(PokerTable current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<PokerTable> tables = this.ToList();
            tables = tables.OrderBy(r => r.id).ToList();
            PokerTable c = default(PokerTable);

            if (Count <= 0)
                return c;

            int index = tables.IndexOf(current);

            if (index >= 1)
                return tables.ElementAt(index - 1);
            return current;
        }
        /// <summary>
        /// Item Relative Next:
        /// Search and return the next list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the next list item from the current item or returns the first item if there is no item in the list after him!</returns>
        public PokerTable NextRel(PokerTable current)
        {
            PokerTable c = default(PokerTable);
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<PokerTable> tables = this.ToList();
            tables = tables.OrderBy(r => r.id).ToList();

            if (Count <= 0)
                return c;

            if (Count > 1)
            {
                int index = tables.IndexOf(current);

                if (index < Count - 1)
                {
                    return tables.ElementAt(index + 1);
                }
                else
                {
                    return tables.FirstOrDefault();
                }
            }
            else
            {
                return current;
            }
        }
        /// <summary>
        /// Item Relative Previous:
        /// Search and return the prev list item from current item reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="current"> Current Item</param>
        /// <returns>returns the prev list item from the current item or returns the last item if there is no item in the list after him!</returns>
        public PokerTable PrevRel(PokerTable current)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<PokerTable> tables = this.ToList();
            tables = tables.OrderBy(r => r.id).ToList();

            PokerTable c = default(PokerTable);

            if (Count <= 0)
                return c;

            if (Count > 1)
            {
                int index = tables.IndexOf(current);

                if (index >= 1)
                {
                    return tables.ElementAt(index - 1);
                }
                else
                {
                    return tables.LastOrDefault();
                }
            }
            else
            {
                return current;
            }
        }
        /// <summary>
        /// Item Relative Next:
        /// Search and return the next list item from current item place id reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="tableID"> Current Item place id</param>
        /// <returns>returns the next list item from the current item or returns the first item if there is no item in the list after him!</returns>
        public PokerTable NextRel(int tableID)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<PokerTable> tables = this.ToList();
            tables = tables.OrderBy(r => r.id).ToList();


            PokerTable cur = default(PokerTable);

            for (int a = tableID; a < 9; a++)
            {
                int p = a + 1;
                cur = tables.Find(r => r.id == p);
                if (cur != null)
                    break;
            }

            if (cur == null)
            {
                for (int a = 0; a < 9; a++)
                {
                    int p = a + 1;
                    cur = tables.Find(r => r.id == p);
                    if (cur != null)
                        break;
                }
            }

            if (cur == null)
                return tables.FirstOrDefault();
            else
                return cur;

        }
        /// <summary>
        /// Item Relative Previous:
        /// Search and return the prev list item from current item place id reference
        /// <note type="note">
        /// The order of the players on the list is based on the place of order of each player,
        /// and the selection of the next player or former player is done in clockwise as the texas poker standards rules.
        /// </note> 
        /// </summary>
        /// <param name="tableID"> Current Item place id</param>
        /// <returns>returns the prev list item from the current item or returns the last item if there is no item in the list after him!</returns>
        public PokerTable PrevRel(int tableID)
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<PokerTable> tables = this.ToList();
            tables = tables.OrderBy(r => r.id).ToList();


            PokerTable cur = default(PokerTable);

            for (int a = tableID; a > 1; a--)
            {
                int p = a - 1;
                cur = tables.Find(r => r.id == p);
                if (cur != null)
                    break;
            }

            if (cur == null)
            {
                for (int a = 10; a > 1; a++)
                {
                    int p = a - 1;
                    cur = tables.Find(r => r.id == p);
                    if (cur != null)
                        break;
                }
            }

            if (cur == null)
                return tables.LastOrDefault();
            else
                return cur;

        }
        /// <summary>
        /// Get the first Item of the List
        /// </summary>
        /// <returns>First Item of the List or null if empty list</returns>
        public PokerTable First()
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<PokerTable> tables = this.ToList();
            tables = tables.OrderBy(r => r.id).ToList();

            PokerTable c = default(PokerTable);

            if (Count <= 0)
                return c;

            return tables.FirstOrDefault();
        }
        /// <summary>
        /// Get the last Item of the List
        /// </summary>
        /// <returns>Last item of the List or null if empty list</returns>
        public PokerTable Last()
        {
            //Organiza Lista de jogadores por ordem de lugar crescente
            List<PokerTable> tables = this.ToList();
            tables = tables.OrderBy(r => r.id).ToList();

            PokerTable c = default(PokerTable);

            if (Count <= 0)
                return c;

            return tables.LastOrDefault();
        }
        /// <summary>
        /// Add new PokerTable Item to the List.
        /// 
        /// <note type="note"> 
        /// Se <see cref="component"/> for uma PokerTable, a pokertable do jogador sera atualizada automáticamente
        /// Triggers Event: <see cref="OnAdd"/>
        /// 
        /// </note>
        /// </summary>
        /// <param name="_player">PokerTable Item to add</param>
        public new void Add(PokerTable _table)
        {

            if (maxCapacity > 0 && Count >= maxCapacity)
                return;

            if (Exists(r => r.Equals(_table)) == false)
            {
                _table.overhideID = ++autoID;
                base.Add(_table);

                PokerTableEventArgs args = new PokerTableEventArgs(_table);
                Event_OnAdd(args);
                Debug.Log("add");
                if (component != null)
                {
                    //if (component.GetType() == typeof(PokerTable))
                    //    _table. = (PokerTable)component;
                }


            }

        }
        public void Add(PokerTable _table, int id)
        {
            if (maxCapacity > 0 && Count >= maxCapacity)
            {
                Debug.LogError("Não foi possivel adicionar a TableID " + id + " á lista de pokerTables, ID Ja existente");
                return;
            }

            if (Exists(r => r.Equals(_table)) == true || Exists(r => r.id == id) == true)
            {
                Debug.LogError("Não foi possivel adicionar a TableID " + id + " á lista de pokerTables, ID Ja existente");
                return;
            }

            _table.overhideID = id;
            base.Add(_table);

            PokerTableEventArgs args = new PokerTableEventArgs(_table);
            Event_OnAdd(args);

            if (component != null)
            {
                //if (component.GetType() == typeof(PokerTable))
                //    _table. = (PokerTable)component;
            }




        }

        /// <summary>
        /// Clar all items of the tables list
        /// <note type="note"> 
        /// Triggers Event: <see cref="OnRemove"/> to eatch item.
        /// </note>
        /// </summary>
        public new void Clear()
        {
            List<PokerTable> list = new List<PokerTable>(this);
            foreach (PokerTable p in list)
            {
                Remove(p);
            }
            list.Clear();
            base.Clear();
        }
        /// <summary>
        /// Remove the current item of the list
        /// <note type="note"> 
        /// Se <see cref="component"/> for uma PokerTable, a PokerTable do jogador sera atualizada automáticamente
        /// Triggers Event: <see cref="OnRemove"/>;
        /// </note>
        /// </summary>
        /// <param name="_table"></param>
        /// <returns>true if success and false if fail</returns>
        public new bool Remove(PokerTable _table)
        {
            bool result = base.Remove(_table);
            if (result == true)
            {
                if (component != null)
                {
                    // if (component.GetType() == typeof(PokerTable))
                    //     _table.overhidePokertable = null;
                }

                PokerTableEventArgs args = new PokerTableEventArgs(_table);
                Event_OnRemove(args);
            }
            return result;
        }

    }
    /// <summary>
    /// Playing card suit symbols enumerator including:
    /// <para>  Clubs = 1, Diamonds = 2, Hearts = 3, Spades = 4,</para>   
    /// </summary>
    /// <example>
    ///<code>
    ///using UnityEngine;
    ///using System.Collections;
    ///using PokerCollections;
    ///
    ///public class SimpleCreateCard : MonoBehaviour
    ///    {
    ///
    ///        void Start()
    ///        {
    ///            //Create a new card object
    ///            Card card = new Card();
    ///            card.suit = Suit.Diamonds; //SUIT
    ///            card.cvalue = CardValue.Jack;
    ///        }
    ///
    ///
    ///    }
    /// }
    /// </code> 
    /// </example>
    public enum CardSuit
    {

        Clubs = 1,
        Diamonds = 2,
        Hearts = 3,
        Spades = 4,
    }
    /// <summary>
    /// Playing cards values enumerator: 
    /// <para> As = 1, Two = 2, Tree = 3, Four = 4, Five = 5, Six = 6, Seven = 7, Eight = 8, Nine = 9, Ten = 10, Jack = 11, Queen = 12, King = 13,</para> 
    /// </summary>
    /// <example>
    ///<code>
    ///using UnityEngine;
    ///using System.Collections;
    ///using PokerCollections;
    ///
    ///public class SimpleCreateCard : MonoBehaviour
    ///    {
    ///
    ///        void Start()
    ///        {
    ///            //Create a new card object
    ///            Card card = new Card();
    ///            card.suit = Suit.Diamonds;
    ///            card.cvalue = CardValue.Jack; //CARDVALUE
    ///        }
    ///
    ///
    ///    }
    /// }
    /// </code> 
    /// </example>
    public enum CardValue
    {

        As = 1,
        Two = 2,
        Tree = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        Jack = 11,
        Queen = 12,
        King = 13,
    }
    /// <summary>
    /// Turn of the match
    /// </summary>
    public enum BetTurn
    {
        none,
        preflop,
        flop,
        turn,
        river,
        showdown,
        NumberOfTypes
    }
    /// <summary>
    /// Used to inform the type of bet placed by a player
    /// </summary>
    public enum BetType
    {
        none = 0,
        check = 1,
        bet = 2,
        call = 3,
        rise = 4,
        fold = 5
    }
    /// <summary>
    /// It contains information about a texasHoldem playing card, such as:
    /// <para>Suit, CardValue, used sprite image and more...</para>
    /// </summary>
    /// <example>
    ///<code>
    ///using UnityEngine;
    ///using System.Collections;
    ///using PokerCollections;
    ///
    ///public class SimpleCreateCard : MonoBehaviour
    ///    {
    ///
    ///        void Start()
    ///        {
    ///            //Create a new card object
    ///            Card card = new Card();
    ///            card.suit = Suit.Diamonds;
    ///            card.cvalue = CardValue.Jack;
    ///        }
    ///
    ///
    ///    }
    /// }
    /// </code> 
    /// </example>
    public class Card : IPokerCard, ICloneable
    {

        /// <summary>
        /// Used as a unique identifier for each card created
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Used to modfy suit of the card
        /// </summary>
        public CardSuit suit { get; set; }
        /// <summary>
        /// Used to modfy value of the card
        /// </summary>
        public CardValue cvalue { get; set; }
        /// <summary>
        /// Use this if you want to assign a sprite to this card
        /// </summary>
        public Sprite sprite { get; set; }
        /// <summary>
        /// Use this if you want to assign a gameObject of your scene to this card
        /// </summary>
        public GameObject component { get; set; }

        /// <summary>
        /// Card initialization
        /// </summary>
        public Card()
        {

        }
        /// <summary>
        /// Card initialization
        /// </summary>
        /// <param name="_id">unique identifier</param>
        /// <param name="_cardvalue">value of the card</param>
        /// <param name="_suit">suit of the card</param>
        public Card(int _id, CardValue _cardvalue, CardSuit _suit)
        {
            this.id = _id;
            this.cvalue = _cardvalue;
            this.suit = _suit;
            this.sprite = null;

        }
        /// <summary>
        /// Card initialization
        /// </summary>
        /// <param name="_cardvalue">value of the card</param>
        /// <param name="_suit">suit of the card</param>
        public Card(CardValue _cardvalue, CardSuit _suit)
        {
            this.id = -1;
            this.cvalue = _cardvalue;
            this.suit = _suit;
            this.sprite = null;

        }

        /// <summary>
        /// Use for getting compllete description os the texas hand card game
        /// </summary>
        /// <returns>text with complete description card, include cardvalue and card suit</returns>
        public override string ToString()
        {
            return cvalue.ToString() + " of the " + suit.ToString();
        }
        public static bool operator ==(Card a, Card b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
            {
                return true;
            }

            if (ReferenceEquals(a, null))
            {
                return false;
            }
            if (ReferenceEquals(b, null))
            {
                return false;
            }

            return (a.id == b.id) && (a.cvalue == b.cvalue) && (a.suit == b.suit);
        }
        public static bool operator !=(Card a, Card b)
        {
            return !(a == b);
        }
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Card c = (Card)obj;

            if (c == this)
                return true;
            else if (c.id == this.id && c.cvalue == this.cvalue && c.suit == this.suit)
                return true;

            return base.Equals(obj);

        }
        public override int GetHashCode()
        {
            return id.GetHashCode() ^ cvalue.GetHashCode() ^ suit.GetHashCode();

        }
        public object Clone()
        {
            return MemberwiseClone();
        }
        public string ToEval()
        {
            return PokerEval.CardToEval(this);
        }



    }
    /// <summary>
    /// Contains 52 two cards of the common standards deck with 13 cards multiplied by 4 different suits
    /// <para>Sample:</para>    
    /// </summary>
    /// <example>
    /// <code>
    ///using UnityEngine;
    ///using System.Collections;
    ///using PokerCollections;
    ///
    ///public class SimpleCreateDeck : MonoBehaviour
    ///    {
    ///        //The deck resource folder to find sprites pack
    ///        public string spriteResourcePath;
    ///
    ///       void Start()
    ///        {
    ///            //Initialize new deck
    ///            Deck deck = new Deck();
    ///            //Shuffle deck
    ///            deck.Shuffle();
    ///            //Set sprit pack to each card of the deck
    ///            deck.SetSpriteRessources(spriteResourcePath);
    ///
    ///            Debug.Log(deck.Cards.Count); //Output: 52 
    ///         
    ///        }
    ///    }
    /// }
    /// </code> 
    /// </example>
    public class Deck
    {
        /// <summary>
        /// List of the cards from deck
        /// </summary>
        public CardsList<Card> Cards { get; private set; }
        /// <summary>
        /// the folder used to load sprite resource path 
        /// </summary>
        public string spriteResourceFolder { get; private set; }
        /// <summary>
        /// true if used sprite to deck cards
        /// </summary>
        public bool isSprited { get; private set; }
        /// <summary>
        /// Inittialize new deck
        /// </summary>
        public Deck()
        {
            Reset();
        }
        /// <summary>
        ///  Reset Deck to default Sort 52 cards
        /// </summary>  
        public bool Reset()
        {
            /*
            if (Cards != null)
                if (Cards.Count > 0)
                {
                    //Remove gameObject if exist
                    foreach (Card c in Cards)
                    {
                        if (c.component != null)
                        {
                            MonoBehaviour.Destroy(c.component);
                        }
                    }
                    Cards.Clear();
                }
                */
            if (Cards != null)
                Cards.Clear();
            Cards = Enumerable.Range(1, 4).SelectMany(
                naipe => Enumerable.Range(1, 13).Select(
                    cardValue => new Card() { suit = (CardSuit)naipe, cvalue = (CardValue)cardValue })).ToCardsList();

            //Adiciona INDEX
            int index = 0;
            Cards.ForEach(delegate (Card card) { card.id = index++; });

            if (isSprited)
            {
                SetSpriteRessources(spriteResourceFolder);
            }
            return true;
        }
        /// <summary>
        ///  Embaralha ordem das cartas do deck
        /// </summary>  
        public bool Shuffle()
        {
            Cards = Cards.ShuffleCardsList();
            return true;
        }
        /// <summary>
        ///  Ordena Deck
        /// </summary>  
        public void SortFromCardValue()
        {
            if (Cards != null)
                Cards = Cards.OrderBy(o => o.cvalue).ToCardsList();
        }
        /// <summary>
        /// Find sprite pack from ressource path
        /// <note type="note"> 
        /// Important: 
        /// Use name as the cards numbers 0-51, with 0 being the letter Ace of Clubs and 51 Kings of Spade. 
        /// The order of the night are as follows: 
        /// > Club (0-12), 
        /// > Diamond(13-25), 
        /// > Heart(26-38), 
        /// > Spade(39,51);
        /// </note>        
        /// </summary>
        /// <param name="ressource">the resource path folder to deck sprite pack(52 cards)</param>
        public void SetSpriteRessources(string ressource)
        {

            if (ressource.Length <= 0)
                return;

            foreach (Card card in Cards)
                card.sprite = Resources.Load<Sprite>(ressource + "/" + card.id.ToString());

            spriteResourceFolder = ressource;
            isSprited = true;
        }
    }
    /// <summary>
    /// Container used to receive HoldemHand Evaluation result of the players.
    /// <para>Sample:</para>     
    /// </summary>
    /// <example>
    /// <code> 
    /// EvaluatedHoldemHand handOdds = table.GetHandOddsPlayers(); 
    /// Debug.Log(handOdds.ToString());
    /// </code> 
    /// </example>
    public class EvaluatedHoldemHand
    {
        /// <summary>
        /// The total number of hands enumarated.
        /// </summary>
        public long totalHands { get; private set; }
        /// <summary>
        /// Get Winners players List
        /// </summary>
        public List<BetPlayer> winners { get; private set; }
        /// <summary>
        /// Get Tiers Players List
        /// </summary>
        public List<BetPlayer> ties { get; private set; }
        /// <summary>
        /// Get Losses Players List
        /// </summary>
        public List<BetPlayer> losses { get; private set; }

        /// <summary>
        /// Initialize HandOdds Countainer 
        /// </summary>
        /// <param name="_winners"> list with winners player</param>
        /// <param name="_ties">list with _ties player</param>
        /// <param name="_losses">list with _losses player</param>
        /// <param name="_totalhands">total number of hands enumarated</param>
        public EvaluatedHoldemHand(List<BetPlayer> _winners, List<BetPlayer> _ties, List<BetPlayer> _losses, long _totalhands)
        {
            totalHands = _totalhands;
            winners = _winners;
            ties = _ties;
            losses = _losses;
        }

        /// <summary>
        /// Used to get complete text description about evaluation result hands
        /// </summary>
        /// <returns> text description</returns>
        public override string ToString()
        {
            string tostring = "HandOdds Result: \n";

            foreach (BetPlayer p in winners)
            {
                tostring += "> winner: " + p.name + " (" + p.holdemhand.Description + ")\n";
            }
            foreach (BetPlayer p in ties)
            {
                tostring += "> tier: " + p.name + " (" + p.holdemhand.Description + ")\n";
            }
            foreach (BetPlayer p in losses)
            {
                tostring += "> loser: " + p.name + " (" + p.holdemhand.Description + ")\n";
            }

            return tostring;
        }
    }
    /// <summary>
    ///Used to manage Poker Player and your poker actions!
    /// <para>Sample:</para>    
    /// </summary>
    /// <example>
    /// <code>
    ///using UnityEngine;
    ///using System.Collections.Generic;
    ///using PokerCollections;
    ///
    /// //Create a new Deck
    ///public class CreateBetPlayerSample : MonoBehaviour
    ///    {
    ///
    ///        void Start()
    ///        {
    ///            //Create a new deck
    ///            Deck deck = new Deck();
    ///            deck.Shuffle();
    ///
    ///            //Create a new player
    ///            BetPlayer betplayer = new BetPlayer("Player1");
    ///            //Take card one to player hand
    ///            betplayer.handCards.Add(deck.TakeCard());
    ///            //Take card two to player hand
    ///            betplayer.handCards.Add(deck.TakeCard());
    ///
    ///            //Get the 5 cards to the board cards games
    ///            List &lt;card&gt; boardCards = new List&lt;card&gt;();
    /// 
    ///            for (int a = 0; a &lt; 5; a++)
    ///            {
    ///                boardCards.Add(deck.TakeCard());
    ///            }
    ///
    ///            //Prepare Hand to eval handholdem
    ///            betplayer.setBoard(boardCards);
    ///            betplayer.setHand();
    ///
    ///            //Show handholdem description
    ///            Debug.Log(betplayer.hand.Description);
    ///        }
    ///    }
    /// </code> 
    /// </example>
    public class BetPlayer : IPokerPlayer, ICloneable
    {
        public object component { get; private set; }
        public PokerTable pokerTable { get; private set; }
        /// <summary>
        /// Lista de cartas da mão do jogador
        /// </summary>
        public CardsList<Card> holeCards { get; private set; }
        /// <summary>
        /// Represents a Texas HoldemHand from uPoker Lib. Used for evaluated calc.
        /// </summary>
        public Hand holdemhand { get; private set; }
        /// <summary>
        /// Identificador único do jogador
        /// <see cref="overhideID"/>
        /// </summary>
        public int id { get; private set; }
        /// <summary>
        /// Ordem do jogador ou "lugar" em uma mesa de poker "pokerTable"
        /// </summary>
        public int placeId { get; private set; }
        /// <summary>
        /// Player Name
        /// </summary>
        public string name { get; private set; }
        /// <summary>
        /// chips amount of the player
        /// </summary>
        public double chipsAmount { get; private set; }
        /// <summary>
        /// Subistitui o ID único do jogador por outro. 
        /// <note type="note"></note>
        /// </summary>
        public int overhideID
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
        /// <summary>
        /// Modifica o atual id de ordem de jogador em uma pokerTable
        /// </summary>
        /// <param name="_id">new id orderer</param>
        public int overhidePlaceID
        {
            get { return placeId; }
            set { placeId = value; }
        }
        public PokerTable overhidePokertable
        {
            get
            {
                return pokerTable;
            }
            set {

                pokerTable = value;
                if (pokerTable != null)
                    lastPokerTable = pokerTable.id;
            }
        }
        public int lastPokerTable { get; private set; }
        /// <summary>
        /// Initialize a new Player
        /// </summary>
        /// <param name="_name">Name of the Player</param>
        public BetPlayer(string _name, object component)
        {
            name = _name;
            holeCards = new CardsList<Card>(this);
            holdemhand = new Hand();
            chipsAmount = 0;
            this.component = component;
        }
        /// <summary>
        /// Adiciona um valor ao total do jogador
        /// </summary>
        /// <param name="_value">valor a ser adicionado ao total do jogador</param>
        public void AddChip(double _value)
        {
            chipsAmount += _value;
            BetPlayerEventArgs args = new BetPlayerEventArgs(this);
            Event_OnAddChips(_value, args);
        }
        /// <summary>
        /// Retira o valor expesificado do saldo do jogador e aciona eventos relacionados.
        /// Esta ação ira retornar um valor diferente de zero, caso o jogador possua um saldo menor do que o necessário para a subtração.
        /// Neste caso o valor retornado por esta função sera o valor que faltou para ser retirado do jogador.
        /// O Valor retornado pelo evento sera o valor real descontado do jogador.
        /// </summary>
        /// <param name="_value">valor a ser retirado</param>
        /// <returns>
        /// Retorna zero se o jogador possui saldo suficiente para a subtração.
        /// Ou Retorna o valor que faltou para o jogador pagar.
        /// </returns>
        public double SubChip(double _value)
        {
            double realSub = chipsAmount - _value;

            chipsAmount -= _value;

            if (chipsAmount < 0)
                chipsAmount = 0;


            if (realSub < 0)
            {
                double result = Math.Abs(realSub);
                BetPlayerEventArgs args = new BetPlayerEventArgs(this);
                Event_OnRemoveChips(result, args);
                return result;
            }
            else
            {
                BetPlayerEventArgs args = new BetPlayerEventArgs(this);
                Event_OnRemoveChips(Math.Abs(_value), args);
                return 0;
            }
        }
        /// <summary>
        /// This will return the five best cards classified in evaluating hands
        /// </summary>
        /// <returns> returns a value, or null if there is some kind error referring to the player's cards information. 
        /// <para>
        /// Example: Returns null if the player's hand contains a smaller or larger amount than two letters, 
        /// or if the table contains a value of five cards
        /// </para>
        /// </returns>
        public List<Card> GetBestFiveCards()
        {
            List<Card> handc = PokerEval.EvalToCards(holdemhand.PocketCards);
            List<Card> boardc = PokerEval.EvalToCards(holdemhand.Board);
            List<Card> allCards = new List<Card>();
            allCards.AddRange(handc);
            allCards.AddRange(boardc);

            // PokerList<Card> cards = PokerTable.getCardFromEvalString(holdemhand.BestFiveCards());
            return allCards;
        }
        #region INTERFACES AND OVERHIDES
        public static bool operator ==(BetPlayer a, BetPlayer b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
            {
                return true;
            }

            if (ReferenceEquals(a, null))
            {
                return false;
            }
            if (ReferenceEquals(b, null))
            {
                return false;
            }



            if (a.id > 0 || b.id > 0)
            {
                return a.id == b.id;
            }
            else
            {
                return (a.id == b.id) && (a.name == b.name);
            }
        }
        public static bool operator !=(BetPlayer a, BetPlayer b)
        {
            return !(a == b);
        }
        /// <summary>
        /// Compara um objeto origem do tipo BetPlayer com outro objeto e retorna verdadeiro ou falso 
        /// <note type="note"> Não é possivel a comparação entre um Objeto BetPlayer com um objeto MatchPlayer apartir deste comparador.
        /// Para esta necessidade utilize a comparação apartir do objeto MatchPlayer.
        /// <see cref="MatchGame.MatchPlayer"/>  Exemplo:
        /// <code> MatchPlayer mp.Equals(betplayer)</code>
        /// </note>
        /// </summary>
        /// <param name="obj"> objeto BetPlayer Destino</param>
        /// <returns>Verdadeiro ou Falso</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            BetPlayer p = (BetPlayer)obj;
            return this == p;

        }
        public override int GetHashCode()
        {
            return id.GetHashCode() ^ placeId.GetHashCode() ^ name.GetHashCode();

        }
        public object Clone()
        {
            return MemberwiseClone();
        }
        #endregion
        #region EVAL ENGUINE

        /// <summary>
        /// If there are cards in the player's hand, a TexasHolde'm description of the hand is returned.
        /// </summary>
        /// <returns>TexasHolde'm description of the hand</returns>
        public string getHandDescription()
        {
            try
            {
                if (holeCards.Count > 0)
                {
                    updateHand();
                    return holdemhand.Description;
                }
                else
                {
                    return "empty hand";
                }
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return "empty hand";
            }
        }
        /// <summary>
        /// Having cards in the player's hand, will be returned this TexasHold'm hand classification 
        /// </summary>
        /// <returns>TexasHolde'm type of the hand</returns>
        public Hand.HandTypes getHandType()
        {

            updateHand();
            return holdemhand.HandTypeValue;

        }
        /// <summary>
        /// Used to receive the hand of the player's cards validation format to be used by the validation engine
        /// </summary>
        /// <returns>string Hand.Poketcards format</returns>
        public string getCardsEval()
        {
            string cards = PokerEval.CardsToEval(this.holeCards);
            return cards;
        }
        /// <summary>
        /// Use for geting the complete description of the hand cards from player
        /// </summary>
        /// <returns>Complete description of the hand cards from player</returns>
        public string PrintCards()
        {
            string result;
            if (holeCards.Count == 2)
            {
                result = holeCards[0].ToString() + " AND " + holeCards[1].ToString();
            }
            else if (holeCards.Count == 1)
            {
                result = holeCards[0].ToString();
            }
            else
            {
                result = "empty hand";
            }

            return result;
        }
        /// <summary>
        /// Update board cards from HAND for evaluated calc.
        /// <para> Before requesting a validation hands of the players, it is important that this function is called before hand to update the individual validation class player</para>
        /// <para> <b>Use To (setHand method) for update hand cards of the player</b></para>
        /// <seealso cref="updateHand"/>
        /// </summary>
        /// <param name="cards">string cards reference of the board, sample: (ac ks qs 2d 7c)</param>
        public void setBoard(string cards)
        {
            holdemhand.Board = cards;
            updateHand();
        }
        /// <summary>
        /// Used to receive the board cards to the player's cards validation format to be used by the validation engine
        /// </summary>
        /// <param name="cards"> List card to the eval format</param>
        /// <returns>string HandHoldem board format</returns>
        public void setBoard(List<Card> cards)
        {
            string eval = PokerEval.CardsToEval(cards);
            setBoard(eval);
            updateHand();

        }
        /// <summary>
        /// Update hand cards from HAND for evaluated calc.
        /// </summary>
        /// <para> Before requesting a validation hands of the players, it is important that this function is called before hand to update the individual validation class player</para>
        /// <para> <b>Use To (setBoard method) for update hand cards of the player</b></para>
        /// <seealso cref="updateHand"/>
        public void updateHand()
        {
            string poketcards = getCardsEval();
            holdemhand.PocketCards = poketcards;
        }
        #endregion
        #region EVENTS CALLBACK
        protected virtual void Event_OnAddChips(double value, BetPlayerEventArgs e)
        {
            EventHandler_Chips<BetPlayerEventArgs> handler = OnAddChips;
            if (handler != null)
            {
                handler(0, e);
            }
        }
        protected virtual void Event_OnRemoveChips(double value, BetPlayerEventArgs e)
        {
            EventHandler_Chips<BetPlayerEventArgs> handler = OnRemoveChips;
            if (handler != null)
            {
                handler(0, e);
            }
        }

        public event EventHandler_Chips<BetPlayerEventArgs> OnAddChips;
        public event EventHandler_Chips<BetPlayerEventArgs> OnRemoveChips;

        #endregion

    }
    /// <summary>
    /// Use this class to extend the functions of your component sena and attach events of class to your component.
    /// </summary>
    public class PokerTable_MonoBehavior : MonoBehaviour
    {
        /// <summary>
        /// Remove this list from <see cref="PokerTables"/>, when the component was destroyed!
        /// </summary>
        public virtual void OnDestroy()
        {
            PokerTables.Tables.Remove(PokerTables.Tables.Find(r => r.component.Equals(this)));
        }
    }
    /// <summary>
    /// Used to manage multiple poker matches.
    /// <note type="important">
    /// It is recommended to expand the class PokerTable components using: <see cref="PokerTable_MonoBehavior"/>, instead <see cref="MonoBehaviour"/>.
    /// </note>
    /// <note type="note">
    /// When booted from an attached component: <see cref="PokerTable(MonoBehaviour component)"/>, 
    /// the pokerTable created, will be added automatically to the table list in <see cref="PokerTables"/>!
    /// </note>
    /// Use <see cref="PokerTables.Tables()"/> to see all pokerTables active in game.
    /// </summary>
    /// <example>
    /// <code>
    /// using UnityEngine;
    /// using System.Collections;
    /// using PokerCollections;
    ///
    /// /*
    /// Using the Poker Collections library is very easy to create and validate poker texas hold hand. 
    /// - Support for multiple matches games simultaneous.
    /// - Easy Player create and mananger
    /// - Easy PokerTable create and Mananger
    /// - Easy Deck Card mananger
    /// - Fast and Very Very Very easy evalue hands
    /// */
    /// public class EasyEvalue : MonoBehaviour
    ///    {
    ///        void Start()
    ///        {
    ///            //We create a PokerTable for the game 
    ///            //and choose to automatically create a deck and shuffle to.  
    ///            PokerTable table = new PokerTable(true);
    ///
    ///            //Creating Player for gamming
    ///            table.CreatBetPlayer("player1");
    ///            table.CreatBetPlayer("player2");
    ///
    ///            /*============================================================
    ///             -With my already shuffled deck, you can simply receive letters directly from the order, 
    ///             because the cards are already in random order.
    ///             ===============================================================*/
    ///
    ///            //TAKE PLAYER HAND CARDS FROM DECK
    ///            //Giving the two player cards automatically and at cyclo
    ///            table.TakeCardsToBetPlayers();
    ///
    ///            //TAKE BOARD CARDS FROM DECK
    ///            //Giving the tree Flop cards board automatically.
    ///            table.GetFlopCards();
    ///            //Giving the one Turn card board automatically.
    ///            table.GetTurnCard();
    ///            //Giving the one River card board automatically.
    ///            table.GetRiverCard();
    ///
    ///            //Print all players hand description
    ///            Debug.Log(table.PrintPlayersHand());
    ///
    ///            //Validate and get list with winners, ties and lossers players.
    ///            EvaluatedHoldemHand handOdds = table.GetEvaluatedHandOdds();
    ///
    ///            if (handOdds != null)
    ///                Debug.Log(handOdds.ToString());
    ///
    ///            //Clear player cards and table cards
    ///            //set deckcards to default and shuffled
    ///            table.ResetTable(true);
    ///        }
    ///    }
    ///
    /// </code>
    /// </example>
    public class PokerTable : IPokerTable
    {
        /// <summary>
        /// Unic ID of the pokertable
        /// <note type="note">This id will be generated automatically whenever the pokerTable starts by method
        /// of the component <see cref="PokerTable(MonoBehaviour component)"/>.
        /// </note>
        /// <note type="note">
        /// Utilize <see cref="PokerTable(MonoBehaviour component, int ID)"/> para gerar um ID manual para a pokertable
        /// que sera adicionada á lista de <see cref="PokerTables"/>!.
        /// </note>
        /// </summary>
        public int id { get; private set; }
        /// <summary>
        /// Allows manual modification of the pokertable <see cref="id"/> 
        /// </summary>
        public int overhideID
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
        /// <summary>
        /// Parent component attached.
        /// See also: <see cref="PokerTable(MonoBehaviour component)"/> 
        /// and <see cref="PokerTable(MonoBehaviour component, int ID)"/>
        /// </summary>
        public MonoBehaviour component;

        /// <summary>
        /// Players list "Sitting at the table"
        /// </summary>
        public PlayerList<BetPlayer> betplayers
        {
            get; private set;
        }
        /// <summary>
        /// HopPlayer. Player waiting for the next round, see also <see cref="MergeHoplayer"/> Method and <see cref="CreateHopleyer(BetPlayer)"/>
        /// </summary>
        public PlayerList<BetPlayer> hoplayers { get; private set; }
        /// <summary>
        /// Get board cards in this PokerTable
        /// </summary>
        public CardsList<Card> boardCards { get; private set; }
        /// <summary>
        /// Get deck cards in this Pokertable
        /// </summary>
        public Deck deck { get; private set; }

        #region INITTIALIZATION METHODS
        /// <summary>
        /// Create a new PokerTable for game witout deck
        /// </summary>
        /// <param name="_deck"></param>
        public PokerTable()
        {

            betplayers = new PlayerList<BetPlayer>(this);
            hoplayers = new PlayerList<BetPlayer>(this);
            boardCards = new CardsList<Card>(this);
            deck = new Deck();

        }
        /// <summary>
        /// Create a new PokerTable for game.
        /// </summary>
        /// <param name="_deck">A Before deck created  to add in table</param>
        public PokerTable(Deck _deck)
        {
            betplayers = new PlayerList<BetPlayer>(this);
            boardCards = new CardsList<Card>(this);
            hoplayers = new PlayerList<BetPlayer>(this);
            deck = _deck;
        }
        /// <summary>
        /// Create a new PokerTable for game and a shuffled deck automatically!
        /// </summary>
        /// <param name="DeckSpriteResourceFolder">Folder Path in resouces folder, for sprite cards search</param>
        /// <param name="shuffle">If true, shuffle deck</param>
        public PokerTable(string DeckSpriteResourceFolder, bool shuffle)
        {
            betplayers = new PlayerList<BetPlayer>(this);
            boardCards = new CardsList<Card>(this);
            hoplayers = new PlayerList<BetPlayer>(this);
            deck = new Deck();

            if (DeckSpriteResourceFolder.Length > 0)
                deck.SetSpriteRessources(DeckSpriteResourceFolder);
            if (shuffle)
                deck.Shuffle();
        }
        /// <summary>
        /// Create a new PokerTable for game and a shuffled deck automatically!
        /// </summary>
        /// <param name="shuffle">If true, shuffle deck</param>
        public PokerTable(bool shuffle)
        {
            betplayers = new PlayerList<BetPlayer>(this);
            boardCards = new CardsList<Card>(this);
            hoplayers = new PlayerList<BetPlayer>(this);
            deck = new Deck();

            if (shuffle)
                deck.Shuffle();
        }
        /// <summary>
        /// <b>USO RECOMENDADO:</b>
        /// Ao usar este metodo de inicialização, sua pokertable sera anexada ao GameObject origem.
        /// <para>
        /// Esta pokertable também sera listada automaticamente e você podera pesquisar todas as pokerTables
        /// existentes no momento, se desejar. Veja: <see cref="PokerTables.Tables"/>!
        /// </para>
        /// <note type="note"> Se o gameObject pai desta PokerTable for destruido em runtime esta pokerTable sera
        /// removida da lista de pokertables automaticamente.
        /// Utilize os eventos referentes para capturar os disparos de adesao e remocao de pokertables na lista para
        /// receber este eventos
        /// </note>
        /// </summary>
        /// <param name="component">Componente dono deste pokerTable</param>
        public PokerTable(MonoBehaviour component)
        {
            betplayers = new PlayerList<BetPlayer>(this);
            boardCards = new CardsList<Card>(this);
            hoplayers = new PlayerList<BetPlayer>(this);
            deck = new Deck();
            this.component = component;
            PokerTables.Tables.Add(this);
        }
        /// <summary>
        /// Ao usar este metodo de inicialização, sua pokertable sera anexada ao GameObject origem porém o ID da
        /// pokertable não sera gerado automáticamente. 
        /// <para>
        /// Esta pokertable também sera listada automaticamente e você podera pesquisar todas as pokerTables
        /// existentes no momento, se desejar. Veja: <see cref="PokerTables.Tables"/>!
        /// </para>
        /// <note type="note"> Se o gameObject pai desta PokerTable for destruido em runtime esta pokerTable sera
        /// removida da lista de pokertables automaticamente.
        /// Utilize os eventos referentes para capturar os disparos de adesao e remocao de pokertables na lista para
        /// receber este eventos
        /// </note>
        /// </summary>
        /// <param name="component">Componente dono deste pokerTable</param>
        /// /// <param name="ID">ID Manual</param>
        public PokerTable(MonoBehaviour component, int ID)
        {
            betplayers = new PlayerList<BetPlayer>(this);
            boardCards = new CardsList<Card>(this);
            hoplayers = new PlayerList<BetPlayer>(this);
            deck = new Deck();
            this.component = component;
            id = ID;
            PokerTables.Tables.Add(this, ID);
        }
        #endregion

        /// <summary>
        /// Create and Add automatically a new player in this PokerTable
        /// </summary>
        /// <param name="name">Player Name</param>
        /// <returns>new BetPlayer created</returns>
        public BetPlayer CreateBetPlayer(string name)
        {
            BetPlayer bp = new BetPlayer(name, null);
            betplayers.Add(bp);
            return bp;
        }
        /// <summary>
        /// Create and Add automatically a new betplayer with manual ID in this PokerTable
        /// </summary>
        /// <param name="name">Player Name</param>
        /// <param name="id">Player ID</param>
        /// <returns>new BetPlayer created</returns>
        public BetPlayer CreateBetPlayer(string name, int id)
        {
            BetPlayer bp = new BetPlayer(name, null);
            bp.overhideID = id;
            betplayers.Add(bp);
            return bp;
        }
        /// <summary>
        /// Create and Add automatically a new player in this PokerTable with stidown place ID
        /// </summary>
        /// <param name="name"> Player Name</param>
        /// <param name="placeID"> Player SitDown place ID</param>
        /// <returns>new BetPlayer created</returns>
        public BetPlayer CreateBetPlayer(string name, int id, int placeID)
        {
            BetPlayer bp = new BetPlayer(name, null);
            bp.overhidePlaceID = placeID;
            bp.overhideID = id;
            betplayers.Add(bp);
            return bp;
        }
        /// <summary>
        /// Create and Add automatically a new player in this PokerTable with stidown place ID and chips amount
        /// </summary>
        /// <param name="name"> Player Name</param>
        /// <param name="placeID"> Player SitDown place ID</param>
        /// <param name="_chipAmount"> Player chips amount</param>
        /// <returns>new BetPlayer created</returns>
        public BetPlayer CreateBetPlayer(string name, int id, int placeID, double _chipAmount)
        {
            BetPlayer bp = new BetPlayer(name, null);
            bp.overhidePokertable = this;
            bp.AddChip(_chipAmount);
            bp.overhidePlaceID = placeID;
            bp.overhideID = id;
            betplayers.Add(bp);
            return bp;
        }
        /// <summary>
        /// Add a created betPlayer to betplayers list
        /// </summary>
        /// <param name="betplayer">Created Betplayer</param>
        public void CreateBetPlayer(BetPlayer betplayer)
        {
            betplayers.Add(betplayer);
        }
        /// <summary>
        /// Create and Add automatically a new hoplayer in this PokerTable
        /// </summary>
        /// <param name="name">Player Name</param>
        /// <returns>new BetPlayer created</returns>
        public BetPlayer CreateHopleyer(string name)
        {
            BetPlayer bp = new BetPlayer(name, null);
            hoplayers.Add(bp);
            return bp;
        }
        /// <summary>
        /// Create and Add automatically a new hoplayer with manual ID in this PokerTable
        /// </summary>
        /// <param name="name">Player Name</param>
        /// <param name="id">Player ID</param>
        /// <returns>new BetPlayer created</returns>
        public BetPlayer CreateHopleyer(string name, int id)
        {
            BetPlayer bp = new BetPlayer(name, null);
            bp.overhideID = id;
            hoplayers.Add(bp);
            return bp;
        }
        /// <summary>
        /// Create and Add automatically a new hoplayer in this PokerTable with stidown place ID
        /// </summary>
        /// <param name="name"> Player Name</param>
        /// <param name="placeID"> Player SitDown place ID</param>
        /// <returns>new BetPlayer created</returns>
        public BetPlayer CreateHopleyer(string name, int id, int placeID)
        {
            BetPlayer bp = new BetPlayer(name, null);
            bp.overhidePlaceID = placeID;
            bp.overhideID = id;
            hoplayers.Add(bp);
            return bp;
        }
        /// <summary>
        /// Create and Add automatically a new hoplayer in this PokerTable with stidown place ID and chips amount
        /// </summary>
        /// <param name="name"> Player Name</param>
        /// <param name="placeID"> Player SitDown place ID</param>
        /// <param name="_chipAmount"> Player chips amount</param>
        /// <returns>new BetPlayer created</returns>
        public BetPlayer CreateHopleyer(string name, int id, int placeID, double _chipAmount)
        {
            BetPlayer bp = new BetPlayer(name, null);
            bp.overhidePokertable = this;
            bp.AddChip(_chipAmount);
            bp.overhidePlaceID = placeID;
            bp.overhideID = id;
            hoplayers.Add(bp);
            return bp;
        }
        /// <summary>
        /// Add a created betPlayer to hoplayers list
        /// </summary>
        /// <param name="betplayer">Created Betplayer</param>
        public void CreateHopleyer(BetPlayer betplayer)
        {
            hoplayers.Add(betplayer);
        }
        /// <summary>
        /// Merge all Hoplayer in BetPlayers list and remove all hoplayer from hoplayers list
        /// </summary>
        public void MergeHoplayer()
        {
            foreach(BetPlayer h in new List<BetPlayer>(hoplayers))
            {
                if (betplayers.Exists(r => r.placeId == h.placeId) == false)
                {
                    hoplayers.Remove(h);
                    betplayers.Add(h);
                }
            }
           
        }
        /// <summary>
        /// Get text with all players hand description
        /// </summary>
        /// <returns> all players hand description</returns>
        public string PrintPlayersHandCards()
        {
            string result = "";
            foreach (BetPlayer player in betplayers)
            {
                result += (player.name + " have: " + player.PrintCards() + "\n");
            }

            return result;
        }
        public string PrintBoardCards()
        {
            string result = "";
            foreach (Card c in boardCards)
            {
                result += c.ToString() + ", ";
            }
            return result;
        }
        public void overHide_ID(int id)
        {
            this.id = id;
        }
        #region EVAL ENGUINE
        /// <summary>
        /// Get board cards in string format from id
        /// </summary>
        /// <param name="result"> out string to geting values</param>
        /// <returns>alwes true</returns>
        public bool getTableCardsIds(out string result)
        {
            string board = PokerEval.CardsToEvalID(boardCards.ToList());
            result = board;
            return true;
        }
        /// <summary>
        ///  Get board in Eval format sample: (as kd 2c ...)
        /// </summary>
        /// <param name="result">out string to geting values</param>
        /// <returns>alwes true</returns>
        public bool getTableCardsEval(out string result)
        {
            string board = PokerEval.CardsToEval(boardCards.ToList());
            result = board;
            return true;

        }
        /// <summary>
        /// Get board in Eval format sample: (as kd 2c ...)
        /// <summary>
        /// <returns>string with card values</returns>
        public string getTableCardsEval()
        {
            string board = PokerEval.CardsToEval(boardCards.ToList());

            return board;

        }
        /// <summary>
        /// From a text containing the card values you will receive a list of already classified cards method
        /// </summary>
        /// <param name="cards"> cards values in eval text formmat. Sampe: (as kd 2c ...)</param>
        /// <returns>List of the Card</returns>
        public static List<Card> getCardFromEvalString(string cards)
        {
            List<Card> _cards = PokerEval.EvalToCards(cards);
            return _cards;
        }
        /// <summary>
        /// From a text containing the card values you will receive a list of already classified cards method
        /// </summary>
        /// <param name="cards"> cards values in eval text formmat. Sampe: (as kd 2c ...)</param>
        /// <param name="result"> ou LIST Card for getting cards</card></param>
        /// <returns>alwes true</returns>
        public static bool getCardFromEvalString(string cards, out List<Card> result)
        {
            List<Card> _cards = PokerEval.EvalToCards(cards);
            result = _cards;
            return true;
        }

        #endregion

    }
    /// <summary>
    /// Utilities to work with conventions of the uPoker.
    /// <note type="note">
    /// Normally used to make CARD conversions in several other formats, that are readable specifically for each library layer that you are using.
    /// > uPoker will work with CARD sequences in string and numeric format set for evaluation of the analysis and probability.
    /// > Poker Collections will work with cards list from the class <see cref="Card"/>. 
    /// This will make the PokerCard readable at a high level and can be easily identified by lay users. 
    /// This is also important to optimize the work in the implementation of high-level methods to the library!
    /// </note>
    /// </summary>
    public static class PokerEval
    {
        /// <summary>
        /// Convert a List of the HighLevel Card to the HoldemHand string cards formats, readable on uPoker.<see cref="Card"/>
        /// </summary>
        /// <param name="_cards">List of Cards</param>
        /// <returns>HoldemHand string cards format, readable on uPoker</returns>
        public static string CardsToEval(List<Card> _cards)
        {

            string cards = "";
            foreach (Card card in _cards)
            {
                string value = "";
                string suit = "";

                if (card.cvalue == CardValue.As) { value = "a"; }
                if (card.cvalue == CardValue.Two) { value = "2"; }
                if (card.cvalue == CardValue.Tree) { value = "3"; }
                if (card.cvalue == CardValue.Four) { value = "4"; }
                if (card.cvalue == CardValue.Five) { value = "5"; }
                if (card.cvalue == CardValue.Six) { value = "6"; }
                if (card.cvalue == CardValue.Seven) { value = "7"; }
                if (card.cvalue == CardValue.Eight) { value = "8"; }
                if (card.cvalue == CardValue.Nine) { value = "9"; }
                if (card.cvalue == CardValue.Ten) { value = "t"; }
                if (card.cvalue == CardValue.Jack) { value = "j"; }
                if (card.cvalue == CardValue.Queen) { value = "q"; }
                if (card.cvalue == CardValue.King) { value = "k"; }

                if (card.suit == CardSuit.Hearts) { suit = "h"; }
                if (card.suit == CardSuit.Diamonds) { suit = "d"; }
                if (card.suit == CardSuit.Spades) { suit = "s"; }
                if (card.suit == CardSuit.Clubs) { suit = "c"; }

                if (card == _cards.Last())
                {
                    cards += value + suit;
                }
                else
                {
                    cards += value + suit + " ";
                }
            }
            return cards;

        }
        /// <summary>
        /// Convert Card list to sequence of ID, in text format card.
        /// </summary>
        /// <param name="_cards">Lista origem contendo cartas</param>
        /// <returns>Squência de identificadores de cartas em modo texto</returns>
        public static string CardsToEvalID(List<Card> _cards)
        {
            string cards = "";
            foreach (Card card in new List<Card>(_cards))
            {
                cards += card.id + ",";
            }

            return cards;
        }
        /// <summary>
        /// Convert <see cref="Card"/> to HoldemHand string card format, readable on uPoker
        /// </summary>
        /// <param name="_cards">Carta Origem</param>
        /// <returns>Carta em formato texto</returns>
        public static string CardToEval(Card _card)
        {

            string card = "";

            string value = "";
            string suit = "";

            if (_card.cvalue == CardValue.As) { value = "a"; }
            if (_card.cvalue == CardValue.Two) { value = "2"; }
            if (_card.cvalue == CardValue.Tree) { value = "3"; }
            if (_card.cvalue == CardValue.Four) { value = "4"; }
            if (_card.cvalue == CardValue.Five) { value = "5"; }
            if (_card.cvalue == CardValue.Six) { value = "6"; }
            if (_card.cvalue == CardValue.Seven) { value = "7"; }
            if (_card.cvalue == CardValue.Eight) { value = "8"; }
            if (_card.cvalue == CardValue.Nine) { value = "9"; }
            if (_card.cvalue == CardValue.Ten) { value = "t"; }
            if (_card.cvalue == CardValue.Jack) { value = "j"; }
            if (_card.cvalue == CardValue.Queen) { value = "q"; }
            if (_card.cvalue == CardValue.King) { value = "k"; }
            if (_card.suit == CardSuit.Hearts) { suit = "h"; }
            if (_card.suit == CardSuit.Diamonds) { suit = "d"; }
            if (_card.suit == CardSuit.Spades) { suit = "s"; }
            if (_card.suit == CardSuit.Clubs) { suit = "c"; }


            card = value + suit;
            return card;
        }
        /// <summary>
        /// Convert HoldemHand cards string to the generic Card List <see cref="Card"/>
        /// </summary>
        /// <param name="cards">HoldemHam string cards</param>
        /// <returns>Generic List of the <see cref="Card"/></returns>
        public static List<Card> EvalToCards(string cards)
        {
            cards = cards.ToLower();

            string[] cs = cards.Split(" "[0]);

            Deck deck = new Deck();
            deck.Reset();

            List<Card> _cards = new List<Card>();

            foreach (string c in cs)
            {
                switch (c)
                {
                    case "ac":
                        _cards.Add(deck.Cards.TakeItem(0));
                        break;
                    case "2c":
                        _cards.Add(deck.Cards.TakeItem(1));
                        break;
                    case "3c":
                        _cards.Add(deck.Cards.TakeItem(2));
                        break;
                    case "4c":
                        _cards.Add(deck.Cards.TakeItem(3));
                        break;
                    case "5c":
                        _cards.Add(deck.Cards.TakeItem(4));
                        break;
                    case "6c":
                        _cards.Add(deck.Cards.TakeItem(5));
                        break;
                    case "7c":
                        _cards.Add(deck.Cards.TakeItem(6));
                        break;
                    case "8c":
                        _cards.Add(deck.Cards.TakeItem(7));
                        break;
                    case "9c":
                        _cards.Add(deck.Cards.TakeItem(8));
                        break;
                    case "tc":
                        _cards.Add(deck.Cards.TakeItem(9));
                        break;
                    case "jc":
                        _cards.Add(deck.Cards.TakeItem(10));
                        break;
                    case "qc":
                        _cards.Add(deck.Cards.TakeItem(11));
                        break;
                    case "kc":
                        _cards.Add(deck.Cards.TakeItem(12));
                        break;

                    case "ad":
                        _cards.Add(deck.Cards.TakeItem(13));
                        break;
                    case "2d":
                        _cards.Add(deck.Cards.TakeItem(14));
                        break;
                    case "3d":
                        _cards.Add(deck.Cards.TakeItem(15));
                        break;
                    case "4d":
                        _cards.Add(deck.Cards.TakeItem(16));
                        break;
                    case "5d":
                        _cards.Add(deck.Cards.TakeItem(17));
                        break;
                    case "6d":
                        _cards.Add(deck.Cards.TakeItem(18));
                        break;
                    case "7d":
                        _cards.Add(deck.Cards.TakeItem(18));
                        break;
                    case "8d":
                        _cards.Add(deck.Cards.TakeItem(20));
                        break;
                    case "9d":
                        _cards.Add(deck.Cards.TakeItem(21));
                        break;
                    case "td":
                        _cards.Add(deck.Cards.TakeItem(22));
                        break;
                    case "jd":
                        _cards.Add(deck.Cards.TakeItem(23));
                        break;
                    case "qd":
                        _cards.Add(deck.Cards.TakeItem(24));
                        break;
                    case "kd":
                        _cards.Add(deck.Cards.TakeItem(25));
                        break;

                    case "ah":
                        _cards.Add(deck.Cards.TakeItem(26));
                        break;
                    case "2h":
                        _cards.Add(deck.Cards.TakeItem(27));
                        break;
                    case "3h":
                        _cards.Add(deck.Cards.TakeItem(28));
                        break;
                    case "4h":
                        _cards.Add(deck.Cards.TakeItem(29));
                        break;
                    case "5h":
                        _cards.Add(deck.Cards.TakeItem(30));
                        break;
                    case "6h":
                        _cards.Add(deck.Cards.TakeItem(31));
                        break;
                    case "7h":
                        _cards.Add(deck.Cards.TakeItem(32));
                        break;
                    case "8h":
                        _cards.Add(deck.Cards.TakeItem(33));
                        break;
                    case "9h":
                        _cards.Add(deck.Cards.TakeItem(34));
                        break;
                    case "th":
                        _cards.Add(deck.Cards.TakeItem(35));
                        break;
                    case "jh":
                        _cards.Add(deck.Cards.TakeItem(36));
                        break;
                    case "qh":
                        _cards.Add(deck.Cards.TakeItem(37));
                        break;
                    case "kh":
                        _cards.Add(deck.Cards.TakeItem(38));
                        break;

                    case "as":
                        _cards.Add(deck.Cards.TakeItem(39));
                        break;
                    case "2s":
                        _cards.Add(deck.Cards.TakeItem(40));
                        break;
                    case "3s":
                        _cards.Add(deck.Cards.TakeItem(41));
                        break;
                    case "4s":
                        _cards.Add(deck.Cards.TakeItem(42));
                        break;
                    case "5s":
                        _cards.Add(deck.Cards.TakeItem(43));
                        break;
                    case "6s":
                        _cards.Add(deck.Cards.TakeItem(44));
                        break;
                    case "7s":
                        _cards.Add(deck.Cards.TakeItem(45));
                        break;
                    case "8s":
                        _cards.Add(deck.Cards.TakeItem(46));
                        break;
                    case "9s":
                        _cards.Add(deck.Cards.TakeItem(47));
                        break;
                    case "ts":
                        _cards.Add(deck.Cards.TakeItem(48));
                        break;
                    case "js":
                        _cards.Add(deck.Cards.TakeItem(49));
                        break;
                    case "qs":
                        _cards.Add(deck.Cards.TakeItem(50));
                        break;
                    case "ks":
                        _cards.Add(deck.Cards.TakeItem(51));
                        break;
                }
            }
            return _cards;
        }
        /// <summary>
        /// Convert HoldemHand string cards format to ulong. 
        /// Usually used to work with the library <see cref="uPoker.Hand"/> and <see cref="uPoker.PocketHands"/>,
        /// in order to customize their own methods of validation and poker hand analysis!
        /// </summary>
        /// <param name="cards">HoldemHand sequence cards string</param>
        /// <returns>
        /// uLong masck cards!
        /// </returns>
        public static ulong EvalToMask(string cards)
        {
            ulong result = Hand.ParseHand(cards);
            return result;

        }
        /// <summary>
        /// Convert the uLong masck cards to HoldemHand sequence cards string
        /// </summary>
        /// <param name="mask">uLong mask cards</param>
        /// <returns>HoldemHand sequence cards string</returns>
        public static string MaskToEval(ulong mask)
        {
            string cards = Hand.MaskToString(mask);
            return cards;
        }
        /// <summary>
        /// uLong masck cards to Generic List of the <see cref="Card"/>
        /// </summary>
        /// <param name="mask">uLong mask cards value</param>
        /// <returns>Generic list of the Cards</returns>
        public static List<Card> MaskToCards(ulong mask)
        {
            string cards = Hand.MaskToString(mask);
            List<Card> result = EvalToCards(cards);
            return result;
        }
        /// <summary>
        /// Return a textual description of the TexasHold'em sequences card
        /// </summary>
        /// <param name="playerCards">HoldemHand string cards of the player hand</param>
        /// <param name="boardCards">HoldemHand string cards of the board</param>
        /// <returns></returns>
        public static string GetHandDescription(string playerCards, string boardCards)
        {
            Hand hand = new uPoker.Hand(playerCards, boardCards);
            return hand.Description;
        }

    }
    /// <summary>
    /// All activity pokerTables in game
    /// </summary>
    public static class PokerTables
    {
        private static TableList<PokerTable> _tables;
        /// <summary>
        /// List of the pokerTables
        /// </summary>
        public static TableList<PokerTable> Tables
        {
            get
            {
                if (_tables == null)
                    _tables = new TableList<PokerTable>();

                return _tables;
            }
        }
        /// <summary>
        /// Find pokerTable by ID
        /// </summary>
        /// <param name="id">Identificador único a ser pesquisado</param>
        public static PokerTable Get_PokerTable(int id)
        {
            PokerTable table = _tables.Find(r => r.id == id);
            return table;
        }


    }
}

