﻿using System.Collections.Generic;
using System.Collections;
using System;

namespace PokerCollections.EventHandler
{
    using Interfaces;

    public delegate void EventHandler_Player<TEventArgs>(TEventArgs e) where TEventArgs : EventArgs_BetPlayer;
    public delegate void EventHandler_Chips<TEventArgs>(double value, TEventArgs e) where TEventArgs : EventArgs_BetPlayer;
    public delegate void EventHandler_Table<TEventArgs>(TEventArgs e) where TEventArgs : EventArgs_PokerTable;
    public delegate void EventHandler_Matches<TEventArgs>(TEventArgs e) where TEventArgs : EventArgs_Match;

    public class EventArgs_Match
    {
        public static readonly EventArgs_Match Empty;

        public EventArgs_Match()
        {

        }
    }
    public class EventArgs_BetPlayer
    {
        public static readonly EventArgs_BetPlayer Empty;

        public EventArgs_BetPlayer()
        {

        }
    }
    public class EventArgs_PokerTable
    {
        public static readonly EventArgs_PokerTable Empty;

        public EventArgs_PokerTable()
        {

        }
    }
    
    public class MatchEventArgs : EventArgs_Match
    {
        public IPokerMatch IMatch { get; private set; }
        public MatchEventArgs(IPokerMatch _IMatch)
        {
            IMatch = _IMatch;
        }
    }
    public class MatchActionEventArgs : EventArgs_Match
    {
        public IPokerMatch IMatch { get; private set; }
        public IPokerMatchAction IMatchAction { get; private set; }
        public MatchActionEventArgs(IPokerMatch _IMatch, IPokerMatchAction _Iaction)
        {
            IMatch = _IMatch;
            IMatchAction = _Iaction;
        }
    }
    public class PokerTableEventArgs : EventArgs_PokerTable
    {
        public IPokerTable Ipokertable { get; private set; }
        public PokerTableEventArgs(IPokerTable _Ipokertable)
        {
            Ipokertable = _Ipokertable;
        }
    }
    public class BetPlayerEventArgs : EventArgs_BetPlayer
    {
        public IPokerPlayer IbetPlayer { get; private set; }
        public BetPlayerEventArgs(IPokerPlayer _IbetPlayer)
        {
            IbetPlayer = _IbetPlayer;
        }
    }
    public class CardEventArgs : EventArgs
    {
        public IPokerCard ICard { get; private set; }
        public CardEventArgs(IPokerCard icard)
        {
            ICard = icard;
        }
    }
    
}