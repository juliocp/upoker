﻿using System.Collections.Generic;
using System.Collections;

namespace PokerCollections.Interfaces
{
    public interface IPokerCard
    {
        int id { get; }
        CardSuit suit { get; }
        CardValue cvalue { get; }
    }
    public interface IPokerPlayer
    {
        int id { get; }
        int placeId { get; }
        string name { get; }
        int overhideID { get; set; }
        int overhidePlaceID { get; set; }
        /// <summary>
        /// Last pokertable visited by player.
        /// </summary>
        int lastPokerTable { get; }
        PokerTable overhidePokertable { get; set; }
        double chipsAmount { get; }
    }
    public interface IPokerTable
    {
        int id { get; }
        int overhideID { get; set; }

    }
    public interface IPokerMatch
    {
        int id { get; }
        int overhide_id { get; set; }
        int playersCount { get; }
        int hoplayersCount { get; }
        bool isActive { get; }
        void SetActive(bool status);
        PokerTable pokertable { get; }
        UnityEngine.MonoBehaviour component { get; }
    }
    public interface IPokerMatchAction
    {
        int id { get; }
        int overhide_id { get; set; }
    }
}