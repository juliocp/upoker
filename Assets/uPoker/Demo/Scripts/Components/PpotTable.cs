﻿using UnityEngine;
using System.Collections;
using PokerCollections;
using PokerCollections.Analysis;
using PokerCollections.MatchGame;

/*
====READ-ME=================================================
These are the odds that a currently losing hand will improve to be a winner. 
Positive Potential of a player's hand winner
When you are playing poker on-line, you don't have access to other players' pocket cards. 
One technique that can be used is to analyze the potential of a player's hand compared to an average opponent. 
This is calculated by comparing all possible boards and all possible opponent pocket cards to the player’s hand.
This gives a good feeling for the potential of a hand.However, rarely are you playing against an "average" player.
So keep that in mind if you are using this technique in real poker play.
The following code example shows how this calculation is made:  

Use the methods of the class, only when all the hole cards of the player and three or more cards on 
the table. Otherwise the turnaround time for any results will be very large, making impossible the use of 
this class 
------------------------------------------------------------------
> There are several ways to calculate the POT. Let's see a in this example: 
Calculating POT from a gambling table.  
------------------------------------------------------------------
*/
public class PpotTable : MonoBehaviour
{

    PPOT Ppot; //Positive POTential
   
    void Start()
    {
        //Create a new Poker Table
        PokerTable table = new PokerTable(true);
        //Create a new Poker Match Mananger to the pokerTable
        MatchGame match = new MatchGame(this, table);

        match.StartNewMatch();

        //Add New Player
        BetPlayer player = table.CreateBetPlayer("test");
        //Take cards to Player!
        match.GetCardsToBetPlayers();
        //Take first 3 cards from "FLOP" to the Board
        match.turn = BetTurn.flop;

        //Create a new Analizis to the Positive potential hand, 1 Oppent only
        Ppot = new PPOT();
        Ppot.CalculePPOT(player.holeCards, table.boardCards);

        Debug.Log("Hand: " + player.PrintCards());
        Debug.Log("Board " + table.PrintBoardCards());
        Debug.Log("Hand Description: " + player.getHandDescription());
        Debug.Log("PPOT: " + Ppot.ToString());
    }
}
