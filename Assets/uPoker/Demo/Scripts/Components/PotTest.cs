﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PokerCollections;
using PokerCollections.MatchGame;
using System.Linq;
/*
Como funciona o algoritmo passo passso?


 */
public static class PotTestExtsions
{
    public static void CreateBetPlayer(this List<PotTest.PLAYER> players, PokerTable table)
    {
        foreach (PotTest.PLAYER p in players)
        {
            p.Create(table);

        }
    }
    public static void CreateBetActions(this List<PotTest.BetAction> actions, MatchGame match)
    {
        foreach (PotTest.BetAction a in actions)
        {
            BetPlayer p = match.pokertable.betplayers.Find(r => r.placeId == a.placeID);
            if (p != null)
                match.matchActions.AddBetAction(p, a.betTeype, a.betChips, a.betTurn);
        }
    }
    public static string ToPokerString(this List<PotTest.PLAYER> players)
    {
        string result = "";
        foreach (PotTest.PLAYER p in players)
        {
            result += p.name + ": " + p.chipsAmount.ToString("C2") + ", " + PokerEval.CardsToEval(p.betplayer.holeCards.ToList());
            result += "\n";
        }

        return result;
    }

}
public class PotTest : MonoBehaviour
{
    [System.Serializable]
    public class PLAYER
    {

        public string name;
        public int placeID;
        public double chipsAmount;
        public string handCards;
        public BetPlayer betplayer { get; private set; }

        public void Create(PokerTable pokertable)
        {
            betplayer = new BetPlayer(name, null);
            betplayer.overhidePlaceID = placeID;
            betplayer.overhidePokertable = pokertable;
            betplayer.AddChip(chipsAmount);

            List<Card> cards = PokerEval.EvalToCards(handCards);

            foreach (Card c in cards)
                betplayer.holeCards.Add(c);

            pokertable.CreateBetPlayer(betplayer);
        }
    }

    [System.Serializable]
    public class BetAction
    {
        public string name;
        public int placeID;
        public double betChips;
        public BetType betTeype;
        public BetTurn betTurn;
    }

    public string boardCards;
    //Lista de jogadores
    public List<PLAYER> Players;
    //Lista de apostas
    public List<BetAction> Actions;

    void Start()
    {
        StartCoroutine("start");

    }
    IEnumerator start()
    {
        PokerTable table = new PokerTable(true);
        MatchGame match = new MatchGame(this, table);

        //Criando cartas da mesa
        List<Card> cards = PokerEval.EvalToCards(boardCards);
        foreach (Card c in cards)
            table.boardCards.Add(c);

        //Criando jogadores na partida
        Players.CreateBetPlayer(table);
        ////Simulando apostas
        Actions.CreateBetActions(match);
        //Criando potes de premiação

        //Exemplo usando POT para verificar participacao de lucros de cada jogador
        EvaluatedHoldemPot potmananger = new EvaluatedHoldemPot(this, match);

        while (!potmananger.isDone)
            yield return null;

        //Recebendo lista de potes contendo os valores de premiacoes que cada jogador tem direito
        List<EvaluatedHoldemPot.POT> pots = potmananger.Get_pots();

        foreach (EvaluatedHoldemPot.POT pot in pots)
        {
            if (pot.betplayer.Is_Allin())
                Debug.Log("<color=#ff0000>"+pot.betplayer.name + "</color>: " + pot.evalueType.ToString() + " " + pot.chips);
            else
                Debug.Log("<color=#ffee00><b>" + pot.betplayer.name + "</b></color>: " + pot.evalueType.ToString() + " " + pot.chips);
        }

        //Verificando se existe valor de sobra para ser retornado a mesa
        Debug.Log("LeftOver " + potmananger.leftOver.ToString("C2"));

        yield return null;

    }

}
