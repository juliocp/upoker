﻿using UnityEngine;
using System.Collections;
using PokerCollections;
using PokerCollections.Analysis;
using PokerCollections.MatchGame;
/*
====READ-ME=================================================
Positive Potential Monte Carlo Algorithm
Monte Carlo analysis can be used in analyzing the Positive Potential of a player's hand winner, compared to an average opponent.
There are many reasons people choose to use Monte Carlo Analysis.
In poker, the most common reason is to get a quick answer for something that otherwise would take a prohibitively long time to calculate.
Using this method of analysis, you will be able to check the potêncial of his letters, even when there is no card on the table, only the hand!
*/
public class PpotMonteCarloTable : MonoBehaviour
{
    PPOT_MONTECARLO Ppot; //Positive POTential

    void Start()
    {
        //Create a new Poker Table
        PokerTable table = new PokerTable(true);
        //Create a new match mananger for this pokerTable
        MatchGame match = new MatchGame(this, table);

        match.StartNewMatch();

        //Add New Player
        BetPlayer player = table.CreateBetPlayer("test");
        //Take cards to Player!
        match.GetCardsToBetPlayers();
        //Take first 3 cards from "FLOP" to the Board
        match.turn = BetTurn.flop;

        //Create a new PPOT for probability calcule
        Ppot = new PPOT_MONTECARLO();
		Ppot.Calcule(1,player.holeCards, table.boardCards);

        Debug.Log("Hand: " + player.PrintCards());
        Debug.Log("Board " + table.PrintBoardCards());
        Debug.Log("Hand Description: " + player.getHandDescription());
        Debug.Log("PPOT Player: " + Ppot.player.ToString());
        Debug.Log("PPOT Opponent: " + Ppot.opponent.ToString());
    }
}
