﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using PokerCollections;
using System.Linq;

//In this example, I will show how simple it is to work with a visual card deck using UI.
public class SimpleDeck : MonoBehaviour
{
    public string spriteResources;
    public GameObject CardItem;
    public RectTransform CardsContent;
    public Text txt_cardsNumber;

    //New Deck cards
    Deck deck;
    //taken cards from deck and placed on the table
    CardsList<Card> boardCards;

    // Use this for initialization
    void Start()
    {
        deck = new Deck();

        deck.SetSpriteRessources(spriteResources);

        boardCards = new CardsList<Card>();

        txt_cardsNumber.text = deck.Cards.Count().ToString();


    }

    #region PUBLIC UI METHODS FOR USE
    //Take one card of the deck and set in board cards
    public void TakeCard()
    {
        Card card = deck.Cards.TakeItem();

        if (card != null)
        {
            GameObject cardObj = Instantiate(CardItem);
            cardObj.transform.SetParent(CardsContent);
            cardObj.GetComponent<UICardItem>().SetCard(card);
            boardCards.Add(card);
        }

        txt_cardsNumber.text = deck.Cards.Count().ToString();
    }
    //Shuffle all cards of the deck. Only of the deck
    public void ShuffleDeck()
    {
        deck.Shuffle();
        txt_cardsNumber.text = deck.Cards.Count().ToString();

    }
    //Take all cards of the deck ans set in board cards
    public void takeAllCards()
    {
        int count = deck.Cards.Count;
        for (int a=0; a < count; a++)
        {
            Card card = deck.Cards.TakeItem();
            GameObject cardObj = Instantiate(CardItem);
            cardObj.transform.SetParent(CardsContent);
            cardObj.GetComponent<UICardItem>().SetCard(card);
            boardCards.Add(card);
        }

        txt_cardsNumber.text = deck.Cards.Count().ToString();

    }
    //Restore deck to default cards and order.
    //Remove all cards of the board
    public void ResetDeck()
    {
        foreach (Card c in boardCards)
            Destroy(c.component);

        boardCards.Clear();
        deck.Reset();

        txt_cardsNumber.text = deck.Cards.Count().ToString();

    }
    #endregion
}
