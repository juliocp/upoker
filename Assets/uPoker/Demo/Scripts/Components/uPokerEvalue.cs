﻿using UnityEngine;
using System.Collections.Generic;
using uPoker;

/*
READ-ME=====================================================
This will use the Poker hands validation engine to create and validate hands and print the final result, 
showing winners, losers, draws and also description of their hands on the game! 
Note uPoker is a complete solution to validate poker hands. 
You are free to develop their own classes and methods to manage players, games tables, scores etc. 
> If you want to gain time, look at POKERCOLLECTIONS solution. 
==============================================================
*/

public class uPokerEvalue : MonoBehaviour
{
    //Create board cards for evaluating
    string board = "ah 2h 3c kd 5d";

    void Start()
    {
        List<string> pokets; //Poketc cards hand list

        //Create player1 hand for evaluating
        Hand player1 = new Hand("as ks", board);
        //Create player1 hand for evaluating
        Hand player2 = new Hand("jh jc", board);
        //Add poketcards in list
        pokets = new List<string>();
        pokets.Add(player1.PocketCards);
        pokets.Add(player2.PocketCards);
        //Creating lists for get evaluating result
        long[] winss = new long[pokets.Count];
        long[] ties = new long[pokets.Count];
        long[] losses = new long[pokets.Count];
        long totalhands = 0;
        
        //HandOdds evaluating
        Hand.HandOdds(pokets.ToArray(), board, "", winss, ties, losses, ref totalhands);

        //Print hand descrition from players
        Debug.Log("Hand1: " + player1.Description);
        Debug.Log("Hand2: " + player2.Description);
        //Print best five cards from players
        Debug.Log("Best five cards from HAND1 " + player1.BestFiveCards());
        Debug.Log("Best five cards from HAND2 " + player2.BestFiveCards());

        //Result
        for (int a = 0; a < pokets.Count; a++)
        {
            if (winss[a] > 0)
            {
                Debug.Log("Player" + (a + 1) + " winner");
            }
            else if (ties[a] > 0)
            {
                Debug.Log("Player" + (a + 1) + " tie");
            }
            else if (losses[a] > 0)
            {
                Debug.Log("Player" + (a + 1) + " loose");
            }
        }

    }

}
