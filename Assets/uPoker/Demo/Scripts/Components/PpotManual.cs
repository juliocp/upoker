﻿using UnityEngine;
using System.Collections;
using PokerCollections;
using PokerCollections.Analysis;

/*
====READ-ME=================================================
These are the odds that a currently losing hand will improve to be a winner. 
Positive Potential of a player's hand winner
When you are playing poker on-line, you don't have access to other players' pocket cards. 
One technique that can be used is to analyze the potential of a player's hand compared to an average opponent. 
This is calculated by comparing all possible boards and all possible opponent pocket cards to the player’s hand.
This gives a good feeling for the potential of a hand.However, rarely are you playing against an "average" player.
So keep that in mind if you are using this technique in real poker play.
The following code example shows how this calculation is made:  

Use the methods of the class, only when all the hole cards of the player and three or more cards on 
the table. Otherwise the turnaround time for any results will be very large, making impossible the use of 
this class 
------------------------------------------------------------------
> There are several ways to calculate the POT. Let's see a in this example: 
Calculating POT from a manual cards  
------------------------------------------------------------------
*/

public class PpotManual : MonoBehaviour
{

    PPOT pPot;

    void Start()
    {
        //Create a new PPOT for probability calcule
        string playerCards = "as ks";
        string boardCards = "Ts Qs 2d";

        pPot = new PPOT();
        pPot.CalculePPOT(playerCards, boardCards);

        Debug.Log("Hand: " + playerCards);
        Debug.Log("Board " + boardCards);
        Debug.Log("Hand Description: " + PokerEval.GetHandDescription(playerCards, boardCards));
        Debug.Log("PPOT: " + pPot.ToString());
    }
}
