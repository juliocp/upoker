﻿using UnityEngine;
using System.Collections;
using PokerCollections;
using PokerCollections.Analysis;

/*
====READ-ME=================================================
This method returns the approximate odd for the players mask winning against multiple opponents and no detalhed information. 
This uses a default time duration of 0.25S (or 250mS) for the time allotment for Monte Carlo analysis.
If you are interested in a full chance to report the percentage of each specific hand along with the total, 
use the initialized class PPOT or PPOT_MonteCarlo
 */
public class PpotMonteCarloSimple : MonoBehaviour
{
    public string playerCards;
    public string boardCards;
    public int opponentCount;
    [Header("ReadOnly")]
    public string percentWinner="";

    void Start () 
    {
        double percent = PPOT_MONTECARLO.GetWinnerPotential(playerCards, boardCards, opponentCount) * 100;
        percentWinner = percent.ToString("N1") + "%";
        Debug.Log(percentWinner);
    }
}
