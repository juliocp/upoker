﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PokerCollections;
using PokerCollections.MatchGame;

/*
====READ-ME=================================================
Here we will use a simple but very functional library to work with the validation engine uPoker hands. 
This will allow you to manage the starting information, facilitating the creation and management of all 
components necessary for the development of a poker game.
Note that the PokerCollections library is open and has the intention to be an initial step you implement
new methods that meet all your needs in creating your game.
> Feel free to modify and implement your needs.

Using the Poker Collections library is very easy to create and validate poker texas hold hand. 
- Support for multiple matches games simultaneous.
- Easy Player create and mananger
- Easy PokerTable create and Mananger
- Easy Deck Card mananger
- Fast and Very Very Very easy evalue hands
============================================================
*/
public class EasyEvalue : MonoBehaviour
{
    void Start()
    {
        //We create a PokerTable for the game 
        //and choose to automatically create a deck and shuffle to.  
        PokerTable table = new PokerTable(true);
        MatchGame match = new MatchGame(this, table);

        //Creating Player for gamming
        table.CreateBetPlayer("player1", 1);
        table.CreateBetPlayer("player2", 2);

        //Start new matchGame
        match.StartNewMatch();

        /*============================================================
         -With my already shuffled deck, you can simply receive letters directly from the order, 
         because the cards are already in random order.
         ===============================================================*/

        //TAKE PLAYER HAND CARDS FROM DECK
        //Giving the two player cards automatically and at cyclo
        match.GetCardsToBetPlayers();


        /*TAKE BOARD CARDS FROM DECK
        ===============================================================
        Whenever you modify the current round of the game, new cards will be sent automatically 
        to the table and also to active players of the game.
        Events will always be fired that hidden actions the player is taken by the manager, 
        and you can easily intercept this event if desired.
        Check the documentation for more information!
        ===============================================================
        */

        //Change turn and giving the tree Flop cards board automatically for all player in game.
        match.turn = BetTurn.flop;
        //Change turn and giving the one Turn card board automatically for all player in game.
        match.turn = BetTurn.turn;
        //Change turn and giving the one River card board automatically for all player in game.
        match.turn = BetTurn.river;

        Debug.Log("Board Cards: " + table.PrintBoardCards());
        //Print all players hand description
        Debug.Log("Hand Descriptions: " + table.PrintPlayersHandCards());

        //Validate and get list with winners, ties and lossers players.
        EvaluatedHoldemHand handOdds = match.GetEvaluatedHandOdds();

        if (handOdds != null)
        {
            //Print odds result
            Debug.Log("Evaluated Result: " + handOdds.ToString());

            foreach (BetPlayer player in handOdds.winners)
            {
                //Print from eval cards
                Debug.Log(player.holdemhand.PocketCards + " " + player.holdemhand.Board);
                Debug.Log("Best five cards of the hand: (" + player.holdemhand.BestFiveCards() + ")");

                //List of the best five from evaluated cards and if this on hand or on board table
                List<Card> cards = player.GetBestFiveCards();
                foreach (Card c in cards)
                {
                    if (player.holeCards.Exists(r => r.id == c.id))
                        Debug.Log("At Hand > " + c.ToString());
                    else
                        Debug.Log("At Board > " + c.ToString());
                }
            }
        }

        //Clear player cards and table cards
        //set deckcards to default and shuffled
        match.ResetTable();
    }
}
