﻿using UnityEngine;
using System.Collections;
using PokerCollections;
using PokerCollections.Analysis;
/*
Positive Potential Monte Carlo Algorithm
Monte Carlo analysis can be used in analyzing the Positive Potential of a player's hand winner, compared to an average opponent.
There are many reasons people choose to use Monte Carlo Analysis.
In poker, the most common reason is to get a quick answer for something that otherwise would take a prohibitively long time to calculate.
Using this method of analysis, you will be able to check the potêncial of his letters, even when there is no card on the table, only the hand!
*/
public class PpotMonteCarloManual : MonoBehaviour
{
    PPOT_MONTECARLO Ppot; //Positive POTential

    void Start()
    {
        //Create a new PPOT for probability calcule
        string playerCards = "as ks";
        string boardCards = "Ts Qs 2d";

        //Create a new PPOT for probability calcule
        Ppot = new PPOT_MONTECARLO();
		Ppot.Calcule(playerCards, boardCards);

        Debug.Log("Hand: " + playerCards);
        Debug.Log("Board " + boardCards);
        Debug.Log("Hand Description: " + PokerEval.GetHandDescription(playerCards, boardCards));
        Debug.Log("PPOT Player: " + Ppot.player.ToString());
        Debug.Log("PPOT Opponent: " + Ppot.opponent.ToString());
    }
}
