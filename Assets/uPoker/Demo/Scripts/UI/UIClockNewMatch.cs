﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PokerCollections.MatchGame;
using System;

/*
Utilidade: 
Este componente tem como finalidade mostrar um simples Contador Regrecesso em modo visual, 
sempre que uma nova partida estiver em modo de PREPARO, para ser iniciada efetivamente.

Como usar:
Adicione este script a algum componente utilizando CANVAS.
> Instancie uma imagem com as configurações de (FILL) para que seu FillAmount seja atualizado, 
indicando o tempo restante para o inicio efetivo da partida
> Você podera manter o componente em modo DESABILITADO, uma vez que ele sera habilidade e desabilitado automáticamente
 */
[RequireComponent(typeof(UISplashPanel))]
public class UIClockNewMatch : MonoBehaviour
{
    //Imagem em modo FILL
    public Image imageClock;

    public void Open(MatchGame match)
    {
        gameObject.GetComponent<UISplashPanel>().OpenItSellf();
        StartCoroutine(timerChange(match));
    }
    private void OnEnable()
    {

    }  
 
    private void OnMathcStart(MatchGame match)
    {
        Close();
    }    
    private void Close()
    {
        gameObject.GetComponent<UISplashPanel>().CloseItSelff();
    }
    private IEnumerator timerChange(MatchGame match)
    {
        while (!match.isStarted)
        {
            imageClock.fillAmount = match.regreciveAmount;
            yield return null;

        }

        yield break;
    }
}
