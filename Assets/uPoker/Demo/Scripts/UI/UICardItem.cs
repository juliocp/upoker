﻿using UnityEngine;
using PokerCollections;
using UnityEngine.UI;
using System.Collections;

public class UICardItem : MonoBehaviour
{
    Card card;

    public void SetCard(Card _card)
    {
        card = _card;
        card.component = gameObject;
        GetComponent<Image>().overrideSprite = card.sprite;
    }


}
