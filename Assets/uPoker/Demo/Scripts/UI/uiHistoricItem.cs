﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PokerCollections.MatchGame;

public class uiHistoricItem : MonoBehaviour
{

    [Header("Dependences")]
    public Text txt_PokerID;
    public Text txt_MatchID;
    public Text txt_PlayersCount;
    public Text txt_ChipsAmount;
    public Text txt_MatchTime;

    public MatchGame myMatchGame { get; private set; }


    public void SetItem(MatchGame matchGameItem)
    {
        myMatchGame = matchGameItem;        
        txt_PokerID.text = myMatchGame.pokertable.id.ToString("D2");
        txt_MatchID.text = myMatchGame.id.ToString("D2");
        txt_PlayersCount.text = myMatchGame.playersCount.ToString("D2");
        txt_ChipsAmount.text = myMatchGame.Get_PoteTotal().ToString("N2");

    }


}
