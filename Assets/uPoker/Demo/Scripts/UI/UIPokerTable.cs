﻿using UnityEngine;
using System.Collections;
using PokerCollections;
using PokerCollections.MatchGame;
using UnityEngine.UI;
using System;

public class UIPokerTable : PokerTable_MonoBehavior
{
    [System.Serializable]
    public class Sources
    {
        public Sprite img_emptCard;
    }

    [System.Serializable]
    public class Dependences
    {
        public Text txt_pot;
        public Text txt_turn;
        public Image img_card1;
        public Image img_card2;
        public Image img_card3;
        public Image img_card4;
        public Image img_card5;
    }

    [Header("PokerTable Configurations")]
    public int TableID;

    [Header("Match Configurations")]
    public MatchPoteType potType;
    public double smallBlind;
    public double bigBlind;
    public double smallStake;
    public double bigStake;

    public MatchConfiguration config { get; private set; }

    public Sources sources;
    public Dependences dependences;

    public PokerTable pokerTable { get; private set; }
    private MatchGame pokerMatch;

    private UIMatchMananger uIMatchMananger;

    // Use this for initialization
    void Awake()
    {
        uIMatchMananger = gameObject.GetComponent<UIMatchMananger>();

        pokerTable = new PokerTable(this, TableID);
        pokerTable.deck.SetSpriteRessources("Deck");
        pokerTable.overHide_ID(TableID);

        config = new MatchConfiguration();
        config.poteType = potType;
        config.smallBlind = smallBlind;
        config.bigBlind = bigBlind;
        config.smallStake = smallStake;
        config.bigStake = bigStake;

        DesativeBoardCards();
    }

    public void StartNewMatch()
    {
        if (pokerTable.betplayers.Count >= 2)
        {
            dependences.txt_pot.text = " $000";
            dependences.txt_turn.text = "none";
            pokerMatch = new MatchGame(uIMatchMananger, pokerTable, config, true);
        }
    }
    public void ResetMatch()
    {
        if (pokerMatch == null)
            return;

        dependences.txt_pot.text = " $000";
        dependences.txt_turn.text = "none";
        pokerMatch.ResetTable();
    }
    public void DesativeBoardCards()
    {

        dependences.img_card1.gameObject.SetActive(false);
        dependences.img_card2.gameObject.SetActive(false);
        dependences.img_card3.gameObject.SetActive(false);
        dependences.img_card4.gameObject.SetActive(false);
        dependences.img_card5.gameObject.SetActive(false);

        dependences.img_card1.overrideSprite = sources.img_emptCard;
        dependences.img_card2.overrideSprite = sources.img_emptCard;
        dependences.img_card3.overrideSprite = sources.img_emptCard;
        dependences.img_card4.overrideSprite = sources.img_emptCard;
        dependences.img_card5.overrideSprite = sources.img_emptCard;

    }

}
