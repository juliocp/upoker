﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PokerCollections;
using PokerCollections.MatchGame;

public class UIHistoricMananger : MonoBehaviour
{
    [Header("Dependences")]
    public UIPokerTable uiPokerTable;
    public RectTransform contentView;

    [Header("PreFabs")]
    public GameObject historicItem;

    private List<MatchGame> matches;
    private List<uiHistoricItem> items;
    void OnEnable()
    {
        matches = uiPokerTable.pokerTable.Get_Matches();
        
        if (items == null)
            items = new List<uiHistoricItem>();

        foreach (MatchGame game in matches)
        {
            GameObject item = Instantiate(historicItem);
            item.transform.SetParent(contentView, false);
            item.transform.localScale = historicItem.transform.localScale;
            item.transform.localPosition = historicItem.transform.localPosition;
            items.Add(item.GetComponent<uiHistoricItem>());
            item.GetComponent<uiHistoricItem>().SetItem(game);
        }
        
    }
    void OnDisable()
    {
        foreach (uiHistoricItem item in new List<uiHistoricItem>(items))
        {
            Destroy(item.gameObject);
        }
        items.Clear();
        matches.Clear();
    }

}
