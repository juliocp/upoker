﻿using UnityEngine;
using System.Collections;
using PokerCollections.MatchGame;
using PokerCollections;
using System;
using PokerCollections.EventHandler;
using System.Collections.Generic;

[RequireComponent(typeof(UIPokerTable))]
public class UIMatchMananger : MatchGame_MonoBehavior
{
    UIPokerTable uiPokertable;
    public bool ManualNextAction = false;

    private PokerTable pokerTable;
    private MatchGame pokerMatch;

    void Start()
    {
        uiPokertable = gameObject.GetComponent<UIPokerTable>();
        MatchGameListner.Matches.OnAdd += OnAddMatch;

    }
    private void OnAddMatch(MatchEventArgs e)
    {
        MatchGame game = e.IMatch as MatchGame;
        if (game.pokertable == uiPokertable.pokerTable)
        {
            pokerMatch = game;
            pokerTable = pokerMatch.pokertable;

            pokerMatch.matchActions.OnAdd += OnAction;
            pokerMatch.OnTurn += OnTurn;
            pokerTable.boardCards.OnAdd += OnBoardCard;

            uiPokertable.DesativeBoardCards();

            StartCoroutine(NewMatch());
        }
    }
    private void OnBoardCard(object sender, CardEventArgs e)
    {
        Card card = e.ICard as Card;
        switch (pokerTable.boardCards.Count)
        {
            case 1:
                uiPokertable.dependences.img_card1.overrideSprite = card.sprite;
                uiPokertable.dependences.img_card1.gameObject.SetActive(true);
                break;
            case 2:
                uiPokertable.dependences.img_card2.overrideSprite = card.sprite;
                uiPokertable.dependences.img_card2.gameObject.SetActive(true);

                break;
            case 3:
                uiPokertable.dependences.img_card3.overrideSprite = card.sprite;
                uiPokertable.dependences.img_card3.gameObject.SetActive(true);

                break;
            case 4:
                uiPokertable.dependences.img_card4.overrideSprite = card.sprite;
                uiPokertable.dependences.img_card4.gameObject.SetActive(true);

                break;
            case 5:
                uiPokertable.dependences.img_card5.overrideSprite = card.sprite;
                uiPokertable.dependences.img_card5.gameObject.SetActive(true);

                break;
        }

    }
    private void OnTurn(MatchEventArgs e)
    {
        uiPokertable.dependences.txt_turn.text = pokerMatch.turn.ToString();
        Debug.LogWarning("Turn: " + pokerMatch.turn.ToString());
    }
    private void OnAction(MatchActionEventArgs e)
    {
        //MatchAction action = e.IMatch as MatchAction;
        uiPokertable.dependences.txt_pot.text = pokerMatch.matchActions.Get_ChipsAmount().ToString("C2");

    }
    private IEnumerator NewMatch()
    {
        MatchGame.IsDone isDone = pokerMatch.StartNewMatch(0);
        while (!isDone.done)
            yield return null;

        isDone = pokerMatch.GetCardsToBetPlayersAsync();
        while (!isDone.done)
            yield return null;

        BetTurn turn = pokerMatch.turn;

        if (!ManualNextAction)
        {

            MatchAllowedAction allowAction = turn.Get_AllowAction(pokerMatch);

            while (!allowAction.isOver)
            {
                allowAction.AutoExecute();
                allowAction = allowAction.Next();
                yield return new WaitForSeconds(2);
            }
        }
        else
        {

            //Aguarda a partida finalizar para fazer verificação de vencedores
            while (turn != BetTurn.showdown)
            {
                turn = pokerMatch.turn;
                yield return null;
            }

            //Validate and get list with winners, ties and lossers players.
            //EvaluatedHoldemHand handOdds = pokerMatch.GetEvaluatedHandOdds();
            //Print odds result
            //Debug.Log(handOdds.ToString());


            //Exemplo usando POT para verificar participacao de lucros de cada jogador
            EvaluatedHoldemPot potmananger = new EvaluatedHoldemPot(this, pokerMatch);

            while (!potmananger.isDone)
                yield return null;

            List<EvaluatedHoldemPot.POT> pots = potmananger.Get_pots();

            foreach (EvaluatedHoldemPot.POT pot in pots)
            {
                Debug.Log(pot.betplayer.name + ": " + pot.evalueType.ToString() + " " + pot.chips);
            }

            //Verificando se existe valor de sobra para ser retornado a mesa
            Debug.Log("LeftOver " + potmananger.leftOver.ToString("C2"));


        }


        //Debug.Log("isOver");

    }
    public void nextAction()
    {
        if (pokerMatch == null)
            return;
        
        BetTurn turn = pokerMatch.turn;
        if (turn == BetTurn.showdown || turn == BetTurn.none)
            return;

        if (ManualNextAction)
        {
            MatchAllowedAction allowAction = turn.Get_AllowAction(pokerMatch);

            if (!allowAction.isOver)
            {
                allowAction.AutoExecute();
                allowAction = allowAction.Next();

            }
            else
            {
                pokerMatch.turn = turn.Next();
            }


        }

    }
    public override void OnDestroy()
    {
        MatchGameListner.Matches.OnAdd -= OnAddMatch;

        if (pokerMatch != null)
        {
            pokerMatch.OnTurn -= OnTurn;
            pokerMatch.matchActions.OnAdd -= OnAction;
        }

        if (pokerTable != null)
        {
            pokerTable.boardCards.OnAdd -= OnBoardCard;
        }

        base.OnDestroy();
    }

}
