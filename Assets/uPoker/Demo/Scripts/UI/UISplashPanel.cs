﻿using UnityEngine;
using System.Collections;

public class UISplashPanel : MonoBehaviour
{

    public UISplashPanel Modal;

    public void OpenItSellf()
    {
        if (Modal != null)
            Modal.OpenItSellf();
        gameObject.SetActive(true);
    }
    public void Open(UISplashPanel pannel)
    {
        if (Modal != null)
            Modal.OpenItSellf();


        pannel.OpenItSellf();
    }
    public void Close(UISplashPanel pannel)
    {
        if (Modal != null)
            Modal.CloseItSelff();

        pannel.CloseItSelff();
    }
    public void CloseItSelff()
    {
        gameObject.SetActive(false);
        if (Modal != null)
            Modal.CloseItSelff();

    }
    public void Trade(UISplashPanel pannel)
    {

        CloseItSelff();
        pannel.OpenItSellf();

    }

}
