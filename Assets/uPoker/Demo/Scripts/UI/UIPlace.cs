﻿using UnityEngine;
using System.Collections;
using PokerCollections;
using PokerCollections.MatchGame;
using System;
using PokerCollections.EventHandler;

public class UIPlace : MonoBehaviour
{
    [System.Serializable]
    public class Place
    {
        public int placeID;
        public Transform component;
    }

    public UIPokerTable uiPokerTable;
    public UIBetPlayer uiBetplayer;
    public Place place;

    private PokerTable pokerTable;

    void Start()
    {
        pokerTable = uiPokerTable.pokerTable;
        if (pokerTable == null)
            throw new NullReferenceException("null pokerTable reference, pokerTable not exist");

        //Escutando eventos
        pokerTable.OnAddBetPlayer_Subscribe(OnAddPlayer);
        pokerTable.OnRemoveBetPlayer_Subscribe(OnRemovePlayer);
        pokerTable.OnAddHoplayer_Subscribe(OnAddPlayer);
        pokerTable.OnRemoveHoplayer_Subscribe(OnRemovePlayer);
    }

    //add player manually by clicking the places button
    public void IA_Sitdown()
    {
        try
        {
            //if the game already has started, we will add this player to the list of spectators and 
            //when a new game is started, let's add viewers players in the list of active players. 
            if (pokerTable.HasCurrentActiveGame())
                pokerTable.CreateHopleyer("Teste-" + place.placeID.ToString("D2"), place.placeID, place.placeID, 200);
            else
                pokerTable.CreateBetPlayer("Teste-" + place.placeID.ToString("D2"), place.placeID, place.placeID, 200);

        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }

    }
    public void IA_Situp()
    {
        if (uiBetplayer.betplayer != null)
        {
            if (uiBetplayer.betplayer.isHoplayer())
            {
                pokerTable.hoplayers.Remove(uiBetplayer.betplayer);
            }
            else
            {
                pokerTable.betplayers.Remove(uiBetplayer.betplayer);
            }
            
        }
    }
    private bool isMe(BetPlayer betPlayer)
    {
        if (betPlayer == null)
            return false;

        if (betPlayer.lastPokerTable != pokerTable.id)
            return false;

        if (place.placeID != betPlayer.placeId)
            return false;

        return true;
    }
    private void OnAddPlayer(BetPlayerEventArgs e)
    {

        BetPlayer betPlayer = e.IbetPlayer as BetPlayer;

        if (!isMe(betPlayer))
            return;

        uiBetplayer.SetBetPlayer(betPlayer);
        place.component.gameObject.SetActive(false);

    }
    private void OnRemovePlayer(BetPlayerEventArgs e)
    {
        
        BetPlayer betPlayer = e.IbetPlayer as BetPlayer;

        if (!isMe(betPlayer))
            return;

        uiBetplayer.ResetBetPlayer();
        place.component.gameObject.SetActive(true);

    }

    void OnDestroy()
    {
        pokerTable.OnAddBetPlayer_Unsbscribe(OnAddPlayer);
        pokerTable.OnRemoveBetPlayer_Unsbscribe(OnRemovePlayer);
        pokerTable.OnAddHoplayer_Unsbscribe(OnAddPlayer);
        pokerTable.OnRemoveHoplayer_Unsbscribe(OnRemovePlayer);
    }

}
