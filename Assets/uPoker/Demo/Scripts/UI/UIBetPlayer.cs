﻿using UnityEngine;
using System.Collections;
using PokerCollections;
using PokerCollections.MatchGame;
using System;
using UnityEngine.UI;
using PokerCollections.EventHandler;

public class UIBetPlayer : MonoBehaviour
{
    [System.Serializable]
    public class Sources
    {
        public Sprite img_actionNone;
        public Sprite img_actionBet;
        public Sprite img_actionCall;
        public Sprite img_actionCheck;
        public Sprite img_actionRaise;
        public Sprite img_actionFold;
        public Sprite img_actionAllin;
    }
    [System.Serializable]
    public class Dependences
    {
        public Text txt_name;
        public Text txt_chipAmount;
        public Text txt_betChips;
        public Image img_dealler;
        public Image img_smaller;
        public Image img_bigger;
        public Image img_card1;
        public Image img_card2;
        public Image img_betChips;
        public Image img_action;
    }

    public Sources sources;
    public Dependences dependences;

    public BetPlayer betplayer { get; private set; }
    public PokerTable pokertable { get; private set; }
    private MatchGame pokerMatch;

    //Metodos publicos
    public void SetBetPlayer(BetPlayer _betplayer)
    {
        gameObject.SetActive(true);
        betplayer = _betplayer;
        dependences.txt_name.text = betplayer.name;
        dependences.txt_chipAmount.text = betplayer.chipsAmount.ToString("C2");
        pokertable = betplayer.pokerTable;

        MatchGameListner.Matches.OnAdd += OnMatch;
        MatchGameListner.Matches.OnRemove += OffMatch;

        betplayer.holeCards.OnAdd += OnAddCard;
        betplayer.holeCards.OnRemove += OnRemoveCard;
        betplayer.OnAddChips += OnAddChip;
        betplayer.OnRemoveChips += OnSubChip;

    }
    public void ResetBetPlayer()
    {
        
        
        dependences.txt_name.text = string.Empty;
        dependences.txt_chipAmount.text = string.Empty;

        MatchGameListner.Matches.OnAdd -= OnMatch;
        MatchGameListner.Matches.OnRemove -= OffMatch;

        betplayer.holeCards.OnAdd -= OnAddCard;
        betplayer.holeCards.OnRemove -= OnRemoveCard;
        betplayer.OnAddChips -= OnAddChip;
        betplayer.OnRemoveChips -= OnSubChip;

        betplayer = null;
        gameObject.SetActive(false);

        //betplayer._OnAddChip -= OnAddChips;
        // betplayer._OnSubChip -= OnSubChips;
    }

    //Metodos privados
    private void ChangeCurrent(BetPlayer _betplayer)
    {

    }
    private void ChangeBigger(BetPlayer _betplayer)
    {
        if (_betplayer.Equals(betplayer))
        {
            dependences.img_bigger.gameObject.SetActive(true);
        }
        else
        {
            dependences.img_bigger.gameObject.SetActive(false);
        }
    }
    private void ChangeSmaller(BetPlayer _betplayer)
    {

        if (_betplayer.Equals(betplayer))
        {
            dependences.img_smaller.gameObject.SetActive(true);
        }
        else
        {
            dependences.img_smaller.gameObject.SetActive(false);
        }

    }
    private void ChangeDealler(BetPlayer _betplayer)
    {
        if (_betplayer.Equals(betplayer))
        {
            dependences.img_dealler.gameObject.SetActive(true);
        }
        else
        {
            dependences.img_dealler.gameObject.SetActive(false);
        }


    }

    //Eventos Nova partida para a mesa
    private void OnMatch(MatchEventArgs e)
    {
        if (e.IMatch.pokertable == pokertable)
        {
            MatchGame game = (MatchGame)e.IMatch;
            pokerMatch = game;
            pokerMatch.OnBetPlayer_Current += ChangeBetPlayerCurrent;
            pokerMatch.OnTurn += ChangeTurn;
            pokerMatch.matchActions.OnAdd += OnAction;
        }
    }
    private void OffMatch(MatchEventArgs e)
    {
        if ((MatchGame)e.IMatch == pokerMatch)
        {

        }
    }

    //Eventos de partida em andamento
    private void ChangeTurn(MatchEventArgs e)
    {
        if ((MatchGame)e.IMatch == pokerMatch)
        {
            dependences.txt_betChips.text = "0";
            dependences.img_betChips.gameObject.SetActive(false);
            dependences.img_action.overrideSprite = sources.img_actionNone;
            // Debug.LogWarning("Change turn "+pokerMatch.turn);
        }
    }
    private void ChangeBetPlayerCurrent(MatchEventArgs e)
    {
        if ((MatchGame)e.IMatch == pokerMatch)
        {
            ChangeCurrent(pokerMatch.BetPlayer_Current);
            ChangeSmaller(pokerMatch.BetPlayer_Small);
            ChangeBigger(pokerMatch.BetPlayer_Big);
            ChangeDealler(pokerMatch.BetPlayer_Dealer);
        }
    }
    private void OnAction(MatchActionEventArgs e)
    {
        MatchAction action = (MatchAction)e.IMatchAction;

        if (((MatchGame)e.IMatch) == pokerMatch && action.match_player.betPlayer == betplayer)
        {
            Debug.Log(betplayer.name + " " + action.bet_action.ToString() + " " + action.bet_chips.ToString("C2") + " Total = " + action.bet_total.ToString("c2"));


            switch (action.bet_action)
            {
                case BetType.bet:
                    dependences.img_action.overrideSprite = sources.img_actionBet;
                    break;
                case BetType.call:
                    dependences.img_action.overrideSprite = sources.img_actionCall;
                    break;
                case BetType.check:
                    dependences.img_action.overrideSprite = sources.img_actionCheck;
                    break;
                case BetType.fold:
                    dependences.img_action.overrideSprite = sources.img_actionFold;
                    break;
                case BetType.rise:
                    dependences.img_action.overrideSprite = sources.img_actionRaise;
                    break;
                case BetType.none:
                    dependences.img_action.overrideSprite = sources.img_actionNone;
                    break;
            }

            if (action.bet_action == BetType.check || action.bet_action == BetType.fold || action.bet_action == BetType.none)
                return;

            dependences.img_action.gameObject.SetActive(true);
            dependences.img_betChips.gameObject.SetActive(true);
            dependences.txt_betChips.text = action.match_player.betPlayer.Get_BetsAmount(pokerMatch.turn).ToString("c2");


        }
    }
    //Eventos do jogador
    private void OnAddCard(object sender, CardEventArgs e)
    {
        Card _item = (Card)e.ICard;
        if (betplayer.holeCards.Count == 1)
        {
            dependences.img_card1.overrideSprite = _item.sprite;
            dependences.img_card1.gameObject.SetActive(true);
        }
        else if (betplayer.holeCards.Count == 2)
        {
            dependences.img_card2.overrideSprite = _item.sprite;
            dependences.img_card2.gameObject.SetActive(true);

        }
    }
    private void OnRemoveCard(object sender, CardEventArgs e)
    {
        if (dependences.img_card1.sprite == ((Card)e.ICard).sprite)
        {
            dependences.img_card1.gameObject.SetActive(false);
            dependences.img_card1.overrideSprite = null;
        }
        else
        {
            dependences.img_card2.gameObject.SetActive(false);
            dependences.img_card2.overrideSprite = null;
        }

    }
    private void OnSubChip(double value, BetPlayerEventArgs e)
    {
        dependences.txt_chipAmount.text = betplayer.chipsAmount.ToString("C2");
    }
    private void OnAddChip(double value, BetPlayerEventArgs e)
    {
        dependences.txt_chipAmount.text = betplayer.chipsAmount.ToString("C2");
    }

    //Eventos Unity
    void OnDestroy()
    {

        MatchGameListner.Matches.OnAdd -= OnMatch;
        MatchGameListner.Matches.OnRemove -= OffMatch;

        betplayer.holeCards.OnAdd -= OnAddCard;
        betplayer.holeCards.OnRemove -= OnRemoveCard;
        betplayer.OnAddChips -= OnAddChip;
        betplayer.OnRemoveChips -= OnSubChip;

        if (pokerMatch != null)
        {
            pokerMatch.OnBetPlayer_Current -= ChangeBetPlayerCurrent;
            pokerMatch.OnTurn -= ChangeTurn;
            pokerMatch.matchActions.OnAdd -= OnAction;
        }
    }


}
